import 'package:bloc_test/bloc_test.dart';
import 'package:casa_pro/src/blocs/report/report_bloc.dart';
import 'package:casa_pro/src/data/services/report/report_repository.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:mocktail/mocktail.dart';

import '../test_data.dart';

class MockReportRepository extends Mock implements ReportRepository {}

void main() {
  late ReportRepository reportRepository;

  setUp(() {
    reportRepository = MockReportRepository();
  });

  group('ReportBloc', () {
    test('initial state is ReportInitial', () {
      final ReportBloc reportBloc =
          ReportBloc(reportRepository: reportRepository);
      expect(reportBloc.state, ReportInitial());
      reportBloc.close();
    });

    group('SaveReportToCache', () {
      blocTest<ReportBloc, ReportState>(
        'emits [SaveReportToCacheError] when [SaveReportToCache] is added and '
        'saveReportToCache throws',
        setUp: () {
          when(() => reportRepository.saveReportToCache(report))
              .thenThrow(UpdateDataException(errorMessage));
        },
        build: () => ReportBloc(reportRepository: reportRepository),
        act: (ReportBloc bloc) => bloc.add(SaveReportToCache(report: report)),
        expect: () => <Matcher>[
          isA<SaveReportToCacheInProgress>(),
          isA<SaveReportToCacheError>(),
        ],
      );
      blocTest<ReportBloc, ReportState>(
        'emits [SaveReportToCacheSuccess] when [SaveReportToCache] is added and '
        'saveReportToCache is successfully called',
        setUp: () {
          when(() => reportRepository.saveReportToCache(report))
              .thenAnswer((_) async {});
        },
        build: () => ReportBloc(reportRepository: reportRepository),
        act: (ReportBloc bloc) => bloc.add(SaveReportToCache(report: report)),
        expect: () => <Matcher>[
          isA<SaveReportToCacheInProgress>(),
          isA<SaveReportToCacheSuccess>(),
        ],
      );
    });
  });
}
