import 'package:kindly_components/kindly_components.dart';

const String shift = 'Day';
const String uid = 'uid';
const String email = 'test@email.com';
const String memberId = 'memberId';
const String patientId = 'patientId';
const String notificationId = 'notificationId';
const String errorMessage = 'An error occurred.';
const String assignCareProviderEmailError =
    'The provided email address is not associated to any Care Provider. Please check the email address and try again.';
final AppUser user = AppUser(
  uid: 'uid',
  phone: '0',
  name: 'name',
  avatar: 'avatar.jpg',
  contacts: const <String>[],
  joinedDate: DateTime.now(),
  streamToken: 'streamToken',
  messageToken: 'messageToken',
);

CareTask careTask = CareTask(
  title: 'title',
  taskId: 'taskId',
  patientId: patientId,
  type: CareTaskType.general,
  repeatDays: const <String>[],
  lastUpdatedBy: 'lastUpdatedBy',
  time: DateTime.utc(2021, 12, 25),
  lastUpdated: DateTime.utc(2021, 12, 25),
);

const Address address = Address(
  number: 42,
  city: 'city',
  latitude: 0.0,
  longitude: 0.0,
  suburb: 'suburb',
  streetName: 'streetName',
);

final Patient patient = Patient(
  bio: 'bio',
  uid: 'uid',
  phone: '0',
  name: 'name',
  gender: 'Female',
  address: address,
  mobility: 'mobility',
  avatar: 'avatar.jpg',
  streamToken: 'streamToken',
  messageToken: 'messageToken',
  contacts: const <String>[],
  allergies: const <String>[],
  nextOfKin: const <String>[],
  conditions: const <String>[],
  maritalStatus: 'maritalStatus',
  dob: DateTime.utc(1945, 12, 25),
  circleMembers: const <String>[],
  circle: const <String, String>{},
  joinedDate: DateTime.utc(2021, 12, 25),
  subscriptionEnds: DateTime.utc(2023, 12, 25),
  assignedCareProviders: const <String, AssignedCareProvider>{},
);

final CareProvider careProvider = CareProvider(
  bio: 'bio',
  uid: 'uid',
  phone: '0',
  name: 'name',
  avatar: 'avatar.jpg',
  hasDriverLicense: false,
  images: const <String>[],
  duties: const <String>[],
  isCovidVaccinated: true,
  services: const <String>[],
  streamToken: 'streamToken',
  contacts: const <String>[],
  occupation: const <String>[],
  messageToken: 'messageToken',
  certificates: const <String>[],
  availability: const <String, String>{},
  joinedDate: DateTime.utc(2021, 12, 25),
  assignedPatients: const <String, AssignedPatient>{},
);

final AssignedPatient assignedPatient = AssignedPatient(
  shift: shift,
  patientId: patient.uid,
  patientName: patient.name,
  careProviderId: careProvider.uid,
  assignedDate: DateTime.utc(2021, 12, 28),
);

final AssignedCareProvider assignedCareProvider = AssignedCareProvider(
  shift: shift,
  patientId: patient.uid,
  careProviderId: careProvider.uid,
  careProviderName: careProvider.name,
  careProviderPhone: careProvider.phone,
  assignedDate: DateTime.utc(2021, 12, 28),
);

const CircleMemberData circleMember = CircleMemberData(
  uid: 'uid',
  email: email,
  isNextOfKin: false,
  patientId: patientId,
  relation: 'relation',
);

final PushNotification pushNotification = PushNotification(
  id: uid,
  text: 'text',
  isSeen: false,
  title: 'title',
  authorPhone: '0',
  authorId: 'authorId',
  authorName: 'authorName',
  type: NotificationType.info,
  recipients: const <String>[],
  createdAt: DateTime.utc(2021, 12, 28),
);

final Report report = Report(
  mood: 'mood',
  reportId: 'reportId',
  patientId: patientId,
  patientInPain: false,
  careProviderPhone: '0',
  patientName: 'patientName',
  dateCreated: '2021, 12, 28)',
  careProviderId: 'careProviderId',
  attachedPhotos: const <String>[],
  careActions: const <CareAction>[],
  careProviderName: 'careProviderName',
  createdAt: DateTime.utc(2021, 12, 28),
  careProviderAvatar: 'careProviderAvatar',
);

const TimeLog timelog = TimeLog(
  patientId: patientId,
  careProviderPhone: '0',
  timeLogId: 'timeLogId',
  patientName: 'patientName',
  dateCreated: '2021, 12, 28)',
  careProviderId: 'careProviderId',
  careProviderName: 'careProviderName',
);
