import 'package:bloc_test/bloc_test.dart';
import 'package:casa_pro/src/blocs/notifications/notification_bloc.dart';
import 'package:casa_pro/src/data/services/notification/notification_repository.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:mocktail/mocktail.dart';

import '../test_data.dart';

class MockNotificationRepository extends Mock
    implements NotificationRepository {}

void main() {
  late NotificationRepository notificationRepository;

  setUp(() {
    notificationRepository = MockNotificationRepository();
  });

  group('NotificationBloc', () {
    test('initial state is NotificationInitial', () {
      final NotificationBloc notificationBloc = NotificationBloc(
        notificationRepository: notificationRepository,
      );
      expect(notificationBloc.state, NotificationsInitial());
      notificationBloc.close();
    });
    group('FetchNotifications', () {
      blocTest<NotificationBloc, NotificationState>(
        'emits [NotificationsLoadSuccess] when [FetchNotifications] is added and '
        'fetchNotifications is successfully called',
        setUp: () {
          when(() => notificationRepository.fetchNotifications(uid)).thenAnswer(
              (_) => Stream<List<PushNotification>>.value(
                  <PushNotification>[pushNotification]));
        },
        build: () =>
            NotificationBloc(notificationRepository: notificationRepository),
        act: (NotificationBloc bloc) =>
            bloc.add(const FetchNotifications(uid: uid)),
        expect: () => <Matcher>[
          isA<NotificationsLoading>(),
          isA<NotificationsLoadSuccess>(),
        ],
      );
    });

    group('DeleteNotification', () {
      blocTest<NotificationBloc, NotificationState>(
        'emits [DeleteNotificationError] when [DeleteNotification] is added and '
        'deleteNotification throws',
        setUp: () {
          when(() => notificationRepository.deleteNotification(
                uid: uid,
                notificationId: notificationId,
              )).thenThrow(UpdateDataException(errorMessage));
        },
        build: () =>
            NotificationBloc(notificationRepository: notificationRepository),
        act: (NotificationBloc bloc) => bloc.add(
            const DeleteNotification(uid: uid, notificationId: notificationId)),
        expect: () => <Matcher>[
          isA<DeleteNotificationInProgress>(),
          isA<DeleteNotificationError>(),
        ],
      );

      blocTest<NotificationBloc, NotificationState>(
        'emits [DeleteNotificationSuccess] when [DeleteNotification] is added and '
        'deleteNotification is successfully called',
        setUp: () {
          when(() => notificationRepository.deleteNotification(
                uid: uid,
                notificationId: notificationId,
              )).thenAnswer((_) async {});
        },
        build: () =>
            NotificationBloc(notificationRepository: notificationRepository),
        act: (NotificationBloc bloc) => bloc.add(
            const DeleteNotification(uid: uid, notificationId: notificationId)),
        expect: () => <Matcher>[
          isA<DeleteNotificationInProgress>(),
          isA<DeleteNotificationSuccess>(),
        ],
      );
    });

    group('SendPushNotification', () {
      blocTest<NotificationBloc, NotificationState>(
        'emits [SendNotificationError] when [SendPushNotification] is added and '
        'sendPushNotification throws',
        setUp: () {
          when(() =>
                  notificationRepository.sendPushNotification(pushNotification))
              .thenThrow(UpdateDataException(errorMessage));
        },
        build: () =>
            NotificationBloc(notificationRepository: notificationRepository),
        act: (NotificationBloc bloc) =>
            bloc.add(SendPushNotification(notification: pushNotification)),
        expect: () => <Matcher>[
          isA<SendNotificationInProgress>(),
          isA<SendNotificationError>(),
        ],
      );

      blocTest<NotificationBloc, NotificationState>(
        'emits [SendNotificationSuccess] when [SendPushNotification] is added and '
        'sendPushNotification is successfully called',
        setUp: () {
          when(() =>
                  notificationRepository.sendPushNotification(pushNotification))
              .thenAnswer((_) async {});
        },
        build: () =>
            NotificationBloc(notificationRepository: notificationRepository),
        act: (NotificationBloc bloc) =>
            bloc.add(SendPushNotification(notification: pushNotification)),
        expect: () => <Matcher>[
          isA<SendNotificationInProgress>(),
          isA<SendNotificationSuccess>(),
        ],
      );
    });

    group('MarkNotificationAsSeen', () {
      blocTest<NotificationBloc, NotificationState>(
        'verify that markNotificationAsSeen is successfully called',
        setUp: () {
          when(() => notificationRepository.markNotificationAsSeen(
              uid: uid,
              notificationId: notificationId)).thenAnswer((_) async {});
        },
        build: () =>
            NotificationBloc(notificationRepository: notificationRepository),
        act: (NotificationBloc bloc) => bloc.add(const MarkNotificationAsSeen(
          uid: uid,
          notificationId: notificationId,
        )),
        verify: (_) {
          verify(() => notificationRepository.markNotificationAsSeen(
              uid: uid, notificationId: notificationId)).called(1);
        },
      );
    });

    group('ChangeNotificationStatus', () {
      blocTest<NotificationBloc, NotificationState>(
        'verify that changeNotificationStatus is successfully called',
        setUp: () {
          when(() => notificationRepository.changeNotificationStatus(
              uid: uid, notificationId: notificationId)).thenAnswer((_) {});
        },
        build: () =>
            NotificationBloc(notificationRepository: notificationRepository),
        act: (NotificationBloc bloc) => bloc.add(const ChangeNotificationStatus(
          uid: uid,
          notificationId: notificationId,
        )),
        verify: (_) {
          verify(() => notificationRepository.changeNotificationStatus(
              uid: uid, notificationId: notificationId)).called(1);
        },
      );
    });
  });
}
