import 'package:bloc_test/bloc_test.dart';
import 'package:casa_pro/src/blocs/time_logs/time_logs_bloc.dart';

import 'package:casa_pro/src/data/services/time_log/time_log_repository.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:mocktail/mocktail.dart';

import '../test_data.dart';

class MockTimeLogRepository extends Mock implements TimeLogRepository {}

void main() {
  late TimeLogRepository timeLogRepository;

  setUp(() {
    timeLogRepository = MockTimeLogRepository();
  });

  group('TimeLogsBloc', () {
    test('initial state is TimeLogsInitial', () {
      final TimeLogsBloc patientBloc =
          TimeLogsBloc(timeLogRepository: timeLogRepository);
      expect(patientBloc.state, TimeLogsInitial());
      patientBloc.close();
    });

    group('FetchTimeLogs', () {
      blocTest<TimeLogsBloc, TimeLogsState>(
        'emits [TimeLogsLoadError] when [FetchTimeLogs] is added and '
        'fetchCareProviderTimeLogsByMonth throws',
        setUp: () {
          when(() => timeLogRepository.fetchCareProviderTimeLogsByMonth(
                month: 7,
                careProviderId: uid,
                patientId: patientId,
              )).thenThrow(FetchDataException(errorMessage));
        },
        build: () => TimeLogsBloc(timeLogRepository: timeLogRepository),
        act: (TimeLogsBloc bloc) => bloc.add(const FetchTimeLogs(
          month: 7,
          careProviderId: uid,
          patientId: patientId,
        )),
        expect: () => <Matcher>[
          isA<TimeLogsLoading>(),
          isA<TimeLogsLoadError>(),
        ],
      );
      blocTest<TimeLogsBloc, TimeLogsState>(
        'emits [TimeLogsLoadSuccess] when [FetchTimeLogs] is added and '
        'fetchCareProviderTimeLogsByMonth is successfully called',
        setUp: () {
          when(() => timeLogRepository.fetchCareProviderTimeLogsByMonth(
                    month: 7,
                    careProviderId: uid,
                    patientId: patientId,
                  ))
              .thenAnswer(
                  (_) async => Future<List<TimeLog>>.value(<TimeLog>[timelog]));
        },
        build: () => TimeLogsBloc(timeLogRepository: timeLogRepository),
        act: (TimeLogsBloc bloc) => bloc.add(const FetchTimeLogs(
          month: 7,
          careProviderId: uid,
          patientId: patientId,
        )),
        expect: () => <Matcher>[
          isA<TimeLogsLoading>(),
          isA<TimeLogsLoadSuccess>(),
        ],
      );
    });
  });
}
