import 'package:bloc_test/bloc_test.dart';
import 'package:casa_pro/src/blocs/patient/patient_bloc.dart';
import 'package:casa_pro/src/data/services/patient/patient_repository.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:mocktail/mocktail.dart';

import '../test_data.dart';

class MockPatientRepository extends Mock implements PatientRepository {}

void main() {
  late PatientRepository patientRepository;

  setUp(() {
    patientRepository = MockPatientRepository();
  });

  group('PatientBloc', () {
    test('initial state is PatientInitial', () {
      final PatientBloc patientBloc =
          PatientBloc(patientRepository: patientRepository);
      expect(patientBloc.state, PatientInitial());
      patientBloc.close();
    });

    group('FetchPatient', () {
      blocTest<PatientBloc, PatientState>(
        'emits [PatientLoadError] when [FetchPatient] is added and '
        'fetchPatient throws',
        setUp: () {
          when(() => patientRepository.fetchPatient(patientId))
              .thenThrow(FetchDataException(errorMessage));
        },
        build: () => PatientBloc(patientRepository: patientRepository),
        act: (PatientBloc bloc) =>
            bloc.add(const FetchPatient(patientId: patientId)),
        expect: () => <Matcher>[
          isA<PatientLoading>(),
          isA<PatientLoadError>(),
        ],
      );
      blocTest<PatientBloc, PatientState>(
        'emits [PatientLoadSuccess] when [FetchPatient] is added and '
        'fetchPatient is successfully called',
        setUp: () {
          when(() => patientRepository.fetchPatient(patientId))
              .thenAnswer((_) async => Future<Patient>.value(patient));
        },
        build: () => PatientBloc(patientRepository: patientRepository),
        act: (PatientBloc bloc) =>
            bloc.add(const FetchPatient(patientId: patientId)),
        expect: () => <Matcher>[
          isA<PatientLoading>(),
          isA<PatientLoadSuccess>(),
        ],
      );
    });

    group('FetchPatientCircle', () {
      blocTest<PatientBloc, PatientState>(
        'emits [CircleMembersLoadError] when [FetchPatientCircle] is added and '
        'fetchPatientCircle throws',
        setUp: () {
          when(() => patientRepository.fetchPatientCircle(<String>[uid]))
              .thenThrow(FetchDataException(errorMessage));
        },
        build: () => PatientBloc(patientRepository: patientRepository),
        act: (PatientBloc bloc) =>
            bloc.add(const FetchPatientCircle(memberIds: <String>[uid])),
        expect: () => <Matcher>[
          isA<CircleMembersLoading>(),
          isA<CircleMembersLoadError>(),
        ],
      );
      blocTest<PatientBloc, PatientState>(
        'emits [CircleMembersLoadSuccess] when [FetchPatientCircle] is added and '
        'fetchPatientCircle is successfully called',
        setUp: () {
          when(() => patientRepository.fetchPatientCircle(<String>[uid]))
              .thenAnswer(
                  (_) async => Future<List<AppUser>>.value(<AppUser>[user]));
        },
        build: () => PatientBloc(patientRepository: patientRepository),
        act: (PatientBloc bloc) =>
            bloc.add(const FetchPatientCircle(memberIds: <String>[uid])),
        expect: () => <Matcher>[
          isA<CircleMembersLoading>(),
          isA<CircleMembersLoadSuccess>(),
        ],
      );
    });

    group('FetchAssignedCareProviders', () {
      blocTest<PatientBloc, PatientState>(
        'emits [AssignedCareProvidersLoadError] when [FetchAssignedCareProviders] is added and '
        'fetchAssignedCareProviders throws',
        setUp: () {
          when(() =>
                  patientRepository.fetchAssignedCareProviders(<String>[uid]))
              .thenThrow(FetchDataException(errorMessage));
        },
        build: () => PatientBloc(patientRepository: patientRepository),
        act: (PatientBloc bloc) => bloc.add(
            const FetchAssignedCareProviders(careProviderIds: <String>[uid])),
        expect: () => <Matcher>[
          isA<AssignedCareProvidersLoading>(),
          isA<AssignedCareProvidersLoadError>(),
        ],
      );
      blocTest<PatientBloc, PatientState>(
        'emits [AssignedCareProvidersLoadSuccess] when [FetchAssignedCareProviders] is added and '
        'fetchAssignedCareProviders is successfully called',
        setUp: () {
          when(() => patientRepository
              .fetchAssignedCareProviders(
                  <String>[uid])).thenAnswer((_) async =>
              Future<List<CareProvider>>.value(<CareProvider>[careProvider]));
        },
        build: () => PatientBloc(patientRepository: patientRepository),
        act: (PatientBloc bloc) => bloc.add(
            const FetchAssignedCareProviders(careProviderIds: <String>[uid])),
        expect: () => <Matcher>[
          isA<AssignedCareProvidersLoading>(),
          isA<AssignedCareProvidersLoadSuccess>(),
        ],
      );
    });
  });
}
