import 'package:bloc_test/bloc_test.dart';
import 'package:casa_pro/src/blocs/care_task/care_task_bloc.dart';
import 'package:casa_pro/src/data/services/care_task/care_task_repository.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:mocktail/mocktail.dart';

import '../test_data.dart';

class MockCareTaskRepository extends Mock implements CareTaskRepository {}

void main() {
  late CareTaskRepository careTaskRepository;

  setUp(() {
    careTaskRepository = MockCareTaskRepository();
  });

  group('CareTaskBloc', () {
    test('initial state is CareTaskInitial', () {
      final CareTaskBloc careTaskBloc = CareTaskBloc(
        careTaskRepository: careTaskRepository,
      );
      expect(careTaskBloc.state, CareTaskInitial());
      careTaskBloc.close();
    });

    group('FetchCareTasks', () {
      blocTest<CareTaskBloc, CareTaskState>(
        'emits [CareTasksLoadSuccess] when [FetchCareTasks] is added and '
        'fetchCareTasks is successfully called',
        setUp: () {
          when(() => careTaskRepository.fetchCareTasks(patientId)).thenAnswer(
              (_) => Stream<List<CareTask>>.value(<CareTask>[careTask]));
        },
        build: () => CareTaskBloc(careTaskRepository: careTaskRepository),
        act: (CareTaskBloc bloc) =>
            bloc.add(const FetchCareTasks(patientId: patientId)),
        expect: () => <Matcher>[
          isA<CareTasksLoading>(),
          isA<CareTasksLoadSuccess>(),
        ],
      );
    });
  });
}
