import 'package:bloc_test/bloc_test.dart';
import 'package:casa_pro/src/blocs/care_provider/care_provider_bloc.dart';
import 'package:casa_pro/src/data/services/care_provider/care_provider_repository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:mocktail/mocktail.dart';

import '../test_data.dart';

class MockCareProviderRepository extends Mock
    implements CareProviderRepository {}

void main() {
  late CareProviderRepository careProviderRepository;

  setUp(() {
    careProviderRepository = MockCareProviderRepository();
  });

  group('CareProviderBloc', () {
    test('initial state is CareProviderInitial', () {
      final CareProviderBloc careProviderBloc = CareProviderBloc(
        careProviderRepository: careProviderRepository,
      );
      expect(careProviderBloc.state, CareProviderInitial());
      careProviderBloc.close();
    });

    group('FetchCareProvider', () {
      blocTest<CareProviderBloc, CareProviderState>(
        'emits [CareProviderLoadSuccess] when [FetchCareProvider] is added and '
        'fetchCareProviderById is successfully called',
        setUp: () {
          when(() => careProviderRepository.fetchCareProviderById(uid))
              .thenAnswer((_) => Stream<CareProvider>.value(careProvider));
        },
        build: () =>
            CareProviderBloc(careProviderRepository: careProviderRepository),
        act: (CareProviderBloc bloc) =>
            bloc.add(const FetchCareProvider(careProviderId: uid)),
        expect: () => <Matcher>[
          isA<CareProviderLoading>(),
          isA<CareProviderLoadSuccess>(),
        ],
      );
    });

    group('UpdateCareProvider', () {
      blocTest<CareProviderBloc, CareProviderState>(
        'emits [UpdateCareProviderError] when [UpdateCareProvider] is added and '
        'updateCareProvider throws',
        setUp: () {
          when(() => careProviderRepository.updateCareProvider(careProvider))
              .thenThrow(UpdateDataException(errorMessage));
        },
        build: () =>
            CareProviderBloc(careProviderRepository: careProviderRepository),
        act: (CareProviderBloc bloc) =>
            bloc.add(UpdateCareProvider(careProvider: careProvider)),
        expect: () => <Matcher>[
          isA<UpdateCareProviderInProgress>(),
          isA<UpdateCareProviderError>(),
        ],
      );
      blocTest<CareProviderBloc, CareProviderState>(
        'emits [UpdateCareProviderSuccess] when [UpdateCareProvider] is added and '
        'updateCareProvider is successfully called',
        setUp: () {
          when(() => careProviderRepository.updateCareProvider(careProvider))
              .thenAnswer(
                  (_) async => Future<CareProvider>.value(careProvider));
        },
        build: () =>
            CareProviderBloc(careProviderRepository: careProviderRepository),
        act: (CareProviderBloc bloc) =>
            bloc.add(UpdateCareProvider(careProvider: careProvider)),
        expect: () => <Matcher>[
          isA<UpdateCareProviderInProgress>(),
          isA<UpdateCareProviderSuccess>(),
        ],
      );
    });

    group('FetchAssignedPatients', () {
      blocTest<CareProviderBloc, CareProviderState>(
        'emits [AssignedPatientsLoadError] when [FetchAssignedPatients] is added and '
        'fetchPatients throws',
        setUp: () {
          when(() => careProviderRepository.fetchPatients(<String>[uid]))
              .thenThrow(FetchDataException(errorMessage));
        },
        build: () =>
            CareProviderBloc(careProviderRepository: careProviderRepository),
        act: (CareProviderBloc bloc) =>
            bloc.add(const FetchAssignedPatients(patientIds: <String>[uid])),
        expect: () => <Matcher>[
          isA<AssignedPatientsLoading>(),
          isA<AssignedPatientsLoadError>(),
        ],
      );
      blocTest<CareProviderBloc, CareProviderState>(
        'emits [AssignedPatientsLoadSuccess] when [FetchAssignedPatients] is added and '
        'fetchPatients is successfully called',
        setUp: () {
          when(() => careProviderRepository.fetchPatients(<String>[uid]))
              .thenAnswer(
                  (_) async => Future<List<Patient>>.value(<Patient>[patient]));
        },
        build: () =>
            CareProviderBloc(careProviderRepository: careProviderRepository),
        act: (CareProviderBloc bloc) =>
            bloc.add(const FetchAssignedPatients(patientIds: <String>[uid])),
        expect: () => <Matcher>[
          isA<AssignedPatientsLoading>(),
          isA<AssignedPatientsLoadSuccess>(),
        ],
      );
    });

    group('RemovePatient', () {
      blocTest<CareProviderBloc, CareProviderState>(
        'emits [RemovePatientError] when [RemovePatient] is added and '
        'removePatient throws',
        setUp: () {
          when(() => careProviderRepository.removePatient(
                careProviderId: uid,
                patientId: patientId,
              )).thenThrow(UpdateDataException(errorMessage));
        },
        build: () =>
            CareProviderBloc(careProviderRepository: careProviderRepository),
        act: (CareProviderBloc bloc) => bloc.add(const RemovePatient(
          careProviderId: uid,
          patientId: patientId,
        )),
        expect: () => <Matcher>[
          isA<RemovePatientInProgress>(),
          isA<RemovePatientError>(),
        ],
      );
      blocTest<CareProviderBloc, CareProviderState>(
        'emits [RemovePatientSuccess] when [RemovePatient] is added and '
        'removePatient is successfully called',
        setUp: () {
          when(() => careProviderRepository.removePatient(
                careProviderId: uid,
                patientId: patientId,
              )).thenAnswer((_) async {});
        },
        build: () =>
            CareProviderBloc(careProviderRepository: careProviderRepository),
        act: (CareProviderBloc bloc) => bloc.add(const RemovePatient(
          careProviderId: uid,
          patientId: patientId,
        )),
        expect: () => <Matcher>[
          isA<RemovePatientInProgress>(),
          isA<RemovePatientSuccess>(),
        ],
      );
    });
  });
}
