import * as functions from "firebase-functions";
import admin = require("firebase-admin");
admin.initializeApp();
const db = admin.firestore();

const users = "users";
const testUsers = "care_users";

exports.notifications = functions.firestore
    .document("users/{uid}/notifications/{id}")
    .onCreate(async (snapshot) => {
      try {
        const recipients: string[] = snapshot.get("recipients");
        const query = await db.collection(users)
            .where("uid", "in", recipients).get();
        query.docs.forEach(async (doc) => {
          if (doc.exists) {
            const payload = {
              notification: {
                title: snapshot.get("title"),
                body: snapshot.get("text"),
                sound: "default",
              },
              data: {
                click_action: "FLUTTER_NOTIFICATION_CLICK",
                type: "notification",
              },
            };
            const deviceToken: string = doc.get("messageToken");
            await admin.messaging().sendToDevice(deviceToken, payload);
          }
        });
        console.log("notification send successfully");
      } catch (error) {
        console.log("sending notification failed:");
        console.log(error);
      }
    });
exports.notificationsTest = functions.firestore
    .document("care_users/{uid}/notifications/{id}")
    .onCreate(async (snapshot) => {
      try {
        const recipients: string[] = snapshot.get("recipients");
        const query = await db.collection(testUsers)
            .where("uid", "in", recipients).get();
        query.docs.forEach(async (doc) => {
          if (doc.exists) {
            const payload = {
              notification: {
                title: snapshot.get("title"),
                body: snapshot.get("text"),
                sound: "default",
              },
              data: {
                click_action: "FLUTTER_NOTIFICATION_CLICK",
                type: "notification",
              },
            };
            const deviceToken: string = doc.get("messageToken");
            await admin.messaging().sendToDevice(deviceToken, payload);
          }
        });
        console.log("notification send successfully (test)");
      } catch (error) {
        console.log("sending notification failed (test)");
        console.log(error);
      }
    });
exports.sendChatNotification = functions.https.onCall(
    async (data, context) => {
      try {
        const sender = data["sender"] as string;
        const message = data["message"] as string;
        const recipientId = data["recipientId"] as string;
        const recipient = await db.collection(users).
            doc(recipientId).get();
        const messageToken = recipient.get("messageToken");
        const payload = {
          notification: {
            title: sender,
            body: message,
            sound: "default",
          },
          data: {
            click_action: "FLUTTER_NOTIFICATION_CLICK",
            type: "chat",
          },
        };
        await admin.messaging().sendToDevice(messageToken, payload);
        console.log("chat notification successfully delivered");
      } catch (error) {
        console.log("failed to deliver chat notification");
        console.log(`error: ${error}`);
        throw new functions.https.HttpsError("internal",
            "internal server error", error);
      }
    });
exports.sendChatNotificationTest = functions.https.onCall(
    async (data, context) => {
      try {
        const sender = data["sender"] as string;
        const message = data["message"] as string;
        const recipientId = data["recipientId"] as string;
        const recipient = await db.collection(testUsers).
            doc(recipientId).get();
        const messageToken = recipient.get("messageToken");
        const payload = {
          notification: {
            title: sender,
            body: message,
            sound: "default",
          },
          data: {
            click_action: "FLUTTER_NOTIFICATION_CLICK",
            type: "chat",
          },
        };
        await admin.messaging().sendToDevice(messageToken, payload);
        console.log("chat notification successfully delivered (test)");
      } catch (error) {
        console.log("failed to deliver chat notification (test)");
        console.log(`error: ${error}`);
        throw new functions.https.HttpsError("internal",
            "internal server error", error);
      }
    });
