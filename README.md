# Kindly Care

Kindly Care  - An app to help look after the elderly in their homes. 

## About

The Kindly Care mobile app is built to help care providers look after the sick and elderly in their homes. It comes equipped with all the necessary features to help care providers record a patient's health vitals, meals and activities. Additional features such time logs automatically records a care provider's shift hours and the built-in messaging system allows them to communicate easily with doctors, physicians and the patient's family members. 

## Showcase
Get the Kindly Care mobile application for android [here](https://play.google.com/store/apps/details?id=com.primelabs.casa_pro).

## Getting Started
This project is built using Flutter, for a getting started, checkout [flutter.dev](https://flutter.dev/)

After successful setup of your local flutter environment, fork this repository and run the following commands in the root folder.

```bash
$ flutter clean
$ flutter pub get
$ flutter run
```



## Built With

1. Flutter Dart

2. Firebase services

3. Stream services [here](https://getstream.io/) - for messaging.

   

## Code Structure

Kindly Care was build following the **BLOC** design [pattern](https://bloclibrary.dev) which divides a project into four distinct layers:

1. Presentation
2. Domain (bloc)
3. Repositories
4. Data (infrastructure)

## Contributing
Contributions are welcome, be it technical or non-technical including suggestions, documentations and typos etc.

## Authors

* **Thomas Mabika** 



## License

This project is licensed under the Apache License - see the [LICENSE](LICENSE.md) file for details
