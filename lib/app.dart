// ignore_for_file: depend_on_referenced_packages

import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stream_chat_flutter_core/stream_chat_flutter_core.dart';
import 'package:wiredash/wiredash.dart';

import 'src/app/home/home_page.dart';
import 'src/app/onboarding/onboarding_page.dart';
import 'src/app/setup/halt_page.dart';
import 'src/common/common.dart';

class MyApp extends StatefulWidget {
  final FirebaseRemoteConfig config;
  const MyApp({super.key, required this.config});
  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey<NavigatorState>();

  @override
  void initState() {
    super.initState();
    initializeDateFormatting('en_GB');
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(
      builder: (_, Orientation orientation, ScreenType screenType) {
        final StreamChatClient client = ChatClient.client;
        return MaterialApp(
          navigatorKey: _navigatorKey,
          builder: (BuildContext context, Widget? child) => Wiredash(
            secret: widget.config.getString(kWiredashSecret),
            projectId: widget.config.getString(kWiredashProjectId),
            options: const WiredashOptionsData(),
            theme: WiredashThemeData(
              primaryColor: Colors.teal[400],
              secondaryColor: Colors.teal[400],
            ),
            child: StreamChatCore(client: client, child: child!),
          ),
          title: 'Kindly Care Pro',
          restorationScopeId: 'root',
          theme: AppTheme.lightTheme(),
          debugShowCheckedModeBanner: false,
          onGenerateRoute: AppRoute.generateRoute,
          home: BlocConsumer<AuthBloc, AuthState>(
            listener: (BuildContext context, AuthState state) {
              if (state is Authenticated && state.user is CareProvider) {
                Navigator.of(context)
                    .popAndPushNamed(kHomePageRoute, arguments: state.user);
              }
            },
            buildWhen: (_, AuthState state) {
              return state is AuthInitial ||
                  state is Authenticated ||
                  state is Unauthenticated;
            },
            builder: (_, AuthState state) {
              final SharedPreferences prefs = SharedPrefs.instance;
              final bool isApFirstRun = prefs.getBool(kIsAppFirstRun) ?? true;
              if (isApFirstRun) {
                return const OnboardingPage();
              }
              if (state is AuthInitial) {
                return const SplashScreen();
              } else if (state is Unauthenticated) {
                return LoginPage(
                  details: state.details,
                  onAuthSuccess: (AppUser user) {
                    if (user is CareProvider) {
                      return HomePage(careProvider: user);
                    } else {
                      return HaltPage(user: user);
                    }
                  },
                );
              } else if (state is Authenticated) {
                final AppUser user = state.user;
                if (user is CareProvider) {
                  return HomePage(careProvider: user);
                } else {
                  return HaltPage(user: user);
                }
              } else {
                return const LoadingIndicator();
              }
            },
          ),
        );
      },
    );
  }
}
