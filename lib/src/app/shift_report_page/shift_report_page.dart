import 'dart:io';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../blocs/report/report_bloc.dart';
import '../../common/common.dart';

class ShiftReportPage extends StatelessWidget {
  final TimeLog timelog;
  final CareProvider careProvider;

  const ShiftReportPage(
      {super.key, required this.careProvider, required this.timelog});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ValueListenableBuilder<Box<Report>>(
        valueListenable: ReportBox.box.listenable(),
        builder: (BuildContext context, Box<Report> box, _) {
          final Report? report = box.values.firstWhereOrNull((Report report) =>
              report.patientId == timelog.patientId &&
              report.reportId == timelog.timeLogId);

          return _ShiftReportPageView(
            careProvider: careProvider,
            report: report,
            timelog: timelog,
          );
        },
      ),
    );
  }
}

class _ShiftReportPageView extends StatefulWidget {
  final Report? report;
  final TimeLog timelog;
  final CareProvider careProvider;

  const _ShiftReportPageView({
    Key? key,
    this.report,
    required this.careProvider,
    required this.timelog,
  }) : super(key: key);

  @override
  _ShiftReportPageController createState() => _ShiftReportPageController();
}

class _ShiftReportPageController extends State<_ShiftReportPageView> {
  late bool _patientInPain;
  late List<String> imageURLs;
  late TextEditingController _moodController;
  late TextEditingController _painController;
  late TextEditingController _summaryController;
  late TextEditingController _painLocationController;
  final List<File> _selectedImages = <File>[];

  @override
  void initState() {
    super.initState();
    _patientInPain = widget.report?.patientInPain ?? false;
    imageURLs = widget.report?.attachedPhotos ?? const <String>[];
    _moodController = TextEditingController(text: widget.report?.mood);
    _painController = TextEditingController(text: _getPainStatus());
    _painLocationController =
        TextEditingController(text: widget.report?.painLocation);
    _summaryController = TextEditingController(text: widget.report?.summary);

    _initImages();
  }

  void _initImages() {
    if (imageURLs.isNotEmpty) {
      for (final String path in imageURLs) {
        final Uri uri = Uri(path: path);
        final File file = File.fromUri(uri);
        _selectedImages.add(file);
      }
    }
  }

  String? _getPainStatus() {
    final bool? patientInPain = widget.report?.patientInPain;
    if (patientInPain != null) {
      return patientInPain ? 'Yes' : 'No';
    } else {
      return null;
    }
  }

  @override
  void dispose() {
    _moodController.dispose();
    _painController.dispose();
    _painLocationController.dispose();
    _summaryController.dispose();
    super.dispose();
  }

  void _onSaveToCache() {
    context
        .read<ReportBloc>()
        .add(SaveReportToCache(report: buildReport(), silent: true));
  }

  void _onSubmitReport() {
    context.read<ReportBloc>().add(SaveReportToCache(report: buildReport()));
  }

  Report buildReport() {
    return Report(
      attachedPhotos: imageURLs,
      mood: _moodController.text,
      patientInPain: _patientInPain,
      reportId: widget.timelog.timeLogId,
      patientId: widget.timelog.patientId,
      careProviderId: widget.careProvider.uid,
      patientName: widget.timelog.patientName,
      careProviderName: widget.careProvider.name,
      careProviderPhone: widget.careProvider.phone,
      careProviderAvatar: widget.careProvider.avatar,
      createdAt: widget.report?.createdAt ?? DateTime.now(),
      summary:
          _summaryController.text.isNotEmpty ? _summaryController.text : null,
      careActions: const <CareAction>[],
      painLocation: _painLocationController.text.isNotEmpty
          ? _painLocationController.text
          : null,
      dateCreated: widget.report?.dateCreated ??
          DateFormat('yyyy-MM-dd').format(DateTime.now()),
    );
  }

  String patientName() => widget.timelog.patientName.firstName();

  @override
  Widget build(BuildContext context) {
    final bool isLoading = context.select((ReportBloc bloc) => bloc.state)
        is SaveReportToCacheInProgress;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Care Report'),
      ),
      body: BlocListener<ReportBloc, ReportState>(
        listener: (BuildContext context, ReportState state) {
          if (state is SaveReportToCacheError) {
            retrySnackbar(context, state.error, onRetry: _onSubmitReport);
          } else if (state is SaveReportToCacheSuccess) {
            Navigator.pop(context);
            successSnackbar(context, state.message);
          }
        },
        child: LoadingOverlay(
          isLoading: isLoading,
          child: ListView(
            padding: EdgeInsets.all(3.0.w),
            physics: const BouncingScrollPhysics(),
            children: <Widget>[
              SizedBox(height: 2.5.h),
              DropdownTextField(
                readOnly: true,
                labelText: 'Patient Mood',
                controller: _moodController,
                hintText: "${patientName()}'s general mood",
                onChanged: (_) => _onSaveToCache(),
                items: kMoodItems,
              ),
              SizedBox(height: 2.5.h),
              DropdownTextField(
                readOnly: true,
                controller: _painController,
                labelText: 'Pain',
                hintText: 'Is ${patientName()} in any pain?',
                onChanged: (String val) {
                  _patientInPain = val == 'Yes';
                  _onSaveToCache();
                },
                items: const <String>['Yes', 'No'],
              ),
              SizedBox(height: 2.5.h),
              Visibility(
                visible: _patientInPain,
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 2.5.h),
                    MaterialTextField(
                      minLines: 2,
                      maxLines: null,
                      labelText: 'Pain Location',
                      hintText: 'Where is the source of pain?',
                      controller: _painLocationController,
                      onChanged: (_) => _onSaveToCache(),
                      textInputAction: TextInputAction.newline,
                    ),
                  ],
                ),
              ),
              SizedBox(height: 2.5.h),
              MaterialTextField(
                minLines: 3,
                maxLines: null,
                isOptional: true,
                labelText: 'Summary',
                controller: _summaryController,
                onChanged: (_) => _onSaveToCache(),
                textInputAction: TextInputAction.newline,
                hintText: 'Enter shift summary',
              ),
              SizedBox(height: 2.5.h),
              Padding(
                padding: EdgeInsets.only(left: 1.5.w, bottom: 0.1.h),
                child: const Text(
                  'Attach Photos (Optional)',
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
              ),
              SizedBox(height: 1.0.h),
              ImagePicker(
                selectedImages: _selectedImages,
                onDone: (List<File> images) {
                  final List<String> paths = <String>[];
                  for (final File image in images) {
                    paths.add(image.path);
                  }

                  imageURLs = paths;

                  _onSaveToCache();
                },
              ),
              SizedBox(height: _patientInPain ? 9.5.h : 10.5.h),
              ActionButton(title: 'Save Report', onPressed: _onSubmitReport),
            ],
          ),
        ),
      ),
    );
  }
}
