import 'package:animations/animations.dart';
import 'package:collection/collection.dart';
import 'package:fcm_config/fcm_config.dart';
import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:stream_chat_flutter_core/stream_chat_flutter_core.dart';

import '../../blocs/care_task/care_task_bloc.dart';
import '../../blocs/clock_in/clock_in_bloc.dart';
import '../../common/common.dart';
import '../../cubits/chat_cubit/chat_cubit.dart';
import '../account/account_page.dart';
import '../calender_page/calender_page.dart';
import '../messages/messages.dart';
import '../misc/patient_profile/patient_records_page/patient_records_page.dart';
import '../settings_page/settings_page.dart';
import '../shift_report_page/shift_report_page.dart';
import 'cubit/home_cubit.dart';

class HomePage extends StatelessWidget {
  final CareProvider careProvider;
  const HomePage({super.key, required this.careProvider});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: <BlocProvider<dynamic>>[
        BlocProvider<HomeCubit>(
          create: (BuildContext context) => HomeCubit(),
        ),
      ],
      child: _HomePageView(careProvider),
    );
  }
}

class _HomePageView extends StatefulWidget {
  final CareProvider careProvider;
  const _HomePageView(this.careProvider, {Key? key}) : super(key: key);

  @override
  _HomePageViewState createState() => _HomePageViewState();
}

class _HomePageViewState extends State<_HomePageView>
    with FCMNotificationClickMixin<_HomePageView> {
  @override
  void initState() {
    super.initState();
    context.read<ClockInBloc>().add(SyncData());
    FCMConfig.instance.getInitialMessage().then(_onMessage);
    context.read<ChatCubit>().connectStreamUser(widget.careProvider);
  }

  void _onMessage(RemoteMessage? message) {
    final String? type = message?.data[kType] as String?;
    if (type == kNotification) {
      Navigator.pushNamed(context, kNotificationsPageRoute,
          arguments: widget.careProvider);
    } else if (type == kChat) {
      context.read<HomeCubit>().onSetTab(1);
    }
  }

  Future<void> _onClockButtonTap(TimeLog? timelog, Report? report) async {
    if (timelog != null && report != null) {
      context.read<ClockInBloc>().add(ClockOut(
          careProvider: widget.careProvider, timelog: timelog, report: report));
    } else if (timelog != null && report == null) {
      _showReportNotFoundDialog();
    } else {
      final String? result = await Navigator.pushNamed<String>(
        context,
        kClockInPageRoute,
        arguments: widget.careProvider,
      );

      if (mounted && result != null) {
        context
            .read<CareTaskBloc>()
            .add(FetchCareTasksByDate(patientId: result, date: DateTime.now()));
      }
    }
  }

  bool _showNotificationBadge() {
    final StreamChatClient client = ChatClient.client;
    final int unreadMessages = client.state.totalUnreadCount;
    return unreadMessages >= 1;
  }

  void _onChangeTab(int value) {
    context.read<HomeCubit>().onSetTab(value);
  }

  @override
  Widget build(BuildContext context) {
    final Color activeColor = Theme.of(context).primaryColor;
    final Color disabledColor = Theme.of(context).disabledColor;
    final int selectedTab =
        context.select((HomeCubit cubit) => cubit.state.tab);

    return Scaffold(
      body: BlocListener<ClockInBloc, ClockInState>(
        listener: (BuildContext context, ClockInState state) {
          if (state is ClockOutSuccess) {
            successSnackbar(context, state.message);
          }
        },
        child: PageTransitionSwitcher(
          transitionBuilder: (Widget child, Animation<double> animation,
              Animation<double> secondaryAnimation) {
            return FadeThroughTransition(
              animation: animation,
              secondaryAnimation: secondaryAnimation,
              child: child,
            );
          },
          duration: const Duration(milliseconds: 700),
          child: IndexedStack(
            index: selectedTab,
            children: <Widget>[
              CalenderPage(careProvider: widget.careProvider),
              const MessagesPage(),
              AccountPage(careProvider: widget.careProvider),
              SettingsPage(careProvider: widget.careProvider),
            ],
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: ValueListenableBuilder<Box<TimeLog>>(
        valueListenable: TimeLogBox.box.listenable(),
        builder: (BuildContext context, Box<TimeLog> box, _) {
          final TimeLog? timelog = box.values.firstWhereOrNull(
              (TimeLog timelog) => timelog.finishTime == null);
          return ValueListenableBuilder<Box<Report>>(
            valueListenable: ReportBox.box.listenable(),
            builder: (BuildContext context, Box<Report> box, _) {
              final Report? report = box.values.lastOrNull;
              return FloatingActionButton(
                onPressed: () => _showQuickActionBottomSheet(
                    timelog: timelog, report: report),
                child: Icon(
                  Icons.menu_outlined,
                  size: 3.8.h,
                  color: Colors.white,
                ),
              );
            },
          );
        },
      ),
      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            IconButton(
              icon: Icon(selectedTab == 0
                  ? Boxicons.bx_calendar_event
                  : Boxicons.bxs_calendar_event),
              iconSize: 4.0.h,
              onPressed: () => _onChangeTab(0),
              color: selectedTab == 0 ? activeColor : disabledColor,
            ),
            Padding(
              padding: EdgeInsets.only(right: 15.0.w),
              child: IconButton(
                icon: Stack(
                  children: <Widget>[
                    Icon(
                      selectedTab == 1 ? Boxicons.bxs_chat : Boxicons.bx_chat,
                      size: 4.0.h,
                    ),
                    Visibility(
                      visible: _showNotificationBadge(),
                      child: Positioned(
                        top: 0.1.h,
                        right: 0.8.w,
                        child: Icon(Icons.brightness_1,
                            size: 1.5.h, color: Colors.redAccent),
                      ),
                    ),
                  ],
                ),
                onPressed: () => _onChangeTab(1),
                color: selectedTab == 1 ? activeColor : disabledColor,
              ),
            ),
            IconButton(
              icon:
                  Icon(selectedTab == 2 ? Boxicons.bxs_user : Boxicons.bx_user),
              iconSize: 4.0.h,
              onPressed: () => _onChangeTab(2),
              color: selectedTab == 2 ? activeColor : disabledColor,
            ),
            IconButton(
              icon: Icon(selectedTab == 3 ? Boxicons.bxs_cog : Boxicons.bx_cog),
              iconSize: 4.0.h,
              onPressed: () => _onChangeTab(3),
              color: selectedTab == 3 ? activeColor : disabledColor,
            ),
          ],
        ),
      ),
    );
  }

  void _showQuickActionBottomSheet({TimeLog? timelog, Report? report}) {
    optionsBottomSheet(
      height: 52.0.h,
      context: context,
      title: 'Quick Actions',
      options: <Widget>[
        IconListTile(
          title: timelog == null ? 'Clock-In' : 'Clock-Out',
          showTrailing: false,
          icon: FeatherIcons.clock,
          subtitle: timelog == null
              ? 'Clock-In to begin your shift'
              : 'Clock-Out to end your shift',
          onTap: () {
            Navigator.pop(context);
            _onClockButtonTap(timelog, report);
          },
        ),
        Divider(indent: 18.w),
        IconListTile(
          showTrailing: false,
          title: 'Care Report',
          subtitle: 'Log Care Report',
          icon: FeatherIcons.clipboard,
          onTap: () {
            Navigator.pop(context);
            timelog == null
                ? _showClockedOutLogReportDialog()
                : _onLogCareReport(timelog);
          },
        ),
        Divider(indent: 18.w),
        IconListTile(
          title: 'Patient Vitals',
          showTrailing: false,
          subtitle: 'Record Patient Vitals',
          icon: FeatherIcons.activity,
          onTap: () {
            Navigator.pop(context);
            timelog == null
                ? _showClockedOutVitalsDialog()
                : Navigator.push(
                    context,
                    CupertinoPageRoute<void>(
                      builder: (_) => PatientRecordsPage(
                        patientId: timelog.patientId,
                        patientName: timelog.patientName,
                        careProvider: widget.careProvider,
                      ),
                    ),
                  );
          },
        ),
      ],
    );
  }

  void _showClockedOutVitalsDialog() {
    showOneButtonDialog(
      context,
      title: 'Not Allowed',
      content:
          'You are currently not Clocked-In. You need to be Clocked-In to record Patient Vitals.',
      buttonText: 'Ok',
      onPressed: () => Navigator.pop(context),
    );
  }

  void _showClockedOutLogReportDialog() {
    showOneButtonDialog(
      context,
      title: 'Not Allowed',
      content:
          'You are currently not Clocked-In. You need to be Clocked-In to log Patient Care Reports.',
      buttonText: 'Ok',
      onPressed: () => Navigator.pop(context),
    );
  }

  void _showReportNotFoundDialog() {
    showOneButtonDialog(
      context,
      title: 'Report Not Found',
      content:
          'You have no Care Report. You need to create a Care Report first before you Clock Out.',
      buttonText: 'Ok',
      onPressed: () => Navigator.pop(context),
    );
  }

  @override
  void onClick(RemoteMessage notification) {
    final String? type = notification.data['type'] as String?;
    if (type == 'notification') {
      Navigator.pushNamed(context, kNotificationsPageRoute,
          arguments: widget.careProvider);
    } else if (type == 'chat') {
      _onChangeTab(1);
    }
  }

  void _onLogCareReport(TimeLog timelog) {
    Navigator.push(
      context,
      CupertinoPageRoute<void>(
        builder: (_) => ShiftReportPage(
            careProvider: widget.careProvider, timelog: timelog),
      ),
    );
  }
}
