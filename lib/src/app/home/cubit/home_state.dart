part of 'home_cubit.dart';

class HomeState extends Equatable {
  final int tab;

  const HomeState({this.tab = 0});

  @override
  List<Object> get props => <Object>[tab];
}
