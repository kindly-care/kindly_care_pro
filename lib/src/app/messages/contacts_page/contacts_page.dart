// ignore_for_file: depend_on_referenced_packages

import 'package:cached_network_image/cached_network_image.dart';
import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../common/common.dart';
import '../../../data/services/services.dart';
import 'bloc/contacts_bloc.dart';

class ContactsPage extends StatelessWidget {
  const ContactsPage({super.key});

  @override
  Widget build(BuildContext context) {
    final List<String> contactIds =
        context.select((AuthBloc bloc) => bloc.user).contacts;
    return BlocProvider<ContactsBloc>(
      create: (BuildContext context) => ContactsBloc(
        authRepository: context.read<AuthService>(),
        contactsRepository: context.read<ContactsService>(),
      )..add(FetchContacts(contactIds: contactIds)),
      child: _ContactsPageView(contactIds),
    );
  }
}

class _ContactsPageView extends StatefulWidget {
  final List<String> contactIds;
  const _ContactsPageView(this.contactIds, {Key? key}) : super(key: key);

  @override
  State<_ContactsPageView> createState() => _ContactsPageViewState();
}

class _ContactsPageViewState extends State<_ContactsPageView> {
  late TextEditingController _emailController;

  @override
  void initState() {
    super.initState();
    _emailController = TextEditingController();
  }

  @override
  void didUpdateWidget(_ContactsPageView oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.contactIds.length != oldWidget.contactIds.length) {
      _onFetchContacts();
    }
  }

  @override
  void dispose() {
    _emailController.dispose();
    super.dispose();
  }

  void _onFetchContacts() {
    context
        .read<ContactsBloc>()
        .add(FetchContacts(contactIds: widget.contactIds));
  }

  void _onAddContact(AppUser user) {
    context
        .read<ContactsBloc>()
        .add(AddContact(uid: user.uid, email: _emailController.text));
  }

  void _showAddContactDialog(AppUser user) {
    showEmailInputDialog(
      context,
      title: 'Add Contact',
      controller: _emailController,
      hintText: "Contact's email address",
      onSubmit: () {
        if (!_emailController.text.isValidEmail()) {
          showToast(
              message: 'Please enter valid email address',
              gravity: ToastGravity.BOTTOM);
          return;
        }
        if (_emailController.text == user.email) {
          showToast(
              message: 'Not permitted. Please enter another email address',
              gravity: ToastGravity.BOTTOM);
          return;
        }
        Navigator.pop(context);
        _onAddContact(user);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    final ContactsState state =
        context.select((ContactsBloc bloc) => bloc.state);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Contacts'),
        actions: <Widget>[
          IconButton(
            onPressed: () => _showAddContactDialog(user),
            icon: Icon(Icons.add, size: 3.8.h),
          ),
        ],
      ),
      body: LoadingOverlay(
        isLoading:
            state is AddContactInProgress || state is RemoveContactInProgress,
        child: BlocConsumer<ContactsBloc, ContactsState>(
          listener: (BuildContext context, ContactsState state) {
            if (state is AddContactError) {
              errorSnackbar(context, state.message);
            } else if (state is AddContactSuccess) {
              successSnackbar(context, state.message);
            } else if (state is RemoveContactError) {
              errorSnackbar(context, state.message);
            } else if (state is RemoveContactSuccess) {
              successSnackbar(context, state.message);
            }
          },
          buildWhen: (ContactsState previous, ContactsState current) {
            return current is ContactsLoading ||
                current is ContactsLoadSuccess ||
                current is ContactsLoadError;
          },
          builder: (BuildContext context, ContactsState state) {
            if (state is ContactsLoadError) {
              return LoadErrorWidget(
                  message: state.message, onRetry: _onFetchContacts);
            } else if (state is ContactsLoadSuccess) {
              return _PageContent(contacts: state.contacts, user: user);
            } else {
              return const LoadingIndicator();
            }
          },
        ),
      ),
    );
  }
}

class _PageContent extends StatelessWidget {
  final AppUser user;
  final List<AppUser> contacts;
  const _PageContent({Key? key, required this.contacts, required this.user})
      : super(key: key);

  void _onContactTap(BuildContext context, AppUser contact) {
    createChannel(context, uid: user.uid, otherId: contact.uid);
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    if (contacts.isEmpty) {
      return const NoDataWidget(
          'No contacts found. Press the (+) button to add a contact.');
    } else {
      return ListView.separated(
        separatorBuilder: (_, __) =>
            Divider(indent: 4.0.w, color: Colors.grey.shade300),
        itemCount: contacts.length,
        padding: EdgeInsets.all(3.0.w),
        physics: const BouncingScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          final AppUser contact = contacts[index];
          return ListTile(
            leading: CircleAvatar(
              radius: 3.8.h,
              backgroundImage: CachedNetworkImageProvider(contact.avatar),
            ),
            onTap: () => _onContactTap(context, contact),
            onLongPress: () => _onContactLongPress(context, contact),
            title: Text(
              contact.name,
              overflow: TextOverflow.ellipsis,
              style: theme.titleMedium,
            ),
            trailing: Icon(Icons.chevron_right, size: 3.8.h),
          );
        },
      );
    }
  }

  void _onContactLongPress(BuildContext context, AppUser contact) {
    HapticFeedback.lightImpact();
    optionsBottomSheet(
      height: 32.0.h,
      context: context,
      title: contact.name,
      options: <Widget>[
        IconListTile(
          title: 'Call',
          showTrailing: false,
          icon: FeatherIcons.phone,
          onTap: () {
            Navigator.pop(context);
            onMakeCall(contact.phone);
          },
        ),
        Divider(indent: 18.w),
        IconListTile(
          title: 'Message',
          showTrailing: false,
          icon: FeatherIcons.messageSquare,
          onTap: () {
            Navigator.pop(context);
            createChannel(context, uid: user.uid, otherId: contact.uid);
          },
        ),
      ],
    );
  }
}
