import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../../data/services/services.dart';

part 'contacts_event.dart';
part 'contacts_state.dart';

class ContactsBloc extends Bloc<ContactsEvent, ContactsState> {
  final AuthRepository _authRepository;
  final ContactsRepository _contactsRepository;
  ContactsBloc({
    required AuthRepository authRepository,
    required ContactsRepository contactsRepository,
  })  : _authRepository = authRepository,
        _contactsRepository = contactsRepository,
        super(ContactsInitial()) {
    on<FetchContacts>(_onFetchContacts);
    on<AddContact>(_onAddContact);
    on<RemoveContact>(_onRemoveContact);
  }

  Future<void> _onFetchContacts(
      FetchContacts event, Emitter<ContactsState> emit) async {
    emit(ContactsLoading());

    try {
      final List<AppUser> contacts =
          await _contactsRepository.fetchContacts(event.contactIds);
      emit(ContactsLoadSuccess(contacts: contacts));
    } on FetchDataException catch (e) {
      emit(ContactsLoadError(message: e.toString()));
    }
  }

  Future<void> _onAddContact(
      AddContact event, Emitter<ContactsState> emit) async {
    emit(AddContactInProgress());

    try {
      final AppUser? user = await _authRepository.fetchUserByEmail(event.email);

      if (user == null) {
        emit(const AddContactError(
          message: 'Failed to add contact: User not found.',
        ));
      } else {
        await _contactsRepository.addContact(
            uid: event.uid, contactId: user.uid);
        emit(const AddContactSuccess(message: 'Contact added.'));
      }
    } on UpdateDataException catch (e) {
      emit(AddContactError(message: e.toString()));
    }
  }

  Future<void> _onRemoveContact(
      RemoveContact event, Emitter<ContactsState> emit) async {
    emit(RemoveContactInProgress());
    try {
      await _contactsRepository.removeContact(
          uid: event.uid, contactId: event.contactId);
      emit(const RemoveContactSuccess(message: 'Contact removed.'));
    } on UpdateDataException catch (e) {
      emit(RemoveContactError(message: e.toString()));
    }
  }
}
