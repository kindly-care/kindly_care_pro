import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:stream_chat_flutter_core/stream_chat_flutter_core.dart';

import '../../common/constants/constants.dart';
import '../../common/helper/chat_client.dart';
import '../../cubits/chat_cubit/chat_cubit.dart';
import 'widgets/widgets.dart';

enum _ChatsPopupAction { contacts }

class MessagesPage extends StatelessWidget {
  const MessagesPage({super.key});

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    return _MessagesPageView(user: user);
  }
}

class _MessagesPageView extends StatefulWidget {
  final AppUser user;
  const _MessagesPageView({Key? key, required this.user}) : super(key: key);

  @override
  MessagesState createState() => MessagesState();
}

class MessagesState extends State<_MessagesPageView> {
  final StreamChatClient client = ChatClient.client;
  late final StreamChannelListController _controller =
      StreamChannelListController(
    client: client,
    filter: Filter.and(<Filter>[
      Filter.equal('type', 'messaging'),
      Filter.in_('members', <String>[widget.user.uid])
    ]),
  );

  @override
  void initState() {
    super.initState();
    final ChatCubit cubit = context.read<ChatCubit>();
    if (cubit.state is ChatInitial) {
      cubit.connectStreamUser(widget.user);
    }
    _controller.doInitialLoad();
  }

  @override
  void dispose() {
    if (mounted) {
      _controller.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Messages'),
        actions: const <Widget>[_ChatsPopupButton()],
      ),
      body: PagedValueListenableBuilder<int, Channel>(
        valueListenable: _controller,
        builder: (BuildContext context, PagedValue<int, Channel> value,
            Widget? child) {
          return value.when(
            (List<Channel> channels, int? nextPageKey,
                    StreamChatError? error) =>
                LazyLoadScrollView(
              onEndOfPage: () async {
                if (nextPageKey != null) {
                  _controller.loadMore(nextPageKey);
                }
              },
              child: channels.isEmpty
                  ? const NoDataWidget(
                      'You currently do not have any messages.')
                  : ListView.builder(
                      itemCount: (nextPageKey != null || error != null)
                          ? channels.length + 1
                          : channels.length,
                      itemBuilder: (BuildContext context, int index) {
                        if (index == channels.length) {
                          if (error != null) {
                            return TextButton(
                              onPressed: _controller.retry,
                              child: Text(error.message),
                            );
                          }
                          return const LoadingIndicator();
                        }
                        final Channel channel = channels[index];
                        return MessageTile(user: widget.user, channel: channel);
                      },
                    ),
            ),
            loading: () => const LoadingIndicator(),
            error: (StreamChatError error) => LoadErrorWidget(
              message: error.message,
              onRetry: _controller.retry,
            ),
          );
        },
      ),
    );
  }
}

class _ChatsPopupButton extends StatelessWidget {
  const _ChatsPopupButton({Key? key}) : super(key: key);

  void _onPopupAction(BuildContext context, _ChatsPopupAction action) {
    switch (action) {
      case _ChatsPopupAction.contacts:
        Navigator.of(context).pushNamed(kContactsPageRoute);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<_ChatsPopupAction>(
      icon: Icon(Icons.more_vert_outlined, size: 3.8.h),
      onSelected: (_ChatsPopupAction value) => _onPopupAction(context, value),
      itemBuilder: (BuildContext context) {
        return <PopupMenuEntry<_ChatsPopupAction>>[
          const PopupMenuItem<_ChatsPopupAction>(
            value: _ChatsPopupAction.contacts,
            child: MenuElement(title: 'Contacts'),
          ),
        ];
      },
    );
  }
}
