import 'package:dart_date/dart_date.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class ChatDateLabel extends StatelessWidget {
  final DateTime dateTime;
  const ChatDateLabel({super.key, required this.dateTime});

  String _getTime() {
    final DateTime today = DateTime.now();
    if (dateTime.isToday) {
      return 'TODAY';
    } else if (dateTime.isYesterday) {
      return 'YESTERDAY';
    } else if (dateTime.isAfter(today.subDays(7))) {
      return DateFormat.EEEE().format(dateTime);
    } else {
      return DateFormat.MMMd().format(dateTime);
    }
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Center(
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 5.0.h),
        child: Container(
          decoration: BoxDecoration(
            color: Theme.of(context).cardColor,
            borderRadius: BorderRadius.circular(12.0),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 0.8.h, horizontal: 2.0.w),
            child: Text(
              _getTime(),
              style: theme.bodyMedium?.copyWith(
                fontWeight: FontWeight.bold,
                fontSize: 15.5.sp,
                color: Colors.grey.shade700,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
