import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:stream_chat_flutter_core/stream_chat_flutter_core.dart';

class OtherChatBubble extends StatelessWidget {
  final Message message;
  const OtherChatBubble({super.key, required this.message});

  static const double _borderRadius = 24.0;

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final String createdAt =
        DateFormat.Hm().format(message.createdAt.toLocal());
    return Padding(
      padding: EdgeInsets.only(right: 8.0.w, top: 2.5.h, bottom: 0.5.h),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Theme.of(context).cardColor,
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(_borderRadius),
                  topRight: Radius.circular(_borderRadius),
                  bottomRight: Radius.circular(_borderRadius),
                ),
              ),
              child: Padding(
                padding:
                    EdgeInsets.symmetric(horizontal: 2.0.w, vertical: 2.5.h),
                child: Text(message.text ?? '', style: theme.bodyLarge),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 1.0.h),
              child: Text(
                createdAt,
                style: theme.bodyMedium?.copyWith(
                  fontWeight: FontWeight.bold,
                  fontSize: 15.5.sp,
                  color: Colors.grey,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class OwnChatBubble extends StatelessWidget {
  final Message message;
  const OwnChatBubble({super.key, required this.message});

  static const double _borderRadius = 24.0;

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final String createdAt =
        DateFormat.Hm().format(message.createdAt.toLocal());

    return Padding(
      padding: EdgeInsets.only(left: 8.0.w, top: 2.5.h, bottom: 0.5.h),
      child: Align(
        alignment: Alignment.centerRight,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Colors.teal.shade100,
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(_borderRadius),
                  topRight: Radius.circular(_borderRadius),
                  bottomLeft: Radius.circular(_borderRadius),
                ),
              ),
              child: Padding(
                padding:
                    EdgeInsets.symmetric(horizontal: 2.0.w, vertical: 2.5.h),
                child: Text(message.text ?? '', style: theme.bodyLarge),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 1.0.h),
              child: Text(
                createdAt,
                style: theme.subtitle2!.copyWith(
                  fontWeight: FontWeight.bold,
                  fontSize: 15.5.sp,
                  color: Colors.grey,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
