import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:stream_chat_flutter_core/stream_chat_flutter_core.dart';

import '../../../common/constants/constants.dart';
import '../../../common/helper/stream_chat.dart';
import 'widgets.dart';

class MessageTile extends StatelessWidget {
  final AppUser user;
  final Channel channel;

  const MessageTile({super.key, required this.user, required this.channel});

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;

    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, kChatPageRoute, arguments: channel);
      },
      child: Container(
        height: 14.5.h,
        margin: EdgeInsets.symmetric(horizontal: 0.8.w),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: Theme.of(context).dividerColor,
              width: 0.15.w,
            ),
          ),
        ),
        child: Padding(
          padding: EdgeInsets.all(0.5.h),
          child: Row(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(1.6.h),
                child: CircleAvatar(
                  radius: 3.8.h,
                  backgroundImage: CachedNetworkImageProvider(
                      ChatHelpers.getChannelImage(channel, user)),
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 1.2.h),
                      child: Text(
                        ChatHelpers.getChannelName(channel, user),
                        overflow: TextOverflow.ellipsis,
                        style: theme.titleMedium?.copyWith(
                          letterSpacing: 0.2,
                          wordSpacing: 1.5,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 3.0.h,
                      child: _getLastMessage(context, channel),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(right: 2.0.w),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    _getLastMessageTime(context, channel),
                    SizedBox(height: 1.h),
                    Center(
                      child: UnreadIndicator(channel: channel),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _getLastMessage(BuildContext context, Channel channel) {
    final TextTheme theme = Theme.of(context).textTheme;
    return BetterStreamBuilder<Message>(
      stream: channel.state!.lastMessageStream,
      initialData: channel.state!.lastMessage,
      builder: (_, Message lastMessage) {
        return Text(
          lastMessage.text ?? '',
          overflow: TextOverflow.ellipsis,
          style: theme.titleSmall?.copyWith(
            color: Colors.grey.shade600,
            fontSize: 17.5.sp,
          ),
        );
      },
    );
  }

  Widget _getLastMessageTime(BuildContext context, Channel channel) {
    return BetterStreamBuilder<DateTime>(
      stream: channel.lastMessageAtStream,
      initialData: channel.lastMessageAt,
      builder: (_, DateTime date) {
        final String time = DateFormat.Hm().format(date.toLocal());
        return Text(
          time,
          overflow: TextOverflow.ellipsis,
          style: Theme.of(context)
              .textTheme
              .labelLarge
              ?.copyWith(fontSize: 17.0.sp),
        );
      },
    );
  }
}
