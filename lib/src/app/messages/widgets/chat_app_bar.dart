import 'package:cached_network_image/cached_network_image.dart';
import 'package:collection/collection.dart';
import 'package:dart_date/dart_date.dart';
import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/material.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:stream_chat_flutter_core/stream_chat_flutter_core.dart';

import '../../../common/helper/helper.dart';

class ChatAppBar extends StatelessWidget implements PreferredSizeWidget {
  final AppUser user;
  const ChatAppBar({super.key, required this.user});

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);

  void _onPhoneTap(BuildContext context) {
    final String phone = StreamChatCore.of(context)
        .client
        .state
        .currentUser!
        .extraData['phone'] as String;

    onMakeCall(phone);
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final Channel channel = StreamChannel.of(context).channel;
    return AppBar(
      elevation: 0.3,
      centerTitle: false,
      automaticallyImplyLeading: false,
      actions: <Widget>[
        IconButton(
          icon: Icon(FeatherIcons.phone, size: 3.8.h),
          onPressed: () => _onPhoneTap(context),
        ),
      ],
      title: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const BackButton(),
          CircleAvatar(
            radius: 3.5.h,
            backgroundImage: CachedNetworkImageProvider(
                ChatHelpers.getChannelImage(channel, user)),
          ),
          SizedBox(width: 4.0.w),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  ChatHelpers.getChannelName(channel, user),
                  overflow: TextOverflow.ellipsis,
                  style: theme.bodyMedium?.copyWith(
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(height: 0.5.h),
                BetterStreamBuilder<List<Member>>(
                  stream: channel.state!.membersStream,
                  initialData: channel.state!.members,
                  builder: (BuildContext context, List<Member> data) =>
                      ConnectionStatusBuilder(
                    statusBuilder:
                        (BuildContext context, ConnectionStatus status) {
                      switch (status) {
                        case ConnectionStatus.connected:
                          return _buildConnectedTitleState(context, data);
                        case ConnectionStatus.connecting:
                          return Text(
                            'Connecting..',
                            style: theme.bodyMedium?.copyWith(
                              fontWeight: FontWeight.bold,
                              fontSize: 14.5.sp,
                              color: Colors.green.shade800,
                            ),
                          );
                        case ConnectionStatus.disconnected:
                          return Text(
                            'Offline',
                            style: theme.bodyMedium?.copyWith(
                              fontWeight: FontWeight.bold,
                              fontSize: 14.5.sp,
                              color: Colors.red,
                            ),
                          );
                        default:
                          return const SizedBox.shrink();
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildConnectedTitleState(
      BuildContext context, List<Member>? members) {
    final TextTheme theme = Theme.of(context).textTheme;
    Widget? alternativeWidget;
    final Channel channel = StreamChannel.of(context).channel;
    final int? memberCount = channel.memberCount;
    if (memberCount != null && memberCount > 2) {
      final String text = 'Members: $memberCount';
      alternativeWidget = Text(text);
    } else {
      final Member? otherMember = members?.firstWhereOrNull(
        (Member member) => member.userId != user.uid,
      );

      if (otherMember != null) {
        if (otherMember.user?.online == true) {
          alternativeWidget = Text(
            'Online',
            style: theme.bodyText2!.copyWith(
              fontWeight: FontWeight.bold,
              fontSize: 14.5.sp,
              color: Colors.green.shade800,
            ),
          );
        } else {
          String lastSeen;
          final DateTime? lastActive = otherMember.user?.lastActive;
          if (lastActive == null) {
            lastSeen = 'unknown';
          } else {
            lastSeen = lastActive.toLocal().timeago();
          }
          alternativeWidget = Text(
            'Last seen $lastSeen',
            style: theme.bodyText2!.copyWith(
              fontWeight: FontWeight.bold,
              fontSize: 14.5.sp,
              color: Colors.grey,
            ),
          );
        }
      }
    }

    return TypingIndicator(
      alternativeWidget: alternativeWidget,
    );
  }
}

class TypingIndicator extends StatelessWidget {
  final Widget? alternativeWidget;

  const TypingIndicator({Key? key, this.alternativeWidget}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ChannelClientState channelState =
        StreamChannel.of(context).channel.state!;
    final Widget altWidget = alternativeWidget ?? const SizedBox.shrink();

    return BetterStreamBuilder<Iterable<User>>(
      initialData: channelState.typingEvents.keys,
      stream: channelState.typingEventsStream.map((Map<User, Event> typings) =>
          typings.entries.map((MapEntry<User, Event> e) => e.key)),
      builder: (BuildContext context, Iterable<User> data) {
        return Align(
          alignment: Alignment.centerLeft,
          child: AnimatedSwitcher(
            duration: const Duration(milliseconds: 300),
            child: data.isNotEmpty == true
                ? const Align(
                    alignment: Alignment.centerLeft,
                    key: ValueKey<String>('typing-text'),
                    child: Text(
                      'Typing message..',
                      maxLines: 1,
                      style: TextStyle(
                        fontSize: 10,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  )
                : Align(
                    alignment: Alignment.centerLeft,
                    key: const ValueKey<String>('altwidget'),
                    child: altWidget,
                  ),
          ),
        );
      },
    );
  }
}

class ConnectionStatusBuilder extends StatelessWidget {
  final Stream<ConnectionStatus>? connectionStatusStream;

  final Widget Function(BuildContext context, Object? error)? errorBuilder;

  final WidgetBuilder? loadingBuilder;

  final Widget Function(BuildContext context, ConnectionStatus status)
      statusBuilder;

  const ConnectionStatusBuilder({
    Key? key,
    required this.statusBuilder,
    this.connectionStatusStream,
    this.errorBuilder,
    this.loadingBuilder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Stream<ConnectionStatus> stream = connectionStatusStream ??
        StreamChatCore.of(context).client.wsConnectionStatusStream;
    final StreamChatClient client = StreamChatCore.of(context).client;
    return BetterStreamBuilder<ConnectionStatus>(
      initialData: client.wsConnectionStatus,
      stream: stream,
      noDataBuilder: loadingBuilder,
      errorBuilder: (BuildContext context, Object error) {
        if (errorBuilder != null) {
          return errorBuilder!(context, error);
        }
        return const Offstage();
      },
      builder: statusBuilder,
    );
  }
}
