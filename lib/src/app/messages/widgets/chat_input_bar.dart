import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:stream_chat_flutter_core/stream_chat_flutter_core.dart';

import '../../../common/helper/helper.dart';
import '../../../cubits/chat_cubit/chat_cubit.dart';

class ChatInputBar extends StatefulWidget {
  final AppUser user;
  const ChatInputBar({super.key, required this.user});

  @override
  State<ChatInputBar> createState() => _ChatInputBarState();
}

class _ChatInputBarState extends State<ChatInputBar> {
  final TextEditingController _controller = TextEditingController();

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Future<void> _onSendMessage() async {
    final Channel channel = StreamChannel.of(context).channel;
    if (_controller.text.isNotEmpty) {
      channel.sendMessage(Message(text: _controller.text));
      context.read<ChatCubit>().sendChatNotification(
            sender: widget.user.name,
            message: _controller.text,
            recipientId: ChatHelpers.getOtherRecipientId(channel, widget.user),
          );

      _controller.clear();
      FocusScope.of(context).unfocus();
    }
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 1.0.w),
      height: 11.4.h,
      color: Colors.white,
      child: Row(
        children: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.photo_outlined, size: 4.2.h),
            color: Colors.teal.shade400,
          ),
          Expanded(
            child: TextField(
              controller: _controller,
              onChanged: (String val) {
                StreamChannel.of(context).channel.keyStroke();
              },
              style: theme.bodyLarge?.copyWith(fontWeight: FontWeight.w500),
              cursorColor: Theme.of(context).primaryColor,
              textCapitalization: TextCapitalization.sentences,
              decoration: const InputDecoration(
                hintText: 'Type message...',
                border: InputBorder.none,
              ),
              maxLines: null,
              onSubmitted: (_) => _onSendMessage(),
            ),
          ),
          IconButton(
            onPressed: _onSendMessage,
            icon: Icon(Icons.send_outlined, size: 4.2.h),
            color: Colors.teal.shade400,
          ),
        ],
      ),
    );
  }
}
