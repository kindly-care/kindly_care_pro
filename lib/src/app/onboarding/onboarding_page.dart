

// ignore_for_file: depend_on_referenced_packages

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../common/common.dart';

class OnboardingPage extends StatefulWidget {
  const OnboardingPage({super.key});

  @override
  State<OnboardingPage> createState() => _OnboardingState();
}

class _OnboardingState extends State<OnboardingPage>
    with AutomaticKeepAliveClientMixin {
  List<Slide> _getSlides() {
    final TextTheme theme = Theme.of(context).textTheme;
    final TextStyle styleTitle = theme.titleLarge!.copyWith(fontSize: 21.8.sp);
    final TextStyle? styleDescription = Theme.of(context).textTheme.bodyLarge;
    return <Slide>[
      Slide(
        backgroundColor: Colors.white,
        title: 'Work Profile',
        styleTitle: styleTitle,
        description:
            'Make yourself stand out with a detailed work profile. Complete with work history, qualifications, etc.',
        styleDescription: styleDescription,
        centerWidget: Image.asset(
          'assets/images/following.gif',
          height: 35.0.h,
          width: 55.w,
          gaplessPlayback: true,
        ),
      ),
      Slide(
        backgroundColor: Colors.white,
        title: 'Manage Tasks',
        styleTitle: styleTitle,
        description: 'Manage all your assigned tasks and todos in one place.',
        styleDescription: styleDescription,
        centerWidget: Image.asset(
          'assets/images/tasks.gif',
          height: 35.0.h,
          width: 55.w,
          gaplessPlayback: true,
        ),
      ),
      Slide(
        backgroundColor: Colors.white,
        title: 'Record Keeping',
        styleTitle: styleTitle,
        description:
            "Keep all the patient's records in one place. From health vitals, medications, meals etc.",
        styleDescription: styleDescription,
        centerWidget: Image.asset(
          'assets/images/notes.gif',
          height: 35.0.h,
          width: 55.w,
          gaplessPlayback: true,
        ),
      ),
      Slide(
        backgroundColor: Colors.white,
        title: 'Automated Timelogs',
        styleTitle: styleTitle,
        description:
            'Never worry about filling timelogs again. Your timelogs will be automatically updated when you Clock-Out after a shift.',
        styleDescription: styleDescription,
        centerWidget: Image.asset(
          'assets/images/time.gif',
          height: 35.0.h,
          width: 55.w,
          gaplessPlayback: true,
        ),
      ),
    ];
  }

  @override
  void initState() {
    super.initState();
  }

  Future<void> _onDonePress() async {
    await PermissionHelper().requestPermission();
    final SharedPreferences prefs = SharedPrefs.instance;
    final bool isApFirstRun = prefs.getBool(kIsAppFirstRun) ?? true;
    if (isApFirstRun) {
      prefs.setBool(kIsAppFirstRun, false);
    }
    if (mounted) {
      Navigator.popAndPushNamed(context, kLoginPageRoute);
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(0.0),
        child: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.white,
          automaticallyImplyLeading: false,
          systemOverlayStyle: const SystemUiOverlayStyle(
            statusBarColor: Colors.white,
            statusBarIconBrightness: Brightness.dark,
          ),
        ),
      ),
      body: IntroSlider(
        slides: _getSlides(),
        onDonePress: _onDonePress,
        onSkipPress: _onDonePress,
        colorDot: Colors.grey.shade400,
        backgroundColorAllSlides: Colors.white,
        scrollPhysics: const BouncingScrollPhysics(),
        colorActiveDot: Theme.of(context).primaryColor,
        renderNextBtn: const _ActionButton(title: 'Next'),
        renderSkipBtn: const _ActionButton(title: 'Skip'),
        renderDoneBtn: const _ActionButton(title: 'Done'),
        renderPrevBtn: const _ActionButton(title: 'Back'),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class _ActionButton extends StatelessWidget {
  final String title;
  const _ActionButton({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Text(
      title,
      style: theme.bodyLarge!.copyWith(
        fontWeight: FontWeight.w600,
        color: Theme.of(context).primaryColor,
      ),
    );
  }
}
