// ignore_for_file: library_private_types_in_public_api

import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class RemoteImagePicker extends StatefulWidget {
  final List<File> localImages;
  final List<String> remoteImages;
  final Function(List<File> local, List<String> remote) onDone;

  const RemoteImagePicker({
    super.key,
    required this.onDone,
    required this.localImages,
    required this.remoteImages,
  });

  @override
  _RemoteImagePicker createState() => _RemoteImagePicker();
}

class _RemoteImagePicker extends State<RemoteImagePicker> {
  late List<File> _local;
  late List<String> _remote;
  final double _space = 10.0;

  @override
  void initState() {
    super.initState();
    _local = widget.localImages;
    _remote = widget.remoteImages;
  }

  void _onRemoveLocalImage(File file) {
    setState(() => _local.remove(file));
  }

  void _onRemoveRemoteImage(String image) {
    setState(() => _remote.remove(image));
  }

  List<Widget> _widgets() {
    final List<Widget> widgets = <Widget>[];
    for (final String image in _remote) {
      widgets.add(
        _RemoteImageItem(
          image: image,
          onRemoveImage: _onRemoveRemoteImage,
        ),
      );
    }

    for (final File file in _local) {
      widgets.add(
        _LocalImageItem(
          image: file,
          onRemoveImage: _onRemoveLocalImage,
        ),
      );
    }

    return widgets
      ..add(
        _AddImageButton(
          onPressed: () async {
            try {
              _local = await ImageService.getImages();
              setState(() {
                widget.onDone(_local, _remote);
              });
            } catch (e) {
              showToast(message: e.toString());
            }
          },
        ),
      );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 2.0.w),
      child: Wrap(
        direction: Axis.horizontal,
        spacing: _space,
        runSpacing: _space,
        children: _widgets(),
      ),
    );
  }
}

class _AddImageButton extends StatelessWidget {
  final VoidCallback onPressed;
  const _AddImageButton({Key? key, required this.onPressed}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    return Container(
      height: height * 0.16,
      width: height * 0.16,
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.black12,
        ),
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: IconButton(
        icon: Icon(
          Icons.add,
          color: const Color(0xFF969799),
          size: height * 0.04,
        ),
        onPressed: onPressed,
      ),
    );
  }
}

class _LocalImageItem extends StatelessWidget {
  final File image;
  final Function(File) onRemoveImage;

  const _LocalImageItem({
    Key? key,
    required this.image,
    required this.onRemoveImage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    return Stack(
      clipBehavior: Clip.antiAlias,
      children: <Widget>[
        ClipRRect(
          borderRadius: BorderRadius.circular(4.0),
          child: Image.file(
            image,
            height: height * 0.16,
            width: height * 0.16,
            gaplessPlayback: true,
            fit: BoxFit.cover,
            // quality: 100,
          ),
        ),
        Positioned(
          right: 0,
          top: 0,
          child: InkWell(
            borderRadius: BorderRadius.circular(999.0),
            child: Icon(
              Icons.cancel,
              color: const Color(0xFFD6D6D6),
              size: height * 0.025,
            ),
            onTap: () => onRemoveImage(image),
          ),
        )
      ],
    );
  }
}

class _RemoteImageItem extends StatelessWidget {
  final String image;
  final Function(String) onRemoveImage;

  const _RemoteImageItem({
    Key? key,
    required this.image,
    required this.onRemoveImage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    return Stack(
      clipBehavior: Clip.antiAlias,
      children: <Widget>[
        ClipRRect(
          borderRadius: BorderRadius.circular(6.0),
          child: CachedNetworkImage(
            imageUrl: image,
            fit: BoxFit.cover,
            height: height * 0.16,
            width: height * 0.16,
            placeholder: (_, __) => const Image(
              image: AssetImage('assets/images/loading3.gif'),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Positioned(
          right: 0,
          top: 0,
          child: InkWell(
            borderRadius: BorderRadius.circular(999.0),
            child: Icon(
              Icons.cancel,
              color: const Color(0xFFD6D6D6),
              size: height * 0.025,
            ),
            onTap: () => onRemoveImage(image),
          ),
        )
      ],
    );
  }
}
