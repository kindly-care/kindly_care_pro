// ignore_for_file: prefer_if_null_operators

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class TextFieldComponent extends StatelessWidget {
  final bool readOnly;
  final bool isNumber;
  final bool isEmail;
  final bool hasDecimal;
  final bool isRequired;
  final String? hintText;
  final String labelText;
  final String prefixText;
  final String suffixText;
  final VoidCallback? onTap;
  final int minLines;
  final int? maxLength;
  final Function(String)? onChanged;
  final VoidCallback? onEditingComplete;
  final TextInputAction? textInputAction;
  final TextEditingController controller;
  final Function(String)? onFieldSubmitted;
  final String? Function(String? value)? validator;

  const TextFieldComponent({
    super.key,
    this.onTap,
    this.hintText,
    this.onChanged,
    this.validator,
    this.prefixText = '',
    this.suffixText = '',
    this.onFieldSubmitted,
    this.minLines = 1,
    this.maxLength = 845,
    this.isNumber = false,
    this.readOnly = false,
    this.onEditingComplete,
    this.isRequired = false,
    this.isEmail = false,
    this.hasDecimal = false,
    this.textInputAction = TextInputAction.next,
    required this.labelText,
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 1.0.w),
          child: Row(
            children: <Widget>[
              Text(labelText,
                  style: const TextStyle(fontWeight: FontWeight.w500)),
              Visibility(
                  visible: isRequired,
                  child: const Text('*', style: TextStyle(color: Colors.red)))
            ],
          ),
        ),
        SizedBox(height: 0.6.h),
        CupertinoTextField(
          onTap: onTap,
          maxLines: 4,
          minLines: minLines,
          maxLength: maxLength,
          onChanged: onChanged,
          controller: controller,
          onSubmitted: onFieldSubmitted,
          textInputAction: TextInputAction.next,
          cursorColor: Theme.of(context).primaryColor,
          textCapitalization: TextCapitalization.words,
          keyboardType: _getTextInputType(),
          inputFormatters: isNumber
              ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly]
              : null,
          placeholder: hintText,
          prefix: Padding(
            padding: EdgeInsets.only(left: 2.0.w),
            child: Text(prefixText),
          ),
          suffix: Padding(
            padding: EdgeInsets.only(right: 2.0.w),
            child: Text(suffixText),
          ),
          padding: EdgeInsets.symmetric(vertical: 1.5.h, horizontal: 1.5.w),
        ),
      ],
    );
  }

  TextInputType _getTextInputType() {
    if (isNumber || hasDecimal) {
      return TextInputType.number;
    } else if (isEmail) {
      return TextInputType.emailAddress;
    } else if (minLines != 0) {
      return TextInputType.multiline;
    } else {
      return TextInputType.text;
    }
  }
}
