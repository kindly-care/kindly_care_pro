import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../calender_page/widgets/outline_check.dart';

class CareTaskActionCard extends StatelessWidget {
  final CareTask task;
  final VoidCallback onTap;
  final CareActionStatus status;

  const CareTaskActionCard(this.task,
      {super.key, required this.onTap, required this.status});

  String _getTaskTitle() {
    final CareTask careTask = task;
    if (careTask is MedicationTask) {
      return '${careTask.title} (${careTask.dosage} ${careTask.metric})';
    } else {
      return careTask.title;
    }
  }

  @override
  Widget build(BuildContext context) {
    final String time = DateFormat.Hm().format(task.time);
    final TextTheme theme = Theme.of(context).textTheme;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 2.5.w, vertical: 0.8.h),
      child: ListTile(
        onTap: onTap,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.5)),
        contentPadding: EdgeInsets.only(right: 0.0.w),
        leading: OutlineCheck(status: status),
        tileColor: Colors.white,
        title: Padding(
          padding: EdgeInsets.only(bottom: 0.5.h),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                _getTaskTitle(),
                overflow: TextOverflow.ellipsis,
                style: theme.subtitle1!.copyWith(
                  fontWeight: FontWeight.w600,
                  fontSize: 17.5.sp,
                  decoration: status == CareActionStatus.none
                      ? TextDecoration.none
                      : TextDecoration.lineThrough,
                ),
              ),
              Container(
                margin: EdgeInsets.only(right: 2.8.w),
                padding: EdgeInsets.symmetric(horizontal: 0.5.w),
                decoration: BoxDecoration(
                  color: status == CareActionStatus.incomplete
                      ? Colors.red.shade50
                      : Colors.teal.shade50,
                  borderRadius: BorderRadius.circular(4.5),
                ),
                child: Text(task is MedicationTask ? 'N/A' : time,
                    style: theme.subtitle1),
              ),
            ],
          ),
        ),
        subtitle: Text(
          task.description ?? 'No additional information given.',
          maxLines: 3,
          overflow: TextOverflow.ellipsis,
          style: theme.subtitle2!.copyWith(
            decoration: status == CareActionStatus.none
                ? TextDecoration.none
                : TextDecoration.lineThrough,
          ),
        ),
      ),
    );
  }
}
