part of 'location_bloc.dart';

abstract class LocationState extends Equatable {
  const LocationState();

  @override
  List<Object> get props => <Object>[];
}

class LocationInitial extends LocationState {}

class FetchLocationInProgress extends LocationState {}

class FetchLocationSuccess extends LocationState {
  final LatLng location;
  const FetchLocationSuccess({required this.location});

  @override
  List<Object> get props => <Object>[location];
}

class FetchLocationError extends LocationState {
  final String error;
  const FetchLocationError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

class FetchDirectionsInProgress extends LocationState {}

class FetchDirectionsSuccess extends LocationState {
  final Directions directions;
  const FetchDirectionsSuccess({required this.directions});

  @override
  List<Object> get props => <Object>[directions];
}

class FetchDirectionsError extends LocationState {
  final String error;
  const FetchDirectionsError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}
