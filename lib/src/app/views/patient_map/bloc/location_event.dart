part of 'location_bloc.dart';

abstract class LocationEvent extends Equatable {
  const LocationEvent();

  @override
  List<Object> get props => <Object>[];
}

class FetchCurrentLocation extends LocationEvent {}

class FetchLocationFromAddress extends LocationEvent {
  final String address;
  const FetchLocationFromAddress({required this.address});

  @override
  List<Object> get props => <Object>[address];
}

class FetchDirections extends LocationEvent {
  final LatLng destination;
  const FetchDirections({required this.destination});

  @override
  List<Object> get props => <Object>[destination];
}
