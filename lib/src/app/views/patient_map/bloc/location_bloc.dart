import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../../common/directions/directions.dart';
import '../../../../data/local/location/location_repository.dart';

part 'location_event.dart';
part 'location_state.dart';

class LocationBloc extends Bloc<LocationEvent, LocationState> {
  final LocationRepository _locationRepository;

  LocationBloc({required LocationRepository locationRepository})
      : _locationRepository = locationRepository,
        super(LocationInitial()) {
    on<FetchCurrentLocation>(_onFetchCurrentLocation);
    on<FetchLocationFromAddress>(_onFetchLocationFromAddress);
    on<FetchDirections>(_onFetchDirections);
  }

  Future<void> _onFetchCurrentLocation(
      FetchCurrentLocation event, Emitter<LocationState> emit) async {
    emit(FetchLocationInProgress());
    try {
      final LatLng location = await _locationRepository.getCurrentPosition();
      emit(FetchLocationSuccess(location: location));
    } catch (_) {
      emit(const FetchLocationError(error: 'Failed to get location'));
    }
  }

  Future<void> _onFetchLocationFromAddress(
      FetchLocationFromAddress event, Emitter<LocationState> emit) async {
    emit(FetchLocationInProgress());
    try {
      final LatLng location =
          await _locationRepository.getPositionFromArea(event.address);
      emit(FetchLocationSuccess(location: location));
    } catch (_) {
      emit(const FetchLocationError(error: 'Failed to get location'));
    }
  }

  Future<void> _onFetchDirections(
      FetchDirections event, Emitter<LocationState> emit) async {
    emit(FetchDirectionsInProgress());
    try {
      final Directions directions =
          await _locationRepository.fetchDirections(event.destination);

      emit(FetchDirectionsSuccess(directions: directions));
    } on LocationException catch (e) {
      emit(FetchDirectionsError(error: e.toString()));
    }
  }
}
