import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:simple_speed_dial/simple_speed_dial.dart';

import '../../../common/common.dart';
import '../../../data/local/local.dart';
import 'bloc/location_bloc.dart';

class PatientMapPage extends StatelessWidget {
  final Patient patient;

  const PatientMapPage({super.key, required this.patient});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<LocationBloc>(
      create: (BuildContext context) => LocationBloc(
        locationRepository: context.read<LocationService>(),
      ),
      child: _PatientMapView(patient),
    );
  }
}

class _PatientMapView extends StatefulWidget {
  final Patient patient;

  const _PatientMapView(this.patient, {Key? key}) : super(key: key);

  @override
  PatientMapViewState createState() => PatientMapViewState();
}

class PatientMapViewState extends State<_PatientMapView>
    with AutomaticKeepAliveClientMixin {
  Directions? _directions;
  late LocationBloc _bloc;
  late LatLng _patientHome;
  final double _zoom = 13.4746;
  MapType _mapType = MapType.normal;
  late GoogleMapController _controller;
  Set<Polyline> _polylines = <Polyline>{};
  final Set<Marker> _markers = <Marker>{};

  @override
  void initState() {
    super.initState();
    _patientHome = LatLng(
      widget.patient.address!.latitude!,
      widget.patient.address!.longitude!,
    );
    _markers.add(
      Marker(
        position: _patientHome,
        markerId: MarkerId(widget.patient.uid),
        infoWindow: InfoWindow(title: _getInfoWindowTitle()),
      ),
    );
    _bloc = context.read<LocationBloc>();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _onMapTypeChanged() {
    setState(() {
      _mapType = _mapType == MapType.normal ? MapType.hybrid : MapType.normal;
    });
  }

  String _getInfoWindowTitle() {
    final String name = widget.patient.name.firstName();
    return "$name's Home";
  }

  void _getDirections() {
    if (_directions == null) {
      _bloc.add(FetchDirections(destination: _patientHome));
    } else {
      _directions = null;
      _polylines = <Polyline>{};
      _markers
          .removeWhere((Marker marker) => marker.markerId.value == 'marker');
      setState(() {});
    }
  }

  void _onDirectionsError(BuildContext context, String message) {
    showTwoButtonDialog(
      context,
      title: 'Directions Not Found',
      content: message,
      buttonText1: 'Retry',
      buttonText2: 'Exit',
      onPressed1: () {
        Navigator.pop(context);
        _getDirections();
      },
      onPressed2: () => Navigator.pop(context),
    );
  }

  void _onLocationListener(BuildContext context, LocationState state) {
    if (state is FetchDirectionsError) {
      _onDirectionsError(context, state.error);
    } else if (state is FetchDirectionsSuccess) {
      _directions = state.directions;
      _polylines = _directions!.polylines;
      _markers.add(
        Marker(
          position: state.directions.location,
          markerId: const MarkerId('marker'),
          icon:
              BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueAzure),
          infoWindow: const InfoWindow(title: 'My Location'),
        ),
      );
      _controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
            target: _directions!.location,
            zoom: 14.4746,
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final bool isLoading = context.select((LocationBloc bloc) => bloc.state)
        is FetchDirectionsInProgress;
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark,
        ),
      ),
      body: BlocListener<LocationBloc, LocationState>(
        listener: _onLocationListener,
        child: LoadingOverlay(
          isLoading: isLoading,
          progressIndicator: const LoadingIndicator(),
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Positioned.fill(
                child: GoogleMap(
                  mapToolbarEnabled: false,
                  compassEnabled: true,
                  zoomControlsEnabled: false,
                  polylines: _polylines,
                  markers: _markers,
                  mapType: _mapType,
                  initialCameraPosition: CameraPosition(
                    target: _patientHome,
                    zoom: _zoom,
                  ),
                  onMapCreated: (GoogleMapController controller) =>
                      _controller = controller,
                ),
              ),
              Positioned(
                top: 14.5.h,
                child: _AddressWidget(
                  widget.patient.address?.fullAddress() ?? kUnknownAddress,
                ),
              ),
              Positioned(
                right: 5.0.w,
                bottom: 14.0.h,
                child: SpeedDial(
                  closedBackgroundColor: Colors.white,
                  closedForegroundColor: Colors.black,
                  openBackgroundColor: Colors.white,
                  speedDialChildren: <SpeedDialChild>[
                    SpeedDialChild(
                      child: const Icon(Boxicons.bx_map_alt),
                      foregroundColor: Colors.black,
                      backgroundColor: Colors.white,
                      label: _mapType == MapType.normal
                          ? 'Satellite Map'
                          : 'Normal Map',
                      onPressed: _onMapTypeChanged,
                    ),
                    SpeedDialChild(
                      child: const Icon(Icons.directions_outlined),
                      foregroundColor: Colors.black,
                      backgroundColor: Colors.white,
                      label:
                          _directions == null ? 'Directions' : 'Directions OFF',
                      onPressed: _getDirections,
                    ),
                  ],
                  child: const Icon(Icons.control_camera_outlined),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class _AddressWidget extends StatelessWidget {
  final String address;
  const _AddressWidget(this.address, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 1.0.h,
        horizontal: 3.4.w,
      ),
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.circular(20.0),
        boxShadow: const <BoxShadow>[
          BoxShadow(color: Colors.black12, offset: Offset(0, 2))
        ],
      ),
      child: Text(
        address,
        overflow: TextOverflow.ellipsis,
        style: theme.bodyMedium?.copyWith(color: Colors.white),
      ),
    );
  }
}
