import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../common/actions/actions.dart';
import '../../widgets/widgets.dart';

enum _PopupAction { call, message }

class KinProfilePage extends StatelessWidget {
  final AppUser kin;
  const KinProfilePage({super.key, required this.kin});

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.dark,
        ),
        actions: <Widget>[
          _PopupButton(kin),
        ],
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: <Widget>[
            SizedBox(height: 9.5.h),
            CircleAvatar(
              radius: 12.5.w,
              backgroundImage: CachedNetworkImageProvider(kin.avatar),
            ),
            SizedBox(height: 2.5.h),
            Text(kin.name, style: theme.headline6),
            SizedBox(height: 6.5.h),
            AboutListTileWidget(
              title: 'Name',
              subtitle: kin.name,
              icon: Icons.account_circle_outlined,
            ),
            AboutListTileWidget(
              icon: MdiIcons.genderMaleFemale,
              title: 'Gender',
              subtitle: _getGender(),
            ),
            AboutListTileWidget(
              title: 'Phone',
              subtitle: kin.phone,
              icon: Icons.phone_outlined,
            ),
            AboutListTileWidget(
              title: 'Email',
              subtitle: kin.email,
              icon: Icons.email_outlined,
              onTap: () => onEmail(kin.email ?? ''),
            ),
            AboutListTileWidget(
              title: 'Home Address',
              subtitle: _getAddress(),
              icon: Boxicons.bx_home_heart,
            ),
          ],
        ),
      ),
    );
  }

  String _getGender() {
    return kin.gender ?? 'Unknown';
  }

  String _getAddress() {
    return kin.address?.fullAddress() ?? 'N/A';
  }
}

class _PopupButton extends StatelessWidget {
  final AppUser kin;
  const _PopupButton(this.kin, {Key? key}) : super(key: key);

  void _onPopupAction(BuildContext context, _PopupAction action, AppUser user) {
    switch (action) {
      case _PopupAction.call:
        onMakeCall(kin.phone);
        break;
      case _PopupAction.message:
        createChannel(context, uid: user.uid, otherId: kin.uid);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    return PopupMenuButton<_PopupAction>(
      icon: Icon(Icons.more_vert_outlined, size: 3.8.h),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7.0)),
      onSelected: (_PopupAction value) => _onPopupAction(context, value, user),
      itemBuilder: (BuildContext context) {
        return <PopupMenuEntry<_PopupAction>>[
          const PopupMenuItem<_PopupAction>(
            value: _PopupAction.call,
            child: IconMenuItem(icon: Icons.call_outlined, title: 'Call'),
          ),
          const PopupMenuDivider(),
          const PopupMenuItem<_PopupAction>(
            value: _PopupAction.message,
            child: IconMenuItem(icon: Icons.email_outlined, title: 'Message'),
          ),
        ];
      },
    );
  }
}
