import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../common/actions/actions.dart';
import '../../widgets/widgets.dart';

enum _PopupAction { call, message }

class MemberProfilePage extends StatelessWidget {
  final AppUser user;
  const MemberProfilePage({super.key, required this.user});

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.dark,
        ),
        actions: <Widget>[
          _PopupButton(user),
        ],
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: <Widget>[
            SizedBox(height: 9.5.h),
            CircleAvatar(
              radius: 12.5.w,
              backgroundImage: CachedNetworkImageProvider(user.avatar),
            ),
            SizedBox(height: 2.5.h),
            Text(user.name, style: theme.headline6),
            SizedBox(height: 6.5.h),
            AboutListTileWidget(
              title: 'Name',
              subtitle: user.name,
              icon: Icons.account_circle_outlined,
            ),
            AboutListTileWidget(
              icon: MdiIcons.genderMaleFemale,
              title: 'Gender',
              subtitle: user.gender ?? 'Unknown',
            ),
            AboutListTileWidget(
              title: 'Phone',
              subtitle: user.phone,
              icon: Icons.phone_outlined,
            ),
            AboutListTileWidget(
              title: 'Email',
              subtitle: user.email,
              icon: Icons.email_outlined,
              onTap: () => onEmail(user.email ?? 'Unknown email'),
            ),
            AboutListTileWidget(
              title: 'Home Address',
              subtitle: user.address?.fullAddress() ?? kUnknownAddress,
              icon: Boxicons.bx_home_heart,
            ),
          ],
        ),
      ),
    );
  }
}

class _PopupButton extends StatelessWidget {
  final AppUser member;
  const _PopupButton(this.member, {Key? key}) : super(key: key);

  void _onPopupAction(BuildContext context, _PopupAction action, AppUser user) {
    switch (action) {
      case _PopupAction.call:
        onMakeCall(member.phone);
        break;
      case _PopupAction.message:
        createChannel(context, uid: member.uid, otherId: user.uid);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    return PopupMenuButton<_PopupAction>(
      icon: Icon(Icons.more_vert_outlined, size: 3.8.h),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7.0)),
      onSelected: (_PopupAction action) =>
          _onPopupAction(context, action, user),
      itemBuilder: (BuildContext context) {
        return <PopupMenuEntry<_PopupAction>>[
          const PopupMenuItem<_PopupAction>(
            value: _PopupAction.call,
            child: IconMenuItem(icon: Icons.phone_outlined, title: 'Call'),
          ),
          const PopupMenuDivider(),
          const PopupMenuItem<_PopupAction>(
            value: _PopupAction.message,
            child: IconMenuItem(icon: Icons.email_outlined, title: 'Message'),
          ),
        ];
      },
    );
  }
}
