import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../blocs/care_provider/care_provider_bloc.dart';
import '../../../../common/common.dart';
import '../../../../data/services/care_provider/care_provider_service.dart';
import '../about_tab/about_tab.dart';
import '../patients_tab/patients_tab.dart';
import '../reviews_tab/reviews_tab.dart';

enum _PopupAction { edit }

class CareProviderProfilePage extends StatelessWidget {
  final String careProviderId;

  const CareProviderProfilePage({super.key, required this.careProviderId});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CareProviderBloc>(
      create: (BuildContext context) => CareProviderBloc(
          careProviderRepository: context.read<CareProviderService>())
        ..add(FetchCareProvider(careProviderId: careProviderId)),
      child: _CareProviderProfilePageView(careProviderId),
    );
  }
}

class _CareProviderProfilePageView extends StatelessWidget {
  final String careProviderId;
  const _CareProviderProfilePageView(this.careProviderId, {Key? key})
      : super(key: key);

  void _onFetchCareProvider(BuildContext context) {
    context
        .read<CareProviderBloc>()
        .add(FetchCareProvider(careProviderId: careProviderId));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<CareProviderBloc, CareProviderState>(
        buildWhen: (_, CareProviderState state) {
          return state is CareProviderLoading ||
              state is CareProviderLoadSuccess ||
              state is CareProviderLoadError;
        },
        builder: (BuildContext context, CareProviderState state) {
          if (state is CareProviderLoadError) {
            return LoadErrorWidget(
              message: state.error,
              onRetry: () => _onFetchCareProvider(context),
            );
          } else if (state is CareProviderLoadSuccess) {
            if (state.careProvider == null) {
              return const NoDataWidget('Care Provider not found.');
            } else {
              return _PageContent(state.careProvider!);
            }
          } else {
            return const LoadingIndicator();
          }
        },
      ),
    );
  }
}

class _PageContent extends StatefulWidget {
  final CareProvider careProvider;
  const _PageContent(this.careProvider, {Key? key}) : super(key: key);

  @override
  _PageContentState createState() => _PageContentState();
}

class _PageContentState extends State<_PageContent>
    with SingleTickerProviderStateMixin {
  late TabController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Future<void> _onEditButtonTap() async {
    Navigator.pushNamed(context, kEditCareProviderPageRoute,
        arguments: widget.careProvider);
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final String occupation = widget.careProvider.occupation.join(', ');
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.dark,
        ),
        actions: <Widget>[_PopupButton(onEdit: _onEditButtonTap)],
      ),
      body: SingleChildScrollView(
        physics: const NeverScrollableScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 9.5.h),
            CircleAvatar(
              radius: 12.5.w,
              backgroundImage: CachedNetworkImageProvider(
                widget.careProvider.avatar,
              ),
            ),
            SizedBox(height: 2.0.h),
            Text(widget.careProvider.name, style: theme.titleLarge),
            SizedBox(height: 0.5.h),
            Text(occupation, style: theme.bodyLarge),
            SizedBox(height: 1.0.h),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                ColoredPill(title: getAvailability(widget.careProvider)),
                Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 2.8.w, vertical: 0.8.h),
                  margin: EdgeInsets.symmetric(horizontal: 1.0.w),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12.0),
                    color: Colors.teal.shade100,
                  ),
                  child: RatingStar(rating: widget.careProvider.rating),
                ),
                Visibility(
                  visible: widget.careProvider.address?.city != null,
                  child: ColoredPill(
                      title: widget.careProvider.address?.city ?? ''),
                ),
              ],
            ),
            SizedBox(height: 1.5.h),
            TabBar(
              controller: _controller,
              physics: const NeverScrollableScrollPhysics(),
              labelStyle: TextStyle(
                fontSize: 19.1.sp,
                fontWeight: FontWeight.w500,
              ),
              labelPadding: EdgeInsets.symmetric(horizontal: 8.5.w),
              indicatorColor: Theme.of(context).primaryColor,
              labelColor: Colors.black,
              indicatorSize: TabBarIndicatorSize.tab,
              unselectedLabelColor: Colors.grey,
              isScrollable: true,
              tabs: const <Widget>[
                Tab(text: 'Patients'),
                Tab(text: 'Reviews'),
                Tab(text: 'About'),
              ],
            ),
            SizedBox(
              height: 52.6.h,
              child: TabBarView(
                physics: const BouncingScrollPhysics(),
                controller: _controller,
                children: <Widget>[
                  PatientsTab(careProvider: widget.careProvider),
                  ReviewsTab(careProviderId: widget.careProvider.uid),
                  AboutTab(widget.careProvider),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _PopupButton extends StatelessWidget {
  final VoidCallback onEdit;
  const _PopupButton({Key? key, required this.onEdit}) : super(key: key);

  void _onPopupAction(BuildContext context, _PopupAction action, AppUser user) {
    switch (action) {
      case _PopupAction.edit:
        onEdit();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    return PopupMenuButton<_PopupAction>(
      icon: Icon(Icons.more_vert_outlined, size: 3.8.h),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7.0)),
      onSelected: (_PopupAction value) => _onPopupAction(context, value, user),
      itemBuilder: (BuildContext context) {
        return <PopupMenuEntry<_PopupAction>>[
          const PopupMenuItem<_PopupAction>(
            value: _PopupAction.edit,
            child: IconMenuItem(icon: Icons.edit, title: 'Edit'),
          ),
        ];
      },
    );
  }
}
