import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/material.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../widgets/list_tiles/list_tiles.dart';

class AboutTab extends StatelessWidget {
  final CareProvider careProvider;
  const AboutTab(this.careProvider, {super.key});

  String _getGenderAge() {
    if (careProvider.gender != null && careProvider.dob == null) {
      return careProvider.gender!;
    } else if (careProvider.dob != null && careProvider.gender != null) {
      final int age = DateTime.now().year - careProvider.dob!.year;
      return '${careProvider.gender} ($age years old)';
    } else {
      return 'N/A';
    }
  }

  String _getAddress() {
    final Address? address = careProvider.address;
    if (address == null) {
      return 'N/A';
    } else {
      final int number = address.number;
      final String street = address.streetName;
      final String suburb = address.suburb;
      final String city = address.city;

      if (number != 0 &&
          street.isNotEmpty &&
          suburb.isNotEmpty &&
          city.isNotEmpty) {
        return address.fullAddress();
      } else {
        return address.townAddress();
      }
    }
  }

  String _getYearsActive() {
    if (careProvider.yearStarted != null) {
      final int age = DateTime.now().year - careProvider.yearStarted!;
      return age.toString();
    } else {
      return 'N/A';
    }
  }

  @override
  Widget build(BuildContext context) {
    final TextStyle? style = Theme.of(context).textTheme.titleSmall?.copyWith(
          fontWeight: FontWeight.w500,
          color: Colors.black.withOpacity(0.65),
        );

    final TextStyle? tileStyle = Theme.of(context)
        .textTheme
        .titleMedium
        ?.copyWith(color: Colors.black.withOpacity(0.7));
    return ListView(
      physics: const BouncingScrollPhysics(),
      padding: EdgeInsets.all(3.0.w),
      children: <Widget>[
        AvailabilityListTile(
          title: 'Occupation',
          icon: Icons.medical_services_outlined,
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: careProvider.occupation
                .map((String qualification) => Padding(
                      padding: EdgeInsets.symmetric(vertical: 0.2.h),
                      child: Text(qualification, style: style),
                    ))
                .toList(),
          ),
        ),
        AboutListTileWidget(
          icon: MdiIcons.genderMaleFemale,
          title: 'Gender',
          subtitle: _getGenderAge(),
        ),
        AboutListTileWidget(
          icon: MdiIcons.accountReactivateOutline,
          title: 'Years active',
          subtitle: _getYearsActive(),
        ),
        AboutListTileWidget(
          icon: Icons.place_outlined,
          title: 'Address',
          subtitle: _getAddress(),
        ),
        AboutListTileWidget(
          title: 'Phone',
          icon: FeatherIcons.phone,
          subtitle: careProvider.phone,
          onTap: () => onMakeCall(careProvider.phone),
        ),
        AboutListTileWidget(
          title: 'Email',
          icon: FeatherIcons.mail,
          subtitle: careProvider.email,
          onTap: () => onEmail(careProvider.email ?? 'Unknown email'),
        ),
        AboutListTileWidget(
          icon: Icons.masks_outlined,
          title: 'Covid Vaccinated',
          subtitle: careProvider.isCovidVaccinated ? 'Yes' : 'No',
        ),
        AboutListTileWidget(
          title: 'Driver License',
          icon: MdiIcons.cardAccountDetailsOutline,
          subtitle: careProvider.hasDriverLicense ? 'Yes' : 'No',
        ),
        AvailabilityListTile(
          title: 'Medical Qualifications',
          icon: Icons.school_outlined,
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: careProvider.certificates
                .map((String qualification) => Padding(
                      padding: EdgeInsets.symmetric(vertical: 0.2.h),
                      child: Text(qualification, style: style),
                    ))
                .toList(),
          ),
        ),
        AboutListTileWidget(
          title: 'Services',
          icon: Icons.manage_accounts_outlined,
          subtitle: careProvider.services.join(', '),
        ),
        AboutListTileWidget(
          title: 'Duties',
          icon: Icons.assignment_outlined,
          subtitle: careProvider.duties.join(', '),
        ),
        SizedBox(height: 2.0.h),
        AvailabilityListTile(
          title: 'Availability',
          subtitle: Column(
            children: <Widget>[
              SizedBox(height: 0.8.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Sunday:', style: style),
                  Text(careProvider.sundayAvailability, style: style),
                ],
              ),
              SizedBox(height: 1.5.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Monday:', style: style),
                  Text(careProvider.mondayAvailability, style: style),
                ],
              ),
              SizedBox(height: 1.5.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Tuesday:', style: style),
                  Text(careProvider.tuesdayAvailability, style: style),
                ],
              ),
              SizedBox(height: 1.5.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Wednesday:', style: style),
                  Text(careProvider.wednesdayAvailability, style: style),
                ],
              ),
              SizedBox(height: 1.5.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Thursday:', style: style),
                  Text(careProvider.thursdayAvailability, style: style),
                ],
              ),
              SizedBox(height: 1.5.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Friday:', style: style),
                  Text(careProvider.fridayAvailability, style: style),
                ],
              ),
              SizedBox(height: 1.5.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Saturday:', style: style),
                  Text(careProvider.saturdayAvailability, style: style),
                ],
              ),
            ],
          ),
          icon: Icons.date_range_outlined,
        ),
        SizedBox(height: 2.0.h),
        AboutListTileWidget(
          title: 'Bio',
          subtitle: careProvider.bio,
          icon: Icons.info_outline,
        ),
        Visibility(
          visible: careProvider.images.isNotEmpty,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 3.5.h),
              Padding(
                padding: EdgeInsets.only(left: 3.5.w),
                child: Text('Photos', style: tileStyle),
              ),
              SizedBox(height: 2.0.h),
              ImageContainer(imageURLs: careProvider.images),
            ],
          ),
        ),
      ],
    );
  }
}
