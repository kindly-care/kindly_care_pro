import 'package:cached_network_image/cached_network_image.dart';
import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../blocs/care_provider/care_provider_bloc.dart';
import '../../../../common/constants/constants.dart';

class PatientsTab extends StatefulWidget {
  final CareProvider careProvider;
  const PatientsTab({super.key, required this.careProvider});

  @override
  PatientsTabState createState() => PatientsTabState();
}

class PatientsTabState extends State<PatientsTab>
    with AutomaticKeepAliveClientMixin {
  @override
  void initState() {
    super.initState();
    _loadPatients();
  }

  void _loadPatients() {
    context.read<CareProviderBloc>().add(FetchAssignedPatients(
        patientIds: widget.careProvider.assignedPatients.keys.toList()));
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<CareProviderBloc, CareProviderState>(
      buildWhen: (_, CareProviderState state) {
        return state is AssignedPatientsLoading ||
            state is AssignedPatientsLoadSuccess ||
            state is AssignedPatientsLoadError;
      },
      builder: (BuildContext context, CareProviderState state) {
        if (state is AssignedPatientsLoadError) {
          return LoadErrorWidget(message: state.error, onRetry: _loadPatients);
        } else if (state is AssignedPatientsLoadSuccess) {
          return _TabContent(
              patients: state.patients, careProvider: widget.careProvider);
        } else {
          return const LoadingIndicator();
        }
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class _TabContent extends StatefulWidget {
  final List<Patient> patients;
  final CareProvider careProvider;
  const _TabContent(
      {Key? key, required this.careProvider, required this.patients})
      : super(key: key);

  @override
  State<_TabContent> createState() => _TabContentState();
}

class _TabContentState extends State<_TabContent>
    with AutomaticKeepAliveClientMixin {
  void _onPatientTap(Patient patient) {
    Navigator.pushNamed(context, kPatientProfilePageRoute,
        arguments: patient.uid);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    final TextTheme theme = Theme.of(context).textTheme;
    if (widget.patients.isEmpty) {
      return const NoDataWidget(
          'You currently do not have any patients. Your patients will be displayed here.');
    } else {
      return ListView.builder(
        shrinkWrap: true,
        itemCount: widget.patients.length,
        padding: EdgeInsets.only(top: 1.2.h, left: 1.5.w, right: 1.5.w),
        physics: const BouncingScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          final Patient patient = widget.patients[index];
          return Padding(
            padding: EdgeInsets.all(1.2.h),
            child: ListTile(
              onTap: () => _onPatientTap(patient),
              onLongPress: () => _onPatientLongPress(patient),
              leading: CircleAvatar(
                radius: 3.8.h,
                backgroundImage: CachedNetworkImageProvider(patient.avatar),
              ),
              title: Text(
                patient.name,
                overflow: TextOverflow.ellipsis,
                style: theme.titleMedium,
              ),
              subtitle: Text(
                patient.address?.suburb ?? kUnknownAddress,
                overflow: TextOverflow.ellipsis,
                style: theme.titleSmall,
              ),
              trailing: Icon(Icons.chevron_right, size: 3.8.h),
            ),
          );
        },
      );
    }
  }

  void _onPatientLongPress(Patient patient) {
    HapticFeedback.lightImpact();
    optionsBottomSheet(
      context: context,
      title: patient.name,
      height: 32.0.h,
      options: <Widget>[
        IconListTile(
          title: 'Call',
          showTrailing: false,
          icon: FeatherIcons.phone,
          onTap: () {
            Navigator.pop(context);
            onMakeCall(patient.phone);
          },
        ),
        Divider(indent: 18.w),
        IconListTile(
          title: 'View Profile',
          showTrailing: false,
          icon: FeatherIcons.user,
          onTap: () {
            Navigator.pop(context);
            _onPatientTap(patient);
          },
        ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
