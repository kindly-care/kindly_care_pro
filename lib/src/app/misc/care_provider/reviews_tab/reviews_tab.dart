import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../common/common.dart';
import '../../../../data/services/care_provider/care_provider_service.dart';
import 'bloc/review_bloc.dart';

class ReviewsTab extends StatelessWidget {
  final String careProviderId;
  const ReviewsTab({super.key, required this.careProviderId});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ReviewBloc>(
      create: (BuildContext context) => ReviewBloc(
        careProviderRepository: context.read<CareProviderService>(),
      )..add(FetchReviews(careProviderId: careProviderId)),
      child: _ReviewsTabView(careProviderId),
    );
  }
}

class _ReviewsTabView extends StatelessWidget {
  final String careProviderId;
  const _ReviewsTabView(this.careProviderId, {Key? key}) : super(key: key);

  void _loadReferences(BuildContext context) {
    context
        .read<ReviewBloc>()
        .add(FetchReviews(careProviderId: careProviderId));
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ReviewBloc, ReviewState>(
      builder: (BuildContext context, ReviewState state) {
        if (state is ReviewsLoadError) {
          return LoadErrorWidget(
            message: state.error,
            onRetry: () => _loadReferences(context),
          );
        } else if (state is ReviewsLoadSuccess) {
          return _TabContent(
              careProviderId: careProviderId, reviews: state.reviews);
        } else {
          return const LoadingIndicator();
        }
      },
    );
  }
}

class _TabContent extends StatefulWidget {
  final String careProviderId;
  final List<Review> reviews;
  const _TabContent(
      {Key? key, required this.careProviderId, required this.reviews})
      : super(key: key);

  @override
  _TabContentState createState() => _TabContentState();
}

class _TabContentState extends State<_TabContent>
    with AutomaticKeepAliveClientMixin {
  String _getMessage(AppUser user) {
    if (widget.careProviderId == user.uid) {
      return 'You do not have any reviews yet. Your reviews will be displayed here.';
    } else {
      return 'No reviews here yet.';
    }
  }

  void _onReviewTap(Review review, AppUser user) {
    final TextTheme theme = Theme.of(context).textTheme;
    final TextStyle? style =
        theme.bodyText1?.copyWith(color: Theme.of(context).primaryColor);
    showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) {
        return CupertinoActionSheet(
          title: Text(
            review.name,
            style:
                theme.bodyText1?.copyWith(color: Colors.black.withOpacity(0.6)),
          ),
          actions: <CupertinoActionSheetAction>[
            CupertinoActionSheetAction(
              child: Text('Call', style: style),
              onPressed: () {
                Navigator.pop(context);
                onMakeCall(review.phone);
              },
            ),
            CupertinoActionSheetAction(
              child: Text('Message', style: style),
              onPressed: () {
                Navigator.pop(context);
                createChannel(context, uid: user.uid, otherId: review.authorId);
              },
            ),
            CupertinoActionSheetAction(
              child: Text('Send Email', style: style),
              onPressed: () {
                Navigator.pop(context);
                onEmail(review.phone);
              },
            ),
          ],
          cancelButton: CupertinoActionSheetAction(
            child: Text('Cancel', style: style),
            onPressed: () => Navigator.pop(context),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    if (widget.reviews.isEmpty) {
      return NoDataWidget(_getMessage(user));
    } else {
      return ListView.builder(
        physics: const BouncingScrollPhysics(),
        padding: EdgeInsets.zero,
        itemCount: widget.reviews.length,
        itemBuilder: (BuildContext context, int index) {
          final Review review = widget.reviews[index];
          return _ReviewCard(
            review: review,
            onTap: () => _onReviewTap(review, user),
          );
        },
      );
    }
  }

  @override
  bool get wantKeepAlive => true;
}

class _ReviewCard extends StatelessWidget {
  final Review review;
  final VoidCallback onTap;
  const _ReviewCard({Key? key, required this.review, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final String createdAt = DateFormat.yMd('en_GB').format(review.createdAt);
    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: EdgeInsets.only(
          left: 3.8.w,
          right: 3.8.w,
          top: 2.5.h,
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(12.0),
          child: Material(
            elevation: 0.8,
            child: SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.all(1.2.h),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          review.name,
                          style: theme.subtitle2?.copyWith(fontSize: 17.5.sp),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 0.5.w),
                          decoration: BoxDecoration(
                            color: Colors.teal.shade50,
                            borderRadius: BorderRadius.circular(4.5),
                          ),
                          child: Text(
                            createdAt,
                            style: theme.subtitle2!.copyWith(
                              color: Colors.black.withOpacity(0.6),
                              fontSize: 17.sp,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 0.5.h),
                    RatingBar.builder(
                      initialRating: review.rating,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemSize: 2.1.h,
                      unratedColor: Colors.grey.shade400,
                      itemBuilder: (BuildContext context, _) =>
                          const Icon(Icons.star, color: Color(0xFFFE7404)),
                      onRatingUpdate: (_) {},
                    ),
                    SizedBox(height: 1.0.h),
                    Text(
                      review.text,
                      style: theme.bodyText2,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
