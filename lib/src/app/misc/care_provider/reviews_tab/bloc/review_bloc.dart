import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../../../data/services/care_provider/care_provider_repository.dart';

part 'review_event.dart';
part 'review_state.dart';

class ReviewBloc extends Bloc<ReviewEvent, ReviewState> {
  final CareProviderRepository _careProviderRepository;
  ReviewBloc({required CareProviderRepository careProviderRepository})
      : _careProviderRepository = careProviderRepository,
        super(ReviewsInitial()) {
    on<FetchReviews>(_onFetchReviews);
  }

  Future<void> _onFetchReviews(
      FetchReviews event, Emitter<ReviewState> emit) async {
    emit(ReviewsLoading());

    try {
      final List<Review> reviews = await _careProviderRepository
          .fetchCareProviderReviews(event.careProviderId);
      emit(ReviewsLoadSuccess(reviews: reviews));
    } on FetchDataException catch (e) {
      emit(ReviewsLoadError(error: e.toString()));
    }
  }
}
