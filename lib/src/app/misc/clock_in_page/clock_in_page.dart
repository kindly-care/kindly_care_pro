import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../blocs/clock_in/clock_in_bloc.dart';
import '../../../common/common.dart';
import '../../../cubits/account_cubit/account_cubit.dart';

class ClockInPage extends StatelessWidget {
  final CareProvider careProvider;

  const ClockInPage({super.key, required this.careProvider});

  Future<void> _onClockInTap(BuildContext context, Patient patient) async {
    context
        .read<ClockInBloc>()
        .add(ClockIn(careProvider: careProvider, patient: patient));
  }

  @override
  Widget build(BuildContext context) {
    final List<Patient> patients =
        context.select((AccountCubit cubit) => cubit.state.patients);
    final bool isLoading =
        context.select((ClockInBloc bloc) => bloc.state) is ClockInInProgress;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Clock In'),
      ),
      body: patients.isEmpty
          ? const NoDataWidget(kNoCurrentPatients)
          : LoadingOverlay(
              isLoading: isLoading,
              child: BlocListener<ClockInBloc, ClockInState>(
                listener: (BuildContext context, ClockInState state) {
                  if (state is ClockInError) {
                    switch (state.errorType) {
                      case ClockInErrorType.distance:
                        _showDistantDialog(context, state.error);
                        break;
                      case ClockInErrorType.location:
                        _showLocationErrorDialog(context, state.error);
                        break;
                      case ClockInErrorType.network:
                        errorSnackbar(context, state.error);
                        break;
                    }
                  } else if (state is ClockInSuccess) {
                    Navigator.pop(context, state.patientId);
                    successSnackbar(
                        context, 'You have successfully Clocked In.');
                  }
                },
                child: ListView.separated(
                  itemCount: patients.length,
                  physics: const BouncingScrollPhysics(),
                  padding: EdgeInsets.symmetric(horizontal: 2.0.w),
                  separatorBuilder: (_, __) =>
                      Divider(indent: 2.5.w, endIndent: 2.5.w),
                  itemBuilder: (BuildContext context, int index) {
                    final Patient patient = patients[index];
                    return PatientCard(
                      patient: patient,
                      onLongPress: () {},
                      onTap: () => _onClockInTap(context, patient),
                    );
                  },
                ),
              ),
            ),
    );
  }

  void _showDistantDialog(BuildContext context, String message) {
    showOneButtonDialog(
      context,
      title: 'Not Allowed',
      content: message,
      buttonText: 'Ok',
      onPressed: () => Navigator.pop(context),
    );
  }

  void _showLocationErrorDialog(BuildContext context, String message) {
    showTwoButtonDialog(
      context,
      barrierDismissible: false,
      title: 'Location Error',
      content: message,
      buttonText1: 'Exit',
      buttonText2: 'Settings',
      onPressed1: () => Navigator.pop(context),
      onPressed2: () {
        Navigator.pop(context);
        Geolocator.openLocationSettings();
      },
    );
  }
}
