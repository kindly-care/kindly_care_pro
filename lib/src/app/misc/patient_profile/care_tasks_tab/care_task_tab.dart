import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../blocs/care_task/care_task_bloc.dart';

class CareTaskTab extends StatefulWidget {
  final Patient patient;
  const CareTaskTab({super.key, required this.patient});

  @override
  CareTasksTabState createState() => CareTasksTabState();
}

class CareTasksTabState extends State<CareTaskTab>
    with AutomaticKeepAliveClientMixin {
  @override
  void initState() {
    super.initState();
    _loadCareTasks();
  }

  void _loadCareTasks() {
    context
        .read<CareTaskBloc>()
        .add(FetchCareTasks(patientId: widget.patient.uid));
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<CareTaskBloc, CareTaskState>(
      buildWhen: (_, CareTaskState state) {
        return state is CareTasksLoading ||
            state is CareTasksLoadSuccess ||
            state is CareTasksLoadError;
      },
      builder: (BuildContext context, CareTaskState state) {
        if (state is CareTasksLoadError) {
          return LoadErrorWidget(message: state.error, onRetry: _loadCareTasks);
        } else if (state is CareTasksLoadSuccess) {
          return _TabContent(tasks: state.tasks, patient: widget.patient);
        } else {
          return const LoadingIndicator();
        }
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class _TabContent extends StatelessWidget {
  final Patient patient;
  final List<CareTask> tasks;
  const _TabContent({Key? key, required this.patient, required this.tasks})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (tasks.isEmpty) {
      return const NoDataWidget('No Care Tasks found.');
    } else {
      tasks.sortBy((CareTask task) => task.time);
      tasks
          .sort((CareTask a, CareTask b) => a.time.hour.compareTo(b.time.hour));
      return ListView.builder(
        physics: const BouncingScrollPhysics(),
        shrinkWrap: true,
        padding: EdgeInsets.all(3.0.w),
        itemCount: tasks.length,
        itemBuilder: (BuildContext context, int index) {
          final CareTask task = tasks[index];
          return CareTaskCard(task);
        },
      );
    }
  }
}
