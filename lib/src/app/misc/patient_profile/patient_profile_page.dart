import 'package:cached_network_image/cached_network_image.dart';
import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../blocs/patient/patient_bloc.dart';
import '../../../common/constants/constants.dart';
import 'care_tasks_tab/care_task_tab.dart';
import 'circle_tab/circle_tab.dart';
import 'patient_info_tab/patient_info_tab.dart';
import 'patient_records_page/patient_records_page.dart';

enum _PopupAction { stats, reports }

class PatientProfilePage extends StatefulWidget {
  final String patientId;
  const PatientProfilePage({super.key, required this.patientId});

  @override
  State<PatientProfilePage> createState() => _PatientProfilePageState();
}

class _PatientProfilePageState extends State<PatientProfilePage> {
  @override
  void initState() {
    super.initState();
    _fetchPatient();
  }

  void _fetchPatient() {
    context.read<PatientBloc>().add(FetchPatient(patientId: widget.patientId));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<PatientBloc, PatientState>(
        buildWhen: (_, PatientState state) {
          return state is PatientLoading ||
              state is PatientLoadSuccess ||
              state is PatientLoadError;
        },
        builder: (BuildContext context, PatientState state) {
          if (state is PatientLoadError) {
            return LoadErrorWidget(
                message: state.error, onRetry: _fetchPatient);
          } else if (state is PatientLoadSuccess) {
            if (state.patient == null) {
              return const NoDataWidget('Patient not found.');
            } else {
              return _PageContent(state.patient!);
            }
          } else {
            return const LoadingIndicator();
          }
        },
      ),
    );
  }
}

class _PageContent extends StatefulWidget {
  final Patient patient;
  const _PageContent(this.patient, {Key? key}) : super(key: key);

  @override
  _PageContentState createState() => _PageContentState();
}

class _PageContentState extends State<_PageContent>
    with SingleTickerProviderStateMixin {
  late Patient _patient;
  late TabController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TabController(length: 3, vsync: this);
    _patient = widget.patient;
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  List<String> _getConditions() {
    return _patient.conditions.length < 3
        ? _patient.conditions
        : _patient.conditions.sublist(0, 3);
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.dark,
        ),
        actions: <Widget>[_PopupButton(_patient)],
      ),
      body: Column(
        children: <Widget>[
          SizedBox(height: 9.5.h),
          CircleAvatar(
            radius: 12.5.w,
            backgroundImage: CachedNetworkImageProvider(
              _patient.avatar,
            ),
          ),
          SizedBox(height: 2.0.h),
          Text(widget.patient.name, style: theme.titleLarge),
          SizedBox(height: 0.5.h),
          Text(
            widget.patient.address?.shortAddress() ?? kUnknownAddress,
            style: theme.bodyLarge,
          ),
          SizedBox(height: 1.0.h),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: _getConditions()
                .map((String condition) => ColoredPill(
                      title: condition,
                      margin: EdgeInsets.symmetric(horizontal: 1.0.w),
                    ))
                .toList(),
          ),
          SizedBox(height: 1.5.h),
          TabBar(
            controller: _controller,
            physics: const NeverScrollableScrollPhysics(),
            labelStyle: TextStyle(
              fontSize: 18.88.sp,
              fontWeight: FontWeight.w500,
            ),
            labelPadding: EdgeInsets.only(left: 8.5.w, right: 8.5.w),
            indicatorColor: Theme.of(context).primaryColor,
            labelColor: Colors.black,
            indicatorSize: TabBarIndicatorSize.tab,
            unselectedLabelColor: Colors.grey,
            isScrollable: true,
            tabs: const <Widget>[
              Tab(text: 'About'),
              Tab(text: 'Care Tasks'),
              Tab(text: 'Circle'),
            ],
          ),
          SizedBox(
            height: 52.6.h,
            child: TabBarView(
              physics: const BouncingScrollPhysics(),
              controller: _controller,
              children: <Widget>[
                PatientInfoTab(patient: _patient),
                CareTaskTab(patient: _patient),
                CircleTab(patient: _patient),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _PopupButton extends StatelessWidget {
  final Patient patient;
  const _PopupButton(this.patient, {Key? key}) : super(key: key);

  void _handlePopupAction(
      BuildContext context, _PopupAction action, AppUser user) {
    switch (action) {
      case _PopupAction.stats:
        Navigator.push(
          context,
          CupertinoPageRoute<void>(
            builder: (_) => PatientRecordsPage(
              patientId: patient.uid,
              patientName: patient.name,
            ),
          ),
        );
        break;
      case _PopupAction.reports:
        Navigator.of(context)
            .pushNamed(kPatientReportsPageRoute, arguments: patient);
        break;
      default:
    }
  }

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    return PopupMenuButton<_PopupAction>(
      icon: Icon(Icons.more_vert_outlined, size: 3.8.h),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7.0)),
      onSelected: (_PopupAction value) =>
          _handlePopupAction(context, value, user),
      itemBuilder: (BuildContext context) {
        return <PopupMenuEntry<_PopupAction>>[
          const PopupMenuItem<_PopupAction>(
            value: _PopupAction.stats,
            child: IconMenuItem(
                icon: FeatherIcons.activity, title: 'Health Vitals'),
          ),
          const PopupMenuDivider(),
          const PopupMenuItem<_PopupAction>(
            value: _PopupAction.reports,
            child:
                IconMenuItem(icon: Boxicons.bx_notepad, title: 'Care Reports'),
          ),
        ];
      },
    );
  }
}
