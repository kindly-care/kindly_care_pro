import 'package:dart_date/dart_date.dart';
import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../common/common.dart';
import '../../../../data/services/services.dart';

import 'bloc/patient_reports_bloc.dart';
import 'report_detail_page/report_detail_page.dart';
import 'widgets/report_tile.dart';

class PatientReportsPage extends StatelessWidget {
  final Patient patient;

  const PatientReportsPage({super.key, required this.patient});

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    final int month = DateTime.now().month;
    return BlocProvider<PatientReportsBloc>(
      create: (BuildContext context) => PatientReportsBloc(
        reportRepository: context.read<ReportService>(),
      )..add(FetchReports(month: month, patientId: patient.uid)),
      child: _PatientReportsPageView(user: user, patient: patient),
    );
  }
}

class _PatientReportsPageView extends StatefulWidget {
  final AppUser user;
  final Patient patient;

  const _PatientReportsPageView(
      {Key? key, required this.user, required this.patient})
      : super(key: key);

  @override
  State<_PatientReportsPageView> createState() =>
      _PatientReportsPageViewState();
}

class _PatientReportsPageViewState extends State<_PatientReportsPageView> {
  int _month = DateTime.now().month;

  void _loadReports() {
    context
        .read<PatientReportsBloc>()
        .add(FetchReports(month: _month, patientId: widget.patient.uid));
  }

  @override
  Widget build(BuildContext context) {
    final String month =
        DateFormat.yMMMM('en_GB').format(DateTime.now().setMonth(_month));
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text(month),
        actions: <Widget>[
          MonthPopupButton(
            icon: Icons.calendar_today_outlined,
            onChanged: (int month) {
              setState(() => _month = month);
              _loadReports();
            },
          ),
        ],
      ),
      body: BlocBuilder<PatientReportsBloc, PatientReportsState>(
        buildWhen: (_, PatientReportsState state) {
          return state is PatientReportsLoading ||
              state is PatientReportsLoadSuccess ||
              state is PatientReportsLoadError;
        },
        builder: (BuildContext context, PatientReportsState state) {
          if (state is PatientReportsLoadError) {
            return LoadErrorWidget(
                message: 'Failed to load data', onRetry: _loadReports);
          } else if (state is PatientReportsLoadSuccess) {
            return _PageContent(
              month: _month,
              reports: state.reports,
              patient: widget.patient,
            );
          } else {
            return const LoadingIndicator();
          }
        },
      ),
    );
  }
}

class _PageContent extends StatelessWidget {
  final int month;
  final Patient patient;
  final List<Report> reports;
  const _PageContent({
    Key? key,
    required this.month,
    required this.patient,
    required this.reports,
  }) : super(key: key);

  void _onReportTap(BuildContext context, Report report) {
    Navigator.of(context).push(
      CupertinoPageRoute<void>(
        builder: (_) => ReportDetailPage(
          patientId: patient.uid,
          reportId: report.reportId,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    if (reports.isEmpty) {
      final DateTime date = DateTime.now().setMonth(month);
      final String monthYear = DateFormat.yMMMM('en_GB').format(date);
      return NoDataWidget('No Care Reports found for $monthYear.');
    } else {
      return ListView.separated(
        physics: const BouncingScrollPhysics(),
        separatorBuilder: (_, __) => Divider(indent: 2.0.w, endIndent: 2.0.w),
        itemCount: reports.length,
        itemBuilder: (BuildContext context, int index) {
          final Report report = reports[index];
          return Padding(
            padding: EdgeInsets.symmetric(horizontal: 2.5.w, vertical: 0.8.h),
            child: ReportTile(
              report: report,
              onTap: () => _onReportTap(context, report),
              onLongPress: () => _onLongPress(context, report, user.uid),
            ),
          );
        },
      );
    }
  }

  void _onLongPress(BuildContext context, Report report, String uid) {
    HapticFeedback.lightImpact();
    optionsBottomSheet(
      context: context,
      title: report.careProviderName,
      height: 32.0.h,
      options: <Widget>[
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            onMakeCall(report.careProviderPhone);
          },
          title: 'Call',
          showTrailing: false,
          icon: FeatherIcons.phone,
        ),
        Divider(indent: 18.w),
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            createChannel(context, uid: uid, otherId: report.careProviderId);
          },
          title: 'Message',
          showTrailing: false,
          icon: FeatherIcons.messageSquare,
        ),
      ],
    );
  }
}
