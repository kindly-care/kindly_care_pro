import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../../common/common.dart';
import '../../../../../data/services/services.dart';
import '../bloc/patient_reports_bloc.dart';
import '../widgets/widgets.dart';

enum _PopupAction { call, message }

class ReportDetailPage extends StatelessWidget {
  final String patientId;
  final String reportId;

  const ReportDetailPage(
      {super.key, required this.reportId, required this.patientId});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PatientReportsBloc>(
      create: (BuildContext context) => PatientReportsBloc(
        reportRepository: context.read<ReportService>(),
      )..add(FetchReport(reportId: reportId, patientId: patientId)),
      child: _ReportDetailPageView(reportId: reportId, patientId: patientId),
    );
  }
}

class _ReportDetailPageView extends StatelessWidget {
  final String reportId;
  final String patientId;
  const _ReportDetailPageView(
      {Key? key, required this.reportId, required this.patientId})
      : super(key: key);

  void _loadReport(BuildContext context) {
    context
        .read<PatientReportsBloc>()
        .add(FetchReport(reportId: reportId, patientId: patientId));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<PatientReportsBloc, PatientReportsState>(
        buildWhen: (_, PatientReportsState state) {
          return state is PatientReportLoading ||
              state is PatientReportLoadSuccess ||
              state is PatientReportLoadError;
        },
        builder: (BuildContext context, PatientReportsState state) {
          if (state is PatientReportLoadError) {
            return LoadErrorWidget(
              message: state.error,
              onRetry: () => _loadReport(context),
            );
          } else if (state is PatientReportLoadSuccess) {
            final Report? report = state.report;
            if (report == null) {
              return const NoDataWidget('Report not found.');
            } else {
              return _PageContent(report);
            }
          } else {
            return const LoadingIndicator();
          }
        },
      ),
    );
  }
}

class _PageContent extends StatelessWidget {
  final Report report;
  const _PageContent(this.report, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(DateFormat.yMMMMd('en_GB').format(report.createdAt)),
        actions: <Widget>[
          _PopupActionWidget(report),
        ],
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 3.5.w, vertical: 1.5.h),
        physics: const BouncingScrollPhysics(),
        children: <Widget>[
          SizedBox(height: 2.0.h),
          _TimeReport(report),
          SizedBox(height: 3.0.h),
          _MealReport(report),
          SizedBox(height: 3.0.h),
          _MedicationReport(report),
          SizedBox(height: 3.0.h),
          _OtherTasksReport(report),
          SizedBox(height: 3.0.h),
          _VitalsReport(report),
          SizedBox(height: 3.0.h),
          _AdditionalInformation(report),
          SizedBox(height: 3.0.h),
          _AttachedImages(report),
          SizedBox(height: 3.0.h),
        ],
      ),
    );
  }
}

class _TimeReport extends StatelessWidget {
  final Report report;
  const _TimeReport(this.report, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final TextStyle? style = theme.bodyMedium;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Time',
          style: theme.bodyLarge
              ?.copyWith(fontWeight: FontWeight.w700, fontSize: 18.5.sp),
        ),
        Divider(thickness: 0.5.w, endIndent: 3.5.w),
        SizedBox(height: 1.0.h),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('Clocked In', style: style),
            Text(_getShiftStartedTime(), style: style),
          ],
        ),
        SizedBox(height: 5.0.h),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('Clocked Out', style: style),
            Text(_getShiftFinishedTime(), style: style),
          ],
        ),
      ],
    );
  }

  String _getShiftStartedTime() {
    final DateTime? startTime = report.timelog?.startTime;
    if (startTime != null) {
      return DateFormat('EEE d MMMM,', 'en_GB').add_jm().format(startTime);
    } else {
      return '--';
    }
  }

  String _getShiftFinishedTime() {
    final DateTime? finishTime = report.timelog?.finishTime;
    if (finishTime != null) {
      return DateFormat('EEE d MMMM,', 'en_GB').add_jm().format(finishTime);
    } else {
      return '--';
    }
  }
}

class _MealReport extends StatelessWidget {
  final Report report;
  const _MealReport(this.report, {Key? key}) : super(key: key);

  List<Widget> _getMeals() {
    final List<Widget> widgets = <Widget>[];

    final List<MealAction> actions =
        report.careActions.whereType<MealAction>().toList();

    for (final MealAction action in actions) {
      widgets.add(MealTaskTile(action));
    }
    if (widgets.isEmpty) {
      widgets.add(const NoTaskTile('No Meal Tasks found.'));
    }

    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Meals',
          style: theme.bodyLarge
              ?.copyWith(fontWeight: FontWeight.w700, fontSize: 18.5.sp),
        ),
        Divider(thickness: 0.5.w, endIndent: 3.5.w),
        ..._getMeals(),
      ],
    );
  }
}

class _MedicationReport extends StatelessWidget {
  final Report report;
  const _MedicationReport(this.report, {Key? key}) : super(key: key);

  List<Widget> _getMedications() {
    final List<Widget> widgets = <Widget>[];

    final List<MedicationAction> actions =
        report.careActions.whereType<MedicationAction>().toList();

    for (final MedicationAction action in actions) {
      widgets.add(MedicationTaskTile(action));
    }
    if (widgets.isEmpty) {
      widgets.add(const NoTaskTile('No Medication Tasks found.'));
    }

    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Medication',
          style: theme.bodyLarge
              ?.copyWith(fontWeight: FontWeight.w700, fontSize: 18.5.sp),
        ),
        Divider(thickness: 0.5.w, endIndent: 3.5.w),
        ..._getMedications(),
      ],
    );
  }
}

class _OtherTasksReport extends StatelessWidget {
  final Report report;
  const _OtherTasksReport(this.report, {Key? key}) : super(key: key);

  List<Widget> _getCareActions(BuildContext context) {
    final List<Widget> widgets = <Widget>[];

    final List<CareAction> actions = report.careActions
        .where((CareAction action) =>
            action.taskType != CareTaskType.meal &&
            action.taskType != CareTaskType.medication)
        .toList();

    for (final CareAction action in actions) {
      widgets.add(TaskTile(action));
    }
    if (widgets.isEmpty) {
      widgets.add(const NoTaskTile('No Other Tasks found.'));
    }

    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Other Tasks',
          style: theme.bodyLarge
              ?.copyWith(fontWeight: FontWeight.w700, fontSize: 18.5.sp),
        ),
        Divider(thickness: 0.5.w, endIndent: 3.5.w),
        ..._getCareActions(context),
      ],
    );
  }
}

class _VitalsReport extends StatelessWidget {
  final Report report;
  const _VitalsReport(this.report, {Key? key}) : super(key: key);

  Widget _getTemperature(BuildContext context) {
    final TemperatureRecord? temperature = report.temperature;
    if (temperature != null) {
      return VitalTaskTile(
        vital: 'Temperature',
        recording: '${temperature.temperature} °C',
        takenAt: temperature.takenAt,
      );
    } else {
      return const VitalTaskTile(vital: 'Temperature', recording: 'No Entry');
    }
  }

  Widget _getWeight(BuildContext context) {
    final WeightRecord? record = report.weight;
    if (record != null) {
      return VitalTaskTile(
        vital: 'Weight',
        recording: '${record.weight} Kg',
        takenAt: record.takenAt,
      );
    } else {
      return const VitalTaskTile(vital: 'Weight', recording: 'No Entry');
    }
  }

  Widget _getBloodGlucose(BuildContext context) {
    final BloodGlucoseRecord? record = report.bloodGlucose;
    if (record != null) {
      return VitalTaskTile(
        vital: 'Blood Glucose',
        recording: '${record.bloodGlucose} mmol/L',
        takenAt: record.takenAt,
      );
    } else {
      return const VitalTaskTile(vital: 'Blood Glucose', recording: 'No Entry');
    }
  }

  Widget _getBloodPressure(BuildContext context) {
    final BloodPressureRecord? record = report.bloodPressure;

    if (record != null) {
      return VitalTaskTile(
        vital: 'Blood Pressure',
        recording:
            '${record.systolic.truncate()}/${record.diastolic.truncate()} mmHg',
        takenAt: record.takenAt,
      );
    } else {
      return const VitalTaskTile(
          vital: 'Blood Pressure', recording: 'No Entry');
    }
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Vitals',
          style: theme.bodyLarge
              ?.copyWith(fontWeight: FontWeight.w700, fontSize: 18.5.sp),
        ),
        Divider(thickness: 0.5.w, endIndent: 3.5.w),
        _getWeight(context),
        _getTemperature(context),
        _getBloodGlucose(context),
        _getBloodPressure(context),
      ],
    );
  }
}

class _AdditionalInformation extends StatelessWidget {
  final Report report;
  const _AdditionalInformation(this.report, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Additional Information',
          style: theme.bodyLarge
              ?.copyWith(fontWeight: FontWeight.w700, fontSize: 18.5.sp),
        ),
        Divider(thickness: 0.5.w, endIndent: 3.5.w),
        GenericTaskTile(
            title: 'Patient In Pain',
            subtitle: report.patientInPain ? 'Yes' : 'No'),
        Visibility(
          visible: report.patientInPain,
          child: GenericTaskTile(
              title: 'Pain Source',
              subtitle: report.painLocation ?? 'No Entry'),
        ),
        GenericTaskTile(
            title: 'General Mood', subtitle: report.mood ?? 'No Entry'),
        GenericTaskTile(
          title: 'Care Summary',
          subtitlePadding: 0.8.h,
          subtitle: report.summary ?? 'No Care Summary provided.',
        ),
      ],
    );
  }
}

class _AttachedImages extends StatelessWidget {
  final Report report;
  const _AttachedImages(this.report, {Key? key}) : super(key: key);

  List<String> _getImageURLs() {
    return report.careActions
        .whereType<MealAction>()
        .map((MealAction action) => action.images)
        .flattened
        .toList()
      ..addAll(report.attachedPhotos);
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Photo Attachment(s)',
          style: theme.bodyLarge
              ?.copyWith(fontWeight: FontWeight.w700, fontSize: 18.5.sp),
        ),
        Divider(thickness: 0.5.w, endIndent: 3.5.w),
        SizedBox(height: 1.5.h),
        Builder(
          builder: (BuildContext context) {
            if (_getImageURLs().isEmpty) {
              return Text(
                'No attached photos in this report.',
                style: theme.titleSmall?.copyWith(fontWeight: FontWeight.w500),
              );
            } else {
              return ImageContainer(imageURLs: _getImageURLs());
            }
          },
        ),
      ],
    );
  }
}

class _PopupActionWidget extends StatelessWidget {
  final Report report;
  const _PopupActionWidget(this.report, {Key? key}) : super(key: key);

  void _onPopupAction(BuildContext context, _PopupAction action, AppUser user) {
    switch (action) {
      case _PopupAction.call:
        onMakeCall(report.careProviderPhone);
        break;
      case _PopupAction.message:
        createChannel(context, uid: user.uid, otherId: report.careProviderId);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    return PopupMenuButton<_PopupAction>(
      icon: Icon(Icons.more_vert_outlined, size: 3.8.h),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7.0)),
      onSelected: (_PopupAction value) => _onPopupAction(context, value, user),
      itemBuilder: (BuildContext context) {
        return <PopupMenuEntry<_PopupAction>>[
          PopupMenuItem<_PopupAction>(
            value: _PopupAction.call,
            child: Text(
              'Call ${report.careProviderName.firstName()}',
            ),
          ),
          PopupMenuItem<_PopupAction>(
            value: _PopupAction.message,
            child: Text(
              'Message ${report.careProviderName.firstName()}',
            ),
          ),
        ];
      },
    );
  }
}
