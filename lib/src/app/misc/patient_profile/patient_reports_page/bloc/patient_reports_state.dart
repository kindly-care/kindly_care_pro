part of 'patient_reports_bloc.dart';

abstract class PatientReportsState extends Equatable {
  const PatientReportsState();

  @override
  List<Object?> get props => <Object?>[];
}

class PatientReportsInitial extends PatientReportsState {}

class PatientReportsLoading extends PatientReportsState {}

class PatientReportsLoadSuccess extends PatientReportsState {
  final List<Report> reports;
  const PatientReportsLoadSuccess({required this.reports});

  @override
  List<Object> get props => <Object>[reports];
}

class PatientReportsLoadError extends PatientReportsState {
  final String error;
  const PatientReportsLoadError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

class PatientReportLoading extends PatientReportsState {}

class PatientReportLoadSuccess extends PatientReportsState {
  final Report? report;
  const PatientReportLoadSuccess({required this.report});

  @override
  List<Object?> get props => <Object?>[report];
}

class PatientReportLoadError extends PatientReportsState {
  final String error;
  const PatientReportLoadError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}
