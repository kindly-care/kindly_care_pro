part of 'patient_reports_bloc.dart';

abstract class PatientReportsEvent extends Equatable {
  const PatientReportsEvent();

  @override
  List<Object> get props => <Object>[];
}

class FetchReports extends PatientReportsEvent {
  final int month;
  final String patientId;
  const FetchReports({required this.month, required this.patientId});

  @override
  List<Object> get props => <Object>[month, patientId];
}

class FetchReport extends PatientReportsEvent {
  final String reportId, patientId;
  const FetchReport({required this.reportId, required this.patientId});

  @override
  List<Object> get props => <Object>[reportId, patientId];
}
