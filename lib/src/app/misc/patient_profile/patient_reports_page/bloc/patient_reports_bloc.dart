import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../../../data/services/report/report_repository.dart';

part 'patient_reports_event.dart';
part 'patient_reports_state.dart';

class PatientReportsBloc
    extends Bloc<PatientReportsEvent, PatientReportsState> {
  final ReportRepository _reportRepository;
  PatientReportsBloc({required ReportRepository reportRepository})
      : _reportRepository = reportRepository,
        super(PatientReportsInitial()) {
    on<FetchReports>(_onFetchReports);
    on<FetchReport>(_onFetchReport);
  }

  Future<void> _onFetchReports(
      FetchReports event, Emitter<PatientReportsState> emit) async {
    emit(PatientReportsLoading());

    try {
      final List<Report> reports = await _reportRepository.fetchReportsByMonth(
          patientId: event.patientId, month: event.month);

      reports.sort((Report a, Report b) => b.createdAt.compareTo(a.createdAt));

      emit(PatientReportsLoadSuccess(reports: reports));
    } on FetchDataException catch (e) {
      emit(PatientReportsLoadError(error: e.toString()));
    }
  }

  Future<void> _onFetchReport(
      FetchReport event, Emitter<PatientReportsState> emit) async {
    emit(PatientReportLoading());
    try {
      final Report? report = await _reportRepository.fetchReport(
          patientId: event.patientId, reportId: event.reportId);

      emit(PatientReportLoadSuccess(report: report));
    } on FetchDataException catch (e) {
      emit(PatientReportLoadError(error: e.toString()));
    }
  }
}
