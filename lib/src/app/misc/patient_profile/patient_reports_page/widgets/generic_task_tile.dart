import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class GenericTaskTile extends StatelessWidget {
  final String title;
  final String subtitle;
  final double subtitlePadding;
  const GenericTaskTile({
    super.key,
    required this.title,
    this.subtitlePadding = 0.0,
    required this.subtitle,
  });

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;

    return ListTile(
      contentPadding: EdgeInsets.symmetric(horizontal: 2.0.w),
      title: Text(
        title,
        style: theme.titleMedium
            ?.copyWith(fontWeight: FontWeight.w600, fontSize: 17.68.sp),
      ),
      subtitle: Padding(
        padding: EdgeInsets.only(top: subtitlePadding),
        child: Text(subtitle, style: theme.titleSmall),
      ),
    );
  }
}
