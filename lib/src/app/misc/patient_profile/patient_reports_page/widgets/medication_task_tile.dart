import 'package:flutter/material.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../../common/constants/constants.dart';

class MedicationTaskTile extends StatelessWidget {
  final MedicationAction action;
  const MedicationTaskTile(this.action, {super.key});

  String _getTaskTitle() {
    return '${action.title} (${action.dosage} ${action.metric})';
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final String comment = action.comment ?? kNoAdditionalInfo;
    return ListTile(
      contentPadding: EdgeInsets.symmetric(horizontal: 2.0.w),
      title: Text(
        _getTaskTitle(),
        overflow: TextOverflow.ellipsis,
        style: theme.titleMedium?.copyWith(fontSize: 17.68.sp),
      ),
      subtitle: action.comment != null
          ? Text(comment, style: theme.titleSmall)
          : null,
      trailing: Icon(
        action.status == CareActionStatus.complete
            ? Icons.check_box_outlined
            : Icons.disabled_by_default_outlined,
        color: action.status == CareActionStatus.complete
            ? Theme.of(context).primaryColor
            : Theme.of(context).errorColor,
      ),
    );
  }
}
