import 'package:flutter/material.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../../common/constants/constants.dart';

class MealTaskTile extends StatelessWidget {
  final MealAction action;
  const MealTaskTile(this.action, {super.key});

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final String comment = action.comment ?? kNoAdditionalInfo;
    final bool taskNotComplete = action.status == CareActionStatus.incomplete;
    return ListTile(
      contentPadding: EdgeInsets.symmetric(horizontal: 2.0.w),
      title: Text(
        action.title,
        style: theme.titleMedium
            ?.copyWith(fontWeight: FontWeight.w600, fontSize: 17.68.sp),
      ),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Visibility(
            visible: action.meal.isNotEmpty,
            child: Text(action.meal, style: theme.titleSmall),
          ),
          SizedBox(height: 0.2.h),
          Visibility(
            visible: action.comment != null,
            child: Text(
              taskNotComplete ? comment : 'Comment: $comment',
              style: theme.titleSmall,
            ),
          ),
        ],
      ),
      trailing: Icon(
        action.status == CareActionStatus.complete
            ? Icons.check_box_outlined
            : Icons.disabled_by_default_outlined,
        color: action.status == CareActionStatus.complete
            ? Theme.of(context).primaryColor
            : Theme.of(context).errorColor,
      ),
    );
  }
}
