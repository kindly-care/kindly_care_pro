import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart' show Report;
import 'package:responsive_sizer/responsive_sizer.dart';

class ReportTile extends StatelessWidget {
  final Report report;
  final VoidCallback onTap;
  final VoidCallback onLongPress;
  const ReportTile({
    super.key,
    required this.report,
    required this.onTap,
    required this.onLongPress,
  });

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final String reportDate =
        DateFormat('EEE d MMMM', 'en_GB').format(report.createdAt);
    return Padding(
      padding: EdgeInsets.only(top: 1.0.h),
      child: ListTile(
        onTap: onTap,
        leading: CircleAvatar(
          radius: 3.8.h,
          backgroundImage:
              CachedNetworkImageProvider(report.careProviderAvatar),
        ),
        onLongPress: onLongPress,
        title: Text(
          'Care Report for $reportDate',
          style: theme.titleMedium,
          overflow: TextOverflow.ellipsis,
        ),
        subtitle: Padding(
          padding: EdgeInsets.only(top: 0.5.h, bottom: 0.5.h),
          child: Text(
            '${_getShiftStartedTime()} - ${_getShiftFinishedTime()}\nby ${report.careProviderName}',
            style: theme.titleSmall?.copyWith(fontSize: 16.8.sp),
          ),
        ),
        trailing: Icon(Icons.chevron_right, size: 3.8.h),
      ),
    );
  }

  String _getShiftStartedTime() {
    final DateTime? startTime = report.timelog?.startTime;
    if (startTime != null) {
      return DateFormat.jm().format(startTime);
    } else {
      return '--';
    }
  }

  String _getShiftFinishedTime() {
    final DateTime? finishTime = report.timelog?.finishTime;
    if (finishTime != null) {
      return DateFormat.jm().format(finishTime);
    } else {
      return '--';
    }
  }
}
