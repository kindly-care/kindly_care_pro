import 'package:flutter/material.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../../common/constants/constants.dart';

class TaskTile extends StatelessWidget {
  final CareAction action;
  const TaskTile(this.action, {super.key});

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return ListTile(
      contentPadding: EdgeInsets.symmetric(horizontal: 2.0.w),
      title: Text(
        action.title,
        style: theme.titleMedium?.copyWith(fontSize: 17.68.sp),
      ),
      subtitle: action.comment != null
          ? Text(
              action.comment ?? kNoAdditionalInfo,
              style: theme.titleSmall,
            )
          : null,
      trailing: Icon(
        action.status == CareActionStatus.complete
            ? Icons.check_box_outlined
            : Icons.disabled_by_default_outlined,
        color: action.status == CareActionStatus.complete
            ? Theme.of(context).primaryColor
            : Theme.of(context).errorColor,
      ),
    );
  }
}
