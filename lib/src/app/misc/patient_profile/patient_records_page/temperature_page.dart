// ignore_for_file: depend_on_referenced_packages

import 'package:collection/collection.dart';
import 'package:dart_date/dart_date.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../common/dialogs/dialogs.dart';
import '../../../../data/services/services.dart';
import 'bloc/patient_records_bloc.dart';

class TemperaturePage extends StatelessWidget {
  final CareProvider? careProvider;
  final String patientId;
  final String patientName;
  const TemperaturePage({
    super.key,
    this.careProvider,
    required this.patientId,
    required this.patientName,
  });

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PatientRecordsBloc>(
      create: (BuildContext context) {
        return PatientRecordsBloc(
          vitalsRepository: context.read<VitalsService>(),
        )..add(FetchTemperature(
            patientId: patientId, month: DateTime.now().month));
      },
      child: _TemperaturePageView(
        patientId: patientId,
        patientName: patientName,
        careProvider: careProvider,
      ),
    );
  }
}

class _TemperaturePageView extends StatefulWidget {
  final String patientId;
  final String patientName;
  final CareProvider? careProvider;
  const _TemperaturePageView({
    Key? key,
    this.careProvider,
    required this.patientId,
    required this.patientName,
  }) : super(key: key);

  @override
  State<_TemperaturePageView> createState() => _TemperaturePageViewState();
}

class _TemperaturePageViewState extends State<_TemperaturePageView> {
  int _month = DateTime.now().month;
  late TextEditingController _tempController;

  @override
  void initState() {
    super.initState();
    _tempController = TextEditingController();
  }

  @override
  void dispose() {
    _tempController.dispose();
    super.dispose();
  }

  void _onFetchTemperature() {
    context
        .read<PatientRecordsBloc>()
        .add(FetchTemperature(patientId: widget.patientId, month: _month));
  }

  Future<void> _onAddTemperature() async {
    final num temp = num.parse(_tempController.text);
    final DateTime date = DateTime.now();
    final String dateCreated = DateFormat('yyyy-MM-dd').format(date);
    final CareProvider careProvider = widget.careProvider!;
    final TemperatureRecord record = TemperatureRecord(
      takenAt: date,
      temperature: temp,
      dateCreated: dateCreated,
      patientId: widget.patientId,
      careProviderId: careProvider.uid,
      careProviderName: careProvider.name,
    );
    context.read<PatientRecordsBloc>().add(RecordTemperature(record: record));

    _tempController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.1,
        title: const Text('Body Temperature'),
        actions: <Widget>[
          MonthPopupButton(
            icon: Icons.calendar_today_outlined,
            onChanged: (int month) {
              setState(() => _month = month);
              _onFetchTemperature();
            },
          ),
          Visibility(
            visible: widget.careProvider != null,
            child: IconButton(
              icon: Icon(Icons.add, size: 3.8.h),
              onPressed: _showTemperatureInputDialog,
            ),
          ),
        ],
      ),
      body: BlocConsumer<PatientRecordsBloc, PatientRecordsState>(
        listener: (BuildContext context, PatientRecordsState state) {
          if (state is RecordingVitalError) {
            errorSnackbar(context, state.error);
          } else if (state is RecordingVitalSuccess) {
            successSnackbar(context, state.message);
          }
        },
        buildWhen: (_, PatientRecordsState state) {
          return state is PatientRecordsLoading ||
              state is PatientTemperatureLoadSuccess ||
              state is PatientRecordsLoadError;
        },
        builder: (BuildContext context, PatientRecordsState state) {
          if (state is PatientRecordsLoadError) {
            return LoadErrorWidget(
                message: state.error, onRetry: _onFetchTemperature);
          } else if (state is PatientTemperatureLoadSuccess) {
            return _TemperatureTableView(month: _month, records: state.records);
          } else {
            return const LoadingIndicator();
          }
        },
      ),
    );
  }

  void _showTemperatureInputDialog() {
    showInputDialog(
      context,
      controller: _tempController,
      title: 'Add Temperature',
      suffixText: '°C',
      onSubmit: () {
        if (num.tryParse(_tempController.text) == null) {
          showToast(
              message: 'Please enter temperature',
              gravity: ToastGravity.BOTTOM);
          return;
        }
        Navigator.pop(context);
        _onAddTemperature();
      },
    );
  }
}

class _TemperatureTableView extends StatelessWidget {
  final int month;
  final List<TemperatureRecord> records;
  const _TemperatureTableView(
      {Key? key, required this.month, required this.records})
      : super(key: key);

  static List<String> columns = <String>[
    'Care Pro',
    'Date',
    'Temperature',
  ];

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final DateTime date = DateTime.now().setMonth(month);
    final String monthYear = DateFormat.yMMMM('en_GB').format(date);
    final bool isLoading =
        context.select((PatientRecordsBloc bloc) => bloc.state)
            is RecordingVitalInProgress;
    if (records.isEmpty) {
      return NoDataWidget('No Temperature records found for $monthYear.');
    } else {
      return LoadingOverlay(
        isLoading: isLoading,
        child: InteractiveViewer(
          alignPanAxis: true,
          constrained: false,
          scaleEnabled: false,
          child: DataTable(
            columnSpacing: 18.6.w,
            horizontalMargin: 2.5.w,
            columns: columns
                .mapIndexed((int index, String item) => DataColumn(
                      label: Text(
                        item,
                        style: theme.bodyMedium?.copyWith(
                          fontWeight: FontWeight.w600,
                          color: Colors.black.withOpacity(0.7),
                        ),
                      ),
                      numeric: index == 2,
                    ))
                .toList(),
            rows: records.map((TemperatureRecord record) {
              final String temperature = '${record.temperature} °C';

              final String date =
                  DateFormat('dd/MM', 'en_GB').add_jm().format(record.takenAt);

              final String name =
                  record.careProviderName.firstNameLastInitial();

              final List<String> cells = <String>[
                name,
                date,
                temperature,
              ];
              return DataRow(
                cells: cells
                    .map((String item) => DataCell(Text(
                          item,
                          style: theme.bodyMedium,
                        )))
                    .toList(),
              );
            }).toList(),
          ),
        ),
      );
    }
  }
}
