// ignore_for_file: depend_on_referenced_packages

import 'package:collection/collection.dart';
import 'package:dart_date/dart_date.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../common/dialogs/dialogs.dart';
import '../../../../data/services/services.dart';
import 'bloc/patient_records_bloc.dart';

class BloodGlucosePage extends StatelessWidget {
  final String patientId;
  final String patientName;
  final CareProvider? careProvider;
  const BloodGlucosePage({
    super.key,
    this.careProvider,
    required this.patientId,
    required this.patientName,
  });

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PatientRecordsBloc>(
      create: (BuildContext context) {
        return PatientRecordsBloc(
          vitalsRepository: context.read<VitalsService>(),
        )..add(FetchBloodGlucose(
            patientId: patientId, month: DateTime.now().month));
      },
      child: _BloodGlucosePageView(
        careProvider: careProvider,
        patientId: patientId,
        patientName: patientName,
      ),
    );
  }
}

class _BloodGlucosePageView extends StatefulWidget {
  final String patientId;
  final String patientName;
  final CareProvider? careProvider;
  const _BloodGlucosePageView({
    Key? key,
    this.careProvider,
    required this.patientId,
    required this.patientName,
  }) : super(key: key);

  @override
  State<_BloodGlucosePageView> createState() => _BloodGlucosePageViewState();
}

class _BloodGlucosePageViewState extends State<_BloodGlucosePageView> {
  int _month = DateTime.now().month;
  late TextEditingController _bloodGlucoseController;

  @override
  void initState() {
    super.initState();
    _bloodGlucoseController = TextEditingController();
  }

  @override
  void dispose() {
    _bloodGlucoseController.dispose();
    super.dispose();
  }

  void _showBloodGlucoseInputDialog() {
    showInputDialog(
      context,
      controller: _bloodGlucoseController,
      title: 'Add Blood Glucose',
      suffixText: 'mmol/L',
      onSubmit: () {
        if (num.tryParse(_bloodGlucoseController.text) == null) {
          showToast(
              message: 'Please enter blood glucose',
              gravity: ToastGravity.BOTTOM);
          return;
        }
        Navigator.pop(context);
        _onAddBloodGlucose();
      },
    );
  }

  void _onFetchBloodGlucose() {
    context
        .read<PatientRecordsBloc>()
        .add(FetchBloodGlucose(patientId: widget.patientId, month: _month));
  }

  void _onAddBloodGlucose() {
    final num bloodGlucose = num.parse(_bloodGlucoseController.text);
    final CareProvider careProvider = widget.careProvider!;
    final DateTime date = DateTime.now();
    final String dateCreated = DateFormat('yyyy-MM-dd').format(date);
    final BloodGlucoseRecord record = BloodGlucoseRecord(
      takenAt: date,
      dateCreated: dateCreated,
      bloodGlucose: bloodGlucose,
      patientId: widget.patientId,
      careProviderId: careProvider.uid,
      careProviderName: careProvider.name,
    );

    context.read<PatientRecordsBloc>().add(RecordBloodGlucose(record: record));

    _bloodGlucoseController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.1,
        title: const Text('Blood Glucose'),
        actions: <Widget>[
          MonthPopupButton(
            icon: Icons.calendar_today_outlined,
            onChanged: (int month) {
              setState(() => _month = month);
              _onFetchBloodGlucose();
            },
          ),
          Visibility(
            visible: widget.careProvider != null,
            child: IconButton(
              icon: const Icon(Icons.add),
              onPressed: _showBloodGlucoseInputDialog,
            ),
          ),
        ],
      ),
      body: BlocConsumer<PatientRecordsBloc, PatientRecordsState>(
        listener: (BuildContext context, PatientRecordsState state) {
          if (state is RecordingVitalError) {
            errorSnackbar(context, state.error);
          } else if (state is RecordingVitalSuccess) {
            successSnackbar(context, state.message);
          }
        },
        buildWhen: (_, PatientRecordsState state) {
          return state is PatientRecordsLoading ||
              state is PatientBloodGlucoseLoadSuccess ||
              state is PatientRecordsLoadError;
        },
        builder: (BuildContext context, PatientRecordsState state) {
          if (state is PatientRecordsLoadError) {
            return LoadErrorWidget(
                message: state.error, onRetry: _onFetchBloodGlucose);
          } else if (state is PatientBloodGlucoseLoadSuccess) {
            return _BloodSugarTableView(month: _month, records: state.records);
          } else {
            return const LoadingIndicator();
          }
        },
      ),
    );
  }
}

class _BloodSugarTableView extends StatelessWidget {
  final int month;
  final List<BloodGlucoseRecord> records;
  const _BloodSugarTableView(
      {Key? key, required this.month, required this.records})
      : super(key: key);

  static List<String> columns = <String>[
    'Care Pro',
    'Date',
    'Blood Glucose',
  ];

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final DateTime date = DateTime.now().setMonth(month);
    final String monthYear = DateFormat.yMMMM('en_GB').format(date);
    final bool isLoading =
        context.select((PatientRecordsBloc bloc) => bloc.state)
            is RecordingVitalInProgress;
    if (records.isEmpty) {
      return NoDataWidget('No Blood Glucose records found for $monthYear.');
    } else {
      return LoadingOverlay(
        isLoading: isLoading,
        child: InteractiveViewer(
          alignPanAxis: true,
          constrained: false,
          scaleEnabled: false,
          child: DataTable(
            columnSpacing: 17.5.w,
            horizontalMargin: 2.5.w,
            columns: columns
                .mapIndexed((int index, String item) => DataColumn(
                      label: Text(
                        item,
                        style: theme.bodyMedium?.copyWith(
                          fontWeight: FontWeight.w600,
                          color: Colors.black.withOpacity(0.7),
                        ),
                      ),
                      numeric: index == 2,
                    ))
                .toList(),
            rows: records.map((BloodGlucoseRecord record) {
              final String bloodGlucose = '${record.bloodGlucose} mmol/L';

              final String date =
                  DateFormat('dd/MM', 'en_GB').add_jm().format(record.takenAt);

              final String name =
                  record.careProviderName.firstNameLastInitial();

              final List<String> cells = <String>[
                name,
                date,
                bloodGlucose,
              ];
              return DataRow(
                cells: cells
                    .map((String item) =>
                        DataCell(Text(item, style: theme.bodyMedium)))
                    .toList(),
              );
            }).toList(),
          ),
        ),
      );
    }
  }
}
