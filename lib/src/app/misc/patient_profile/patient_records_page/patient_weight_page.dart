// ignore_for_file: depend_on_referenced_packages

import 'package:collection/collection.dart';
import 'package:dart_date/dart_date.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../common/dialogs/dialogs.dart';
import '../../../../data/services/services.dart';
import 'bloc/patient_records_bloc.dart';

class WeightPage extends StatelessWidget {
  final String patientId;
  final String patientName;
  final CareProvider? careProvider;
  const WeightPage({
    super.key,
    this.careProvider,
    required this.patientId,
    required this.patientName,
  });

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PatientRecordsBloc>(
      create: (BuildContext context) {
        return PatientRecordsBloc(
          vitalsRepository: context.read<VitalsService>(),
        )..add(FetchWeight(patientId: patientId, month: DateTime.now().month));
      },
      child: _WeightPageView(
        patientId: patientId,
        patientName: patientName,
        careProvider: careProvider,
      ),
    );
  }
}

class _WeightPageView extends StatefulWidget {
  final String patientId;
  final String patientName;
  final CareProvider? careProvider;
  const _WeightPageView({
    Key? key,
    this.careProvider,
    required this.patientId,
    required this.patientName,
  }) : super(key: key);

  @override
  State<_WeightPageView> createState() => _WeightPageViewState();
}

class _WeightPageViewState extends State<_WeightPageView> {
  int _month = DateTime.now().month;
  late TextEditingController _weightController;

  @override
  void initState() {
    super.initState();
    _weightController = TextEditingController();
  }

  @override
  void dispose() {
    _weightController.dispose();
    super.dispose();
  }

  void _showWeightInputDialog() {
    showInputDialog(
      context,
      controller: _weightController,
      title: 'Add Weight',
      suffixText: 'kg',
      onSubmit: () {
        if (num.tryParse(_weightController.text) == null) {
          showToast(
              message: 'Please enter weight', gravity: ToastGravity.BOTTOM);
          return;
        }
        Navigator.pop(context);
        _onAddWeight();
      },
    );
  }

  void _onFetchWeight() {
    context
        .read<PatientRecordsBloc>()
        .add(FetchWeight(patientId: widget.patientId, month: _month));
  }

  void _onAddWeight() {
    final num weight = num.parse(_weightController.text);
    final CareProvider careProvider = widget.careProvider!;
    final DateTime date = DateTime.now();
    final String dateCreated = DateFormat('yyyy-MM-dd').format(date);
    final WeightRecord record = WeightRecord(
      takenAt: date,
      weight: weight,
      dateCreated: dateCreated,
      patientId: widget.patientId,
      careProviderId: careProvider.uid,
      careProviderName: careProvider.name,
    );
    context.read<PatientRecordsBloc>().add(RecordWeight(record: record));

    _weightController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.1,
        title: const Text('Body Weight'),
        actions: <Widget>[
          MonthPopupButton(
            icon: Icons.calendar_today_outlined,
            onChanged: (int month) {
              setState(() => _month = month);
              _onFetchWeight();
            },
          ),
          Visibility(
            visible: widget.careProvider != null,
            child: IconButton(
              icon: Icon(Icons.add, size: 3.8.h),
              onPressed: _showWeightInputDialog,
            ),
          ),
        ],
      ),
      body: BlocConsumer<PatientRecordsBloc, PatientRecordsState>(
        listener: (BuildContext context, PatientRecordsState state) {
          if (state is RecordingVitalError) {
            errorSnackbar(context, state.error);
          } else if (state is RecordingVitalSuccess) {
            successSnackbar(context, state.message);
          }
        },
        buildWhen: (_, PatientRecordsState state) {
          return state is PatientRecordsLoading ||
              state is PatientWeightLoadSuccess ||
              state is PatientRecordsLoadError;
        },
        builder: (BuildContext context, PatientRecordsState state) {
          if (state is PatientRecordsLoadError) {
            return LoadErrorWidget(
                message: state.error, onRetry: _onFetchWeight);
          } else if (state is PatientWeightLoadSuccess) {
            return _WeightTableView(month: _month, records: state.records);
          } else {
            return const LoadingIndicator();
          }
        },
      ),
    );
  }
}

class _WeightTableView extends StatelessWidget {
  final int month;
  final List<WeightRecord> records;
  const _WeightTableView({Key? key, required this.month, required this.records})
      : super(key: key);

  static List<String> columns = <String>[
    'Care Pro',
    'Date',
    'Weight (kg)',
  ];

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final DateTime date = DateTime.now().setMonth(month);
    final String monthYear = DateFormat.yMMMM('en_GB').format(date);
    final bool isLoading =
        context.select((PatientRecordsBloc bloc) => bloc.state)
            is RecordingVitalInProgress;
    if (records.isEmpty) {
      return NoDataWidget('No Weight records found for $monthYear.');
    } else {
      return LoadingOverlay(
        isLoading: isLoading,
        child: InteractiveViewer(
          alignPanAxis: true,
          constrained: false,
          scaleEnabled: false,
          child: DataTable(
            columnSpacing: 19.5.w,
            horizontalMargin: 2.5.w,
            columns: columns
                .mapIndexed((int index, String item) => DataColumn(
                      label: Text(
                        item,
                        style: theme.bodyMedium?.copyWith(
                          fontWeight: FontWeight.w600,
                          color: Colors.black.withOpacity(0.7),
                        ),
                      ),
                      numeric: index == 2,
                    ))
                .toList(),
            rows: records.map((WeightRecord record) {
              final String weight = '${record.weight}';

              final String date =
                  DateFormat('dd/MM', 'en_GB').add_Hm().format(record.takenAt);

              final String name =
                  record.careProviderName.firstNameLastInitial();

              final List<String> cells = <String>[
                name,
                date,
                weight,
              ];
              return DataRow(
                cells: cells
                    .map((String item) =>
                        DataCell(Text(item, style: theme.bodyMedium)))
                    .toList(),
              );
            }).toList(),
          ),
        ),
      );
    }
  }
}
