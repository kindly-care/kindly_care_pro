// ignore_for_file: depend_on_referenced_packages

import 'package:collection/collection.dart';
import 'package:dart_date/dart_date.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../common/dialogs/dialogs.dart';
import '../../../../data/services/services.dart';
import 'bloc/patient_records_bloc.dart';

class BloodPressurePage extends StatelessWidget {
  final String patientId;
  final String patientName;
  final CareProvider? careProvider;
  const BloodPressurePage({
    super.key,
    this.careProvider,
    required this.patientId,
    required this.patientName,
  });

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PatientRecordsBloc>(
      create: (BuildContext context) {
        return PatientRecordsBloc(
          vitalsRepository: context.read<VitalsService>(),
        )..add(FetchBloodPressure(
            patientId: patientId, month: DateTime.now().month));
      },
      child: _BloodPressurePageView(
        patientId: patientId,
        patientName: patientName,
        careProvider: careProvider,
      ),
    );
  }
}

class _BloodPressurePageView extends StatefulWidget {
  final String patientId;
  final String patientName;
  final CareProvider? careProvider;
  const _BloodPressurePageView({
    Key? key,
    this.careProvider,
    required this.patientId,
    required this.patientName,
  }) : super(key: key);

  @override
  State<_BloodPressurePageView> createState() => _BloodPressurePageViewState();
}

class _BloodPressurePageViewState extends State<_BloodPressurePageView> {
  int _month = DateTime.now().month;
  late TextEditingController _systolicController;
  late TextEditingController _diastolicController;

  @override
  void initState() {
    super.initState();

    _systolicController = TextEditingController();
    _diastolicController = TextEditingController();
  }

  @override
  void dispose() {
    _systolicController.dispose();
    _diastolicController.dispose();
    super.dispose();
  }

  void _showBloodPressureInputDialog() {
    showBloodPressureInputDialog(
      context,
      systolicController: _systolicController,
      diastolicController: _diastolicController,
      onSubmit: () {
        if (num.tryParse(_systolicController.text) == null) {
          showToast(
              message: 'Please enter systolic value',
              gravity: ToastGravity.BOTTOM);
          return;
        }
        if (num.tryParse(_diastolicController.text) == null) {
          showToast(
              message: 'Please enter diastolic value',
              gravity: ToastGravity.BOTTOM);
          return;
        }
        Navigator.pop(context);
        _onAddBloodPressure();
      },
    );
  }

  void _onFetchBloodPressure() {
    context
        .read<PatientRecordsBloc>()
        .add(FetchBloodPressure(patientId: widget.patientId, month: _month));
  }

  void _onAddBloodPressure() {
    final num systolic = num.parse(_systolicController.text);
    final num diastolic = num.parse(_diastolicController.text);
    final DateTime date = DateTime.now();
    final String dateCreated = DateFormat('yyyy-MM-dd').format(date);

    final BloodPressureRecord record = BloodPressureRecord(
      takenAt: date,
      systolic: systolic,
      diastolic: diastolic,
      dateCreated: dateCreated,
      patientId: widget.patientId,
      careProviderId: widget.careProvider!.uid,
      careProviderName: widget.careProvider!.name,
    );
    context.read<PatientRecordsBloc>().add(RecordBloodPressure(record: record));
    _systolicController.clear();
    _diastolicController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.1,
        title: const Text('Blood Pressure'),
        actions: <Widget>[
          MonthPopupButton(
            icon: Icons.calendar_today_outlined,
            onChanged: (int month) {
              setState(() => _month = month);
              _onFetchBloodPressure();
            },
          ),
          Visibility(
            visible: widget.careProvider != null,
            child: IconButton(
              icon: Icon(Icons.add, size: 3.8.h),
              onPressed: _showBloodPressureInputDialog,
            ),
          )
        ],
      ),
      body: BlocConsumer<PatientRecordsBloc, PatientRecordsState>(
        listener: (BuildContext context, PatientRecordsState state) {
          if (state is RecordingVitalError) {
            errorSnackbar(context, state.error);
          } else if (state is RecordingVitalSuccess) {
            successSnackbar(context, state.message);
          }
        },
        buildWhen: (_, PatientRecordsState state) {
          return state is PatientRecordsLoading ||
              state is PatientBloodPressureLoadSuccess ||
              state is PatientRecordsLoadError;
        },
        builder: (BuildContext context, PatientRecordsState state) {
          if (state is PatientRecordsLoadError) {
            return LoadErrorWidget(
                message: state.error, onRetry: _onFetchBloodPressure);
          } else if (state is PatientBloodPressureLoadSuccess) {
            return _BloodPressureTableView(
                month: _month, records: state.records);
          } else {
            return const LoadingIndicator();
          }
        },
      ),
    );
  }
}

class _BloodPressureTableView extends StatelessWidget {
  final int month;
  final List<BloodPressureRecord> records;
  const _BloodPressureTableView(
      {Key? key, required this.month, required this.records})
      : super(key: key);

  static List<String> columns = <String>[
    'Care Pro',
    'Date',
    'Blood Pressure',
  ];

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final DateTime date = DateTime.now().setMonth(month);
    final String monthYear = DateFormat.yMMMM('en_GB').format(date);
    final bool isLoading =
        context.select((PatientRecordsBloc bloc) => bloc.state)
            is RecordingVitalInProgress;
    if (records.isEmpty) {
      return NoDataWidget('No Blood Pressure records found for $monthYear.');
    } else {
      return LoadingOverlay(
        isLoading: isLoading,
        child: InteractiveViewer(
          alignPanAxis: true,
          constrained: false,
          scaleEnabled: false,
          child: DataTable(
            columnSpacing: 17.5.w,
            horizontalMargin: 2.5.w,
            columns: columns
                .mapIndexed((int index, String item) => DataColumn(
                      label: Text(
                        item,
                        style: theme.bodyMedium?.copyWith(
                          fontWeight: FontWeight.w600,
                          color: Colors.black.withOpacity(0.7),
                        ),
                      ),
                      numeric: index == 2,
                    ))
                .toList(),
            rows: records.map((BloodPressureRecord record) {
              final String systolic = '${record.systolic.truncate()}';
              final String diastolic = '${record.diastolic.truncate()}';
              final String bloodPressure = '$systolic/$diastolic mmHg';

              final String date =
                  DateFormat('dd/MM', 'en_GB').add_jm().format(record.takenAt);

              final String name =
                  record.careProviderName.firstNameLastInitial();

              final List<String> cells = <String>[
                name,
                date,
                bloodPressure,
              ];
              return DataRow(
                cells: cells
                    .map((String item) =>
                        DataCell(Text(item, style: theme.bodyMedium)))
                    .toList(),
              );
            }).toList(),
          ),
        ),
      );
    }
  }
}
