part of 'patient_records_bloc.dart';

abstract class PatientRecordsState extends Equatable {
  const PatientRecordsState();

  @override
  List<Object> get props => <Object>[];
}

class PatientRecordsInitial extends PatientRecordsState {}

class PatientRecordsLoading extends PatientRecordsState {}

class PatientRecordsLoadError extends PatientRecordsState {
  final String error;
  const PatientRecordsLoadError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

class PatientBloodGlucoseLoadSuccess extends PatientRecordsState {
  final List<BloodGlucoseRecord> records;
  const PatientBloodGlucoseLoadSuccess({required this.records});

  @override
  List<Object> get props => <Object>[records];
}

class PatientBloodPressureLoadSuccess extends PatientRecordsState {
  final List<BloodPressureRecord> records;
  const PatientBloodPressureLoadSuccess({required this.records});

  @override
  List<Object> get props => <Object>[records];
}

class PatientWeightLoadSuccess extends PatientRecordsState {
  final List<WeightRecord> records;
  const PatientWeightLoadSuccess({required this.records});

  @override
  List<Object> get props => <Object>[records];
}

class PatientTemperatureLoadSuccess extends PatientRecordsState {
  final List<TemperatureRecord> records;
  const PatientTemperatureLoadSuccess({required this.records});

  @override
  List<Object> get props => <Object>[records];
}

class RecordingVitalInProgress extends PatientRecordsState {}

class RecordingVitalSuccess extends PatientRecordsState {
  final String message;
  const RecordingVitalSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class RecordingVitalError extends PatientRecordsState {
  final String error;
  const RecordingVitalError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}
