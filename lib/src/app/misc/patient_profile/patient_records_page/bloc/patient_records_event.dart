part of 'patient_records_bloc.dart';

abstract class PatientRecordsEvent extends Equatable {
  const PatientRecordsEvent();

  @override
  List<Object> get props => <Object>[];
}

class FetchBloodGlucose extends PatientRecordsEvent {
  final int month;
  final String patientId;
  const FetchBloodGlucose({required this.month, required this.patientId});

  @override
  List<Object> get props => <Object>[month, patientId];
}

class FetchBloodPressure extends PatientRecordsEvent {
  final int month;
  final String patientId;
  const FetchBloodPressure({required this.month, required this.patientId});

  @override
  List<Object> get props => <Object>[month, patientId];
}

class FetchWeight extends PatientRecordsEvent {
  final int month;
  final String patientId;
  const FetchWeight({required this.month, required this.patientId});

  @override
  List<Object> get props => <Object>[month, patientId];
}

class FetchTemperature extends PatientRecordsEvent {
  final int month;
  final String patientId;
  const FetchTemperature({required this.month, required this.patientId});

  @override
  List<Object> get props => <Object>[month, patientId];
}

class RecordTemperature extends PatientRecordsEvent {
  final TemperatureRecord record;
  const RecordTemperature({required this.record});

  @override
  List<Object> get props => <Object>[record];
}

class RecordWeight extends PatientRecordsEvent {
  final WeightRecord record;
  const RecordWeight({required this.record});

  @override
  List<Object> get props => <Object>[record];
}

class RecordBloodGlucose extends PatientRecordsEvent {
  final BloodGlucoseRecord record;
  const RecordBloodGlucose({required this.record});

  @override
  List<Object> get props => <Object>[record];
}

class RecordBloodPressure extends PatientRecordsEvent {
  final BloodPressureRecord record;
  const RecordBloodPressure({required this.record});

  @override
  List<Object> get props => <Object>[record];
}
