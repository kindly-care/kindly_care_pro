import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../../../data/services/vitals/vitals_repository.dart';

part 'patient_records_event.dart';
part 'patient_records_state.dart';

class PatientRecordsBloc
    extends Bloc<PatientRecordsEvent, PatientRecordsState> {
  final VitalsRepository _vitalsRepository;
  PatientRecordsBloc({required VitalsRepository vitalsRepository})
      : _vitalsRepository = vitalsRepository,
        super(PatientRecordsInitial()) {
    on<FetchBloodGlucose>(_onFetchBloodGlucose);
    on<FetchBloodPressure>(_onFetchBloodPressure);
    on<FetchWeight>(_onFetchWeight);
    on<FetchTemperature>(_onFetchTemperature);
    on<RecordTemperature>(_onRecordTemperature);
    on<RecordWeight>(_onRecordWeight);
    on<RecordBloodGlucose>(_onRecordBloodGlucose);
    on<RecordBloodPressure>(_onRecordBloodPressure);
  }

  Future<void> _onFetchBloodGlucose(
      FetchBloodGlucose event, Emitter<PatientRecordsState> emit) async {
    emit(PatientRecordsLoading());

    await emit.forEach<List<BloodGlucoseRecord>>(
      _vitalsRepository.fetchBloodGlucoseByMonth(
          patientId: event.patientId, month: event.month),
      onData: (List<BloodGlucoseRecord> records) =>
          PatientBloodGlucoseLoadSuccess(records: records),
      onError: (_, __) =>
          const PatientRecordsLoadError(error: 'Failed to load data'),
    );
  }

  Future<void> _onFetchBloodPressure(
      FetchBloodPressure event, Emitter<PatientRecordsState> emit) async {
    emit(PatientRecordsLoading());

    await emit.forEach<List<BloodPressureRecord>>(
      _vitalsRepository.fetchBloodPressureByMonth(
          patientId: event.patientId, month: event.month),
      onData: (List<BloodPressureRecord> records) =>
          PatientBloodPressureLoadSuccess(records: records),
      onError: (_, __) =>
          const PatientRecordsLoadError(error: 'Failed to load data'),
    );
  }

  Future<void> _onFetchWeight(
      FetchWeight event, Emitter<PatientRecordsState> emit) async {
    emit(PatientRecordsLoading());

    await emit.forEach<List<WeightRecord>>(
      _vitalsRepository.fetchWeightByMonth(
          patientId: event.patientId, month: event.month),
      onData: (List<WeightRecord> records) =>
          PatientWeightLoadSuccess(records: records),
      onError: (_, __) =>
          const PatientRecordsLoadError(error: 'Failed to load data'),
    );
  }

  Future<void> _onFetchTemperature(
      FetchTemperature event, Emitter<PatientRecordsState> emit) async {
    emit(PatientRecordsLoading());

    await emit.forEach<List<TemperatureRecord>>(
      _vitalsRepository.fetchTemperatureByMonth(
          patientId: event.patientId, month: event.month),
      onData: (List<TemperatureRecord> records) =>
          PatientTemperatureLoadSuccess(records: records),
      onError: (_, __) =>
          const PatientRecordsLoadError(error: 'Failed to load data'),
    );
  }

  Future<void> _onRecordTemperature(
      RecordTemperature event, Emitter<PatientRecordsState> emit) async {
    emit(RecordingVitalInProgress());
    try {
      await _vitalsRepository.recordTemperature(event.record);
      emit(const RecordingVitalSuccess(message: 'Temperature submitted.'));
    } on UpdateDataException catch (e) {
      emit(RecordingVitalError(error: e.toString()));
    }
  }

  Future<void> _onRecordWeight(
      RecordWeight event, Emitter<PatientRecordsState> emit) async {
    emit(RecordingVitalInProgress());
    try {
      await _vitalsRepository.recordWeight(event.record);
      emit(const RecordingVitalSuccess(message: 'Weight submitted.'));
    } on UpdateDataException catch (e) {
      emit(RecordingVitalError(error: e.toString()));
    }
  }

  Future<void> _onRecordBloodGlucose(
      RecordBloodGlucose event, Emitter<PatientRecordsState> emit) async {
    emit(RecordingVitalInProgress());
    try {
      await _vitalsRepository.recordBloodGlucose(event.record);
      emit(const RecordingVitalSuccess(message: 'Blood glucose submitted.'));
    } on UpdateDataException catch (e) {
      emit(RecordingVitalError(error: e.toString()));
    }
  }

  Future<void> _onRecordBloodPressure(
      RecordBloodPressure event, Emitter<PatientRecordsState> emit) async {
    emit(RecordingVitalInProgress());
    try {
      await _vitalsRepository.recordBloodPressure(event.record);
      emit(const RecordingVitalSuccess(message: 'Blood pressure submitted.'));
    } on UpdateDataException catch (e) {
      emit(RecordingVitalError(error: e.toString()));
    }
  }
}
