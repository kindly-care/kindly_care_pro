import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import 'blood_glucose_page.dart';
import 'blood_pressure_page.dart';
import 'patient_weight_page.dart';
import 'temperature_page.dart';

class PatientRecordsPage extends StatelessWidget {
  final String patientId;
  final String patientName;
  final CareProvider? careProvider;

  const PatientRecordsPage({
    super.key,
    this.careProvider,
    required this.patientId,
    required this.patientName,
  });

  void _onTemperatureTap(BuildContext context) {
    Navigator.push(
      context,
      CupertinoPageRoute<void>(
        builder: (_) => TemperaturePage(
          patientId: patientId,
          patientName: patientName,
          careProvider: careProvider,
        ),
      ),
    );
  }

  void _onWeightTap(BuildContext context) {
    Navigator.push(
      context,
      CupertinoPageRoute<void>(
        builder: (_) => WeightPage(
          patientId: patientId,
          patientName: patientName,
          careProvider: careProvider,
        ),
      ),
    );
  }

  void _onBloodGlucoseTap(BuildContext context) {
    Navigator.push(
      context,
      CupertinoPageRoute<void>(
        builder: (_) => BloodGlucosePage(
          patientId: patientId,
          patientName: patientName,
          careProvider: careProvider,
        ),
      ),
    );
  }

  void _onBloodPressureTap(BuildContext context) {
    Navigator.push(
      context,
      CupertinoPageRoute<void>(
        builder: (_) => BloodPressurePage(
          patientId: patientId,
          patientName: patientName,
          careProvider: careProvider,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${patientName.firstName()}'s Vitals"),
      ),
      body: ListView(
        padding: EdgeInsets.all(3.0.w),
        physics: const BouncingScrollPhysics(),
        children: <Widget>[
          TextListTile(
            title: 'Blood Pressure',
            subtitle: careProvider == null
                ? 'View Blood Pressure Records'
                : 'Record Blood Pressure',
            onTap: () => _onBloodPressureTap(context),
          ),
          Divider(indent: 5.w, endIndent: 5.0.w),
          TextListTile(
            title: 'Body Temperature',
            subtitle: careProvider == null
                ? 'View Temperature Records'
                : 'Record Temperature',
            onTap: () => _onTemperatureTap(context),
          ),
          Divider(indent: 5.w, endIndent: 5.0.w),
          TextListTile(
            title: 'Blood Glucose',
            subtitle: careProvider == null
                ? 'View Blood Glucose Records'
                : 'Record Blood Glucose',
            onTap: () => _onBloodGlucoseTap(context),
          ),
          Divider(indent: 5.w, endIndent: 5.0.w),
          TextListTile(
            title: 'Body Weight',
            subtitle:
                careProvider == null ? 'View Weight Records' : 'Record Weight',
            onTap: () => _onWeightTap(context),
          ),
        ],
      ),
    );
  }
}
