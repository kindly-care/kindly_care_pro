import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../blocs/patient/patient_bloc.dart';
import '../../../../common/common.dart';
import '../../../widgets/widgets.dart';

class PatientInfoTab extends StatefulWidget {
  final Patient patient;
  const PatientInfoTab({super.key, required this.patient});

  @override
  PatientInfoTabState createState() => PatientInfoTabState();
}

class PatientInfoTabState extends State<PatientInfoTab>
    with AutomaticKeepAliveClientMixin {
  @override
  void initState() {
    super.initState();
    _loadAssignedCareProviders();
  }

  void _loadAssignedCareProviders() {
    final List<String> careProviderIds = widget
        .patient.assignedCareProviders.values
        .map((AssignedCareProvider careProvider) => careProvider.careProviderId)
        .toList();

    context
        .read<PatientBloc>()
        .add(FetchAssignedCareProviders(careProviderIds: careProviderIds));
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<PatientBloc, PatientState>(
      buildWhen: (_, PatientState state) {
        return state is AssignedCareProvidersLoading ||
            state is AssignedCareProvidersLoadSuccess ||
            state is AssignedCareProvidersLoadError;
      },
      builder: (BuildContext context, PatientState state) {
        if (state is AssignedCareProvidersLoadError) {
          return LoadErrorWidget(
              message: state.error, onRetry: _loadAssignedCareProviders);
        } else if (state is AssignedCareProvidersLoadSuccess) {
          return _TabContent(
              careProviders: state.careProviders, patient: widget.patient);
        } else {
          return const LoadingIndicator();
        }
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class _TabContent extends StatelessWidget {
  final Patient patient;
  final List<CareProvider> careProviders;
  const _TabContent(
      {Key? key, required this.patient, required this.careProviders})
      : super(key: key);

  String _getGenderAge() {
    final int age = DateTime.now().year - patient.dob!.year;
    return '${patient.gender} ($age years old)';
  }

  String _getAllergies() {
    if (patient.allergies.isEmpty) {
      return 'None specified';
    } else {
      return patient.allergies.join(', ');
    }
  }

  String _getAilments() {
    if (patient.conditions.isEmpty) {
      return 'None specified';
    } else {
      return patient.conditions.join(', ');
    }
  }

  String _getBio() {
    if (patient.bio.isNotEmpty) {
      return patient.bio;
    } else {
      return 'No bio provided';
    }
  }

  List<AboutListTileWidget> _getAssignedCareProviders(BuildContext context) {
    if (patient.assignedCareProviders.isNotEmpty) {
      return patient.assignedCareProviders.values
          .map((AssignedCareProvider assignedCareProvider) =>
              AboutListTileWidget(
                title: assignedCareProvider.careProviderName,
                icon: Icons.account_circle_outlined,
                subtitle: assignedCareProvider.shift,
                trailing: Icon(Icons.chevron_right, size: 3.8.h),
                onTap: () => _onCareProviderTap(context, assignedCareProvider),
              ))
          .toList();
    } else {
      return <AboutListTileWidget>[
        const AboutListTileWidget(
          title: 'None',
          icon: Icons.account_circle_outlined,
        )
      ];
    }
  }

  void _onCareProviderTap(BuildContext context, AssignedCareProvider assigned) {
    final CareProvider? careProvider = careProviders.firstWhereOrNull(
        (CareProvider careProvider) =>
            careProvider.uid == assigned.careProviderId);
    if (careProvider != null) {
      Navigator.of(context).pushNamed(kCareProviderProfilePageRoute,
          arguments: careProvider.uid);
    } else {
      errorSnackbar(context, 'Ops! Something went wrong.');
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      physics: const BouncingScrollPhysics(),
      padding: EdgeInsets.all(3.0.w),
      children: <Widget>[
        AboutListTileWidget(
          title: 'Name',
          subtitle: patient.name,
          icon: Icons.account_circle_outlined,
        ),
        AboutListTileWidget(
          icon: MdiIcons.genderMaleFemale,
          title: 'Gender',
          subtitle: _getGenderAge(),
        ),
        AboutListTileWidget(
          icon: MdiIcons.ring,
          title: 'Marital Status',
          subtitle: patient.maritalStatus,
        ),
        const Divider(),
        const _CustomHeading(heading: 'Address & Contact'),
        AboutListTileWidget(
          textColor: Colors.teal.shade300,
          title: 'Home Address',
          subtitle: patient.address?.fullAddress() ?? kUnknownAddress,
          icon: Boxicons.bx_home_heart,
          onTap: () {
            Navigator.pushNamed(context, kPatientMapPageRoute,
                arguments: patient);
          },
        ),
        AboutListTileWidget(
          title: 'Phone',
          subtitle: patient.phone,
          icon: Icons.phone_outlined,
        ),
        const Divider(),
        const _CustomHeading(heading: 'Health & Wellness'),
        AboutListTileWidget(
          title: 'Ailments',
          icon: MdiIcons.medicalBag,
          subtitle: _getAilments(),
        ),
        AboutListTileWidget(
          title: 'Allergies',
          icon: MdiIcons.allergy,
          subtitle: _getAllergies(),
        ),
        AboutListTileWidget(
          title: 'Mobility',
          subtitle: patient.mobility,
          icon: Icons.directions_walk_outlined,
        ),
        const Divider(),
        const _CustomHeading(heading: 'Other'),
        AboutListTileWidget(
          title: 'Bio',
          subtitle: _getBio(),
          icon: Icons.info_outline,
        ),
        const Divider(),
        const _CustomHeading(heading: 'Care Providers'),
        ..._getAssignedCareProviders(context),
      ],
    );
  }
}

class _CustomHeading extends StatelessWidget {
  final String heading;
  const _CustomHeading({Key? key, required this.heading}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 4.5.w, vertical: 1.0.h),
      child: Text(
        heading,
        style: theme.bodyLarge?.copyWith(fontSize: 18.5.sp),
      ),
    );
  }
}
