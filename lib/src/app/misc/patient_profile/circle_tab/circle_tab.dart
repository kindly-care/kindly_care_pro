import 'package:cached_network_image/cached_network_image.dart';
import 'package:collection/collection.dart';
import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../blocs/patient/patient_bloc.dart';
import '../../../../common/common.dart';

class CircleTab extends StatefulWidget {
  final Patient patient;
  const CircleTab({super.key, required this.patient});

  @override
  CircleTabState createState() => CircleTabState();
}

class CircleTabState extends State<CircleTab>
    with AutomaticKeepAliveClientMixin {
  @override
  void initState() {
    super.initState();
    _loadCircleMembers();
  }

  void _loadCircleMembers() {
    context.read<PatientBloc>().add(
        FetchPatientCircle(memberIds: widget.patient.circle.keys.toList()));
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<PatientBloc, PatientState>(
      buildWhen: (_, PatientState state) {
        return state is CircleMembersLoading ||
            state is CircleMembersLoadSuccess ||
            state is CircleMembersLoadError;
      },
      builder: (BuildContext context, PatientState state) {
        if (state is CircleMembersLoadError) {
          return LoadErrorWidget(
              message: state.error, onRetry: _loadCircleMembers);
        } else if (state is CircleMembersLoadSuccess) {
          return _TabContent(members: state.members, patient: widget.patient);
        } else {
          return const LoadingIndicator();
        }
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class _TabContent extends StatelessWidget {
  final Patient patient;
  final List<AppUser> members;
  const _TabContent({Key? key, required this.patient, required this.members})
      : super(key: key);

  void _onMemberTap(BuildContext context, AppUser member) {
    Navigator.pushNamed(context, kMemberProfilePageRoute, arguments: member);
  }

  String _getRelation(AppUser member) {
    final String relation = patient.circle.entries
            .firstWhereOrNull(
                (MapEntry<String, String> entry) => entry.key == member.uid)
            ?.value ??
        'Relation Unknown';

    if (patient.nextOfKin.contains(member.uid)) {
      return '$relation (Next Of Kin)';
    } else {
      return relation;
    }
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    if (members.isEmpty) {
      return Padding(
        padding: EdgeInsets.only(top: 18.0.h),
        child: const NoDataWidget('No circle members found.'),
      );
    } else {
      return ListView.builder(
        shrinkWrap: true,
        itemCount: members.length,
        physics: const BouncingScrollPhysics(),
        padding: EdgeInsets.symmetric(horizontal: 1.0.w, vertical: 1.8.h),
        itemBuilder: (BuildContext context, int index) {
          final AppUser member = members[index];
          return ListTile(
            onTap: () => _onMemberTap(context, member),
            onLongPress: () => _onMemberLongPress(context, member, user),
            leading: CircleAvatar(
              radius: 3.6.h,
              backgroundImage: CachedNetworkImageProvider(member.avatar),
            ),
            title: Text(
              member.name,
              style: theme.titleMedium,
              overflow: TextOverflow.ellipsis,
            ),
            subtitle: Text(
              _getRelation(member),
              maxLines: 1,
              style: theme.titleSmall,
              overflow: TextOverflow.ellipsis,
            ),
            trailing: IconButton(
              onPressed: () => onMakeCall(member.phone),
              icon: Icon(FeatherIcons.phone, size: 3.8.h),
            ),
          );
        },
      );
    }
  }

  void _onMemberLongPress(BuildContext context, AppUser member, AppUser user) {
    HapticFeedback.lightImpact();
    optionsBottomSheet(
      context: context,
      title: member.name,
      height: 32.0.h,
      options: <Widget>[
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            onMakeCall(member.phone);
          },
          title: 'Call',
          showTrailing: false,
          icon: FeatherIcons.phone,
        ),
        Divider(indent: 18.w),
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            createChannel(context, uid: user.uid, otherId: member.uid);
          },
          title: 'Message',
          showTrailing: false,
          icon: FeatherIcons.messageSquare,
        ),
      ],
    );
  }
}
