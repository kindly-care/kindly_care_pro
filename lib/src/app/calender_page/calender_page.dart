import 'package:collection/collection.dart';
import 'package:dart_date/dart_date.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:flutter_clean_calendar/flutter_clean_calendar.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../blocs/care_task/care_task_bloc.dart';
import '../../blocs/notifications/notification_bloc.dart';
import '../../common/common.dart';
import 'widgets/care_tasks/care_tasks.dart';

class CalenderPage extends StatelessWidget {
  final CareProvider careProvider;
  const CalenderPage({super.key, required this.careProvider});

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<Box<TimeLog>>(
      valueListenable: TimeLogBox.box.listenable(keys: <String>[kTimeLogKey]),
      builder: (BuildContext context, Box<TimeLog> box, _) {
        final TimeLog? timelog = box.values
            .firstWhereOrNull((TimeLog timelog) => timelog.finishTime == null);

        return _CalenderPageView(careProvider: careProvider, timelog: timelog);
      },
    );
  }
}

class _CalenderPageView extends StatefulWidget {
  final TimeLog? timelog;
  final CareProvider careProvider;

  const _CalenderPageView({Key? key, this.timelog, required this.careProvider})
      : super(key: key);

  @override
  State<_CalenderPageView> createState() => _CalenderPageViewState();
}

class _CalenderPageViewState extends State<_CalenderPageView> {
  DateTime _selectedDate = DateTime.now();

  @override
  void initState() {
    super.initState();

    if (widget.timelog != null) {
      _onLoadCareTasks();
    }
  }

  @override
  void didUpdateWidget(_CalenderPageView oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.timelog != widget.timelog) {
      if (widget.timelog != null) {
        _onLoadCareTasks();
      }
    }
  }

  void _onLoadCareTasks() {
    if (widget.timelog != null) {
      context.read<CareTaskBloc>().add(FetchCareTasksByDate(
          date: _selectedDate, patientId: widget.timelog!.patientId));
    }
  }

  void _onNotificationTap() {
    Navigator.pushNamed(context, kNotificationsPageRoute,
        arguments: widget.careProvider);
  }

  bool _showNotificationBadge(List<PushNotification> notifications) {
    final List<PushNotification> notSeenNotifications = notifications
        .where((PushNotification notification) => !notification.isSeen)
        .toList();

    if (notSeenNotifications.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    final List<PushNotification> notifications =
        context.watch<NotificationBloc>().notifications;
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: const Text('My Calendar'),
        actions: <Widget>[
          IconButton(
            icon: Stack(
              children: <Widget>[
                Icon(Boxicons.bx_bell, size: 3.8.h),
                Visibility(
                  visible: _showNotificationBadge(notifications),
                  child: Positioned(
                    top: 0.1.h,
                    right: 0.8.w,
                    child: Icon(Icons.brightness_1,
                        size: 1.5.h, color: Colors.redAccent),
                  ),
                ),
              ],
            ),
            onPressed: _onNotificationTap,
          ),
        ],
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              height: 21.0.h,
              color: Theme.of(context).backgroundColor,
              child: Calendar(
                startOnMonday: false,
                initialDate: DateTime.now(),
                events: const <DateTime, List<CleanCalendarEvent>>{},
                onDateSelected: (DateTime date) {
                  setState(() => _selectedDate = date);
                  _onLoadCareTasks();
                },
                isExpandable: false,
                selectedColor: Theme.of(context).primaryColor,
                todayColor: Colors.blue,
                hideTodayIcon: true,
                locale: 'en_GB',
                todayButtonText: '',
                dayOfWeekStyle: TextStyle(
                  fontSize: 16.0.sp,
                  color: Colors.black,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ),
          SizedBox(height: 2.5.h),
          Expanded(
            flex: 3,
            child: ValueListenableBuilder<Box<TimeLog>>(
              valueListenable: TimeLogBox.box.listenable(),
              builder: (BuildContext context, Box<TimeLog> box, _) {
                final TimeLog? timelog = box.values.firstWhereOrNull(
                    (TimeLog timelog) => timelog.finishTime == null);
                final bool clockedIn = timelog != null;
                return !clockedIn
                    ? const NoDataWidget(kNotClockedIn)
                    : BlocBuilder<CareTaskBloc, CareTaskState>(
                        buildWhen: (_, CareTaskState state) {
                          return state is CareTasksLoading ||
                              state is CareTasksLoadSuccess ||
                              state is CareTasksLoadError;
                        },
                        builder: (BuildContext context, CareTaskState state) {
                          if (state is CareTasksLoadError) {
                            return LoadErrorWidget(
                                message: state.error,
                                onRetry: _onLoadCareTasks);
                          } else if (state is CareTasksLoadSuccess) {
                            if (_selectedDate.isToday) {
                              return TodayCareTasks(
                                  careProvider: widget.careProvider,
                                  tasks: state.tasks);
                            } else if (_selectedDate.isPast) {
                              return PastCareTasks(
                                tasks: state.tasks,
                                date: _selectedDate,
                                actions: state.actions,
                              );
                            } else {
                              return FutureCareTasks(tasks: state.tasks);
                            }
                          } else {
                            return const LoadingIndicator();
                          }
                        },
                      );
              },
            ),
          ),
        ],
      ),
    );
  }
}
