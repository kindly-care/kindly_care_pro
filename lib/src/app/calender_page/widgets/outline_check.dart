import 'package:flutter/material.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class OutlineCheck extends StatelessWidget {
  final CareActionStatus status;

  const OutlineCheck({super.key, required this.status});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 3.8.w, top: 1.2.h),
      child: Icon(
        _getIcon(),
        color: status == CareActionStatus.incomplete ? Colors.red : Colors.teal,
      ),
    );
  }

  IconData _getIcon() {
    switch (status) {
      case CareActionStatus.complete:
        return Boxicons.bx_check_circle;
      case CareActionStatus.incomplete:
        return Boxicons.bx_x_circle;
      case CareActionStatus.none:
        return Boxicons.bx_circle;
    }
  }
}
