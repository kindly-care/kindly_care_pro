import 'package:collection/collection.dart';
import 'package:dart_date/dart_date.dart';
import 'package:flutter/cupertino.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../widgets/widgets.dart';
import '../../task_detail/meal_task_detail_page.dart';
import '../../task_detail/medication_task_detail_page.dart';
import '../../task_detail/task_detail_page.dart';

class PastCareTasks extends StatelessWidget {
  final DateTime date;
  final List<CareTask> tasks;
  final List<CareAction> actions;
  const PastCareTasks({
    super.key,
    required this.date,
    required this.tasks,
    required this.actions,
  });

  void _onTaskTap(BuildContext context, CareTask task) {
    final CareAction? action = actions.firstWhereOrNull((CareAction action) =>
        action.taskId == task.taskId && action.createdAt.isEqual(date));

    Navigator.push<void>(
      context,
      CupertinoPageRoute<void>(
        fullscreenDialog: true,
        builder: (_) {
          if (task is MealTask) {
            return MealTaskDetailPage(
              task: task,
              readOnly: true,
              action: action as MealAction?,
            );
          } else if (task is MedicationTask) {
            return MedicationTaskDetailPage(
              task: task,
              readOnly: true,
              action: action as MedicationAction?,
            );
          } else {
            return TaskDetailPage(
              task: task,
              action: action,
              readOnly: true,
            );
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (tasks.isEmpty) {
      return Padding(
        padding: EdgeInsets.only(top: 22.0.h),
        child: const NoDataWidget('No Care Tasks found'),
      );
    } else {
      return ListView.builder(
        shrinkWrap: true,
        itemCount: tasks.length,
        physics: const BouncingScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          final CareTask task = tasks[index];
          final CareActionStatus actionStatus;

          final CareAction? action = actions.firstWhereOrNull(
              (CareAction action) => action.taskId == task.taskId);

          if (action != null) {
            actionStatus = action.status;
          } else {
            actionStatus = CareActionStatus.none;
          }
          return CareTaskActionCard(
            task,
            status: actionStatus,
            onTap: () => _onTaskTap(context, task),
          );
        },
      );
    }
  }
}
