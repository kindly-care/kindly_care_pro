import 'package:flutter/cupertino.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../widgets/widgets.dart';
import '../../task_detail/meal_task_detail_page.dart';
import '../../task_detail/medication_task_detail_page.dart';
import '../../task_detail/task_detail_page.dart';

class FutureCareTasks extends StatelessWidget {
  final List<CareTask> tasks;
  const FutureCareTasks({super.key, required this.tasks});

  void _onTaskTap(BuildContext context, CareTask task) {
    Navigator.push<void>(
      context,
      CupertinoPageRoute<void>(
        fullscreenDialog: true,
        builder: (_) {
          if (task is MealTask) {
            return MealTaskDetailPage(task: task, readOnly: true);
          } else if (task is MedicationTask) {
            return MedicationTaskDetailPage(task: task, readOnly: true);
          } else {
            return TaskDetailPage(task: task, readOnly: true);
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (tasks.isEmpty) {
      return Padding(
        padding: EdgeInsets.only(top: 22.0.h),
        child: const NoDataWidget('No Care Tasks found'),
      );
    } else {
      return ListView.builder(
        shrinkWrap: true,
        itemCount: tasks.length,
        physics: const BouncingScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          final CareTask task = tasks[index];
          return CareTaskActionCard(
            task,
            status: CareActionStatus.none,
            onTap: () => _onTaskTap(context, task),
          );
        },
      );
    }
  }
}
