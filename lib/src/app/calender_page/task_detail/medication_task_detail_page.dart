import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../blocs/care_task/care_task_bloc.dart';
import '../../../common/constants/constants.dart';
import '../../widgets/textfields/textfields.dart';

class MedicationTaskDetailPage extends StatefulWidget {
  final bool readOnly;
  final MedicationTask task;
  final MedicationAction? action;
  const MedicationTaskDetailPage({
    super.key,
    this.action,
    required this.task,
    required this.readOnly,
  });

  @override
  State<MedicationTaskDetailPage> createState() => _MealTaskDetailPageState();
}

class _MealTaskDetailPageState extends State<MedicationTaskDetailPage> {
  late CareActionStatus _actionStatus;
  late TextEditingController _commentController;

  @override
  void initState() {
    super.initState();
    _actionStatus = widget.action?.status ?? CareActionStatus.none;
    _commentController = TextEditingController(text: widget.action?.comment);
  }

  @override
  void dispose() {
    _commentController.dispose();
    super.dispose();
  }

  void _onSubmitButtonTap(String careProviderId) {
    final String dateCreated = DateFormat(kYMD).format(DateTime.now());

    final MedicationAction action = MedicationAction(
      status: _actionStatus,
      title: widget.task.title,
      taskType: widget.task.type,
      taskId: widget.task.taskId,
      dosage: widget.task.dosage,
      metric: widget.task.metric,
      actionId: widget.task.title,
      careProviderId: careProviderId,
      patientId: widget.task.patientId,
      description: widget.task.description,
      images: widget.action?.images ?? const <String>[],
      createdAt: widget.action?.createdAt ?? DateTime.now(),
      dateCreated: widget.action?.dateCreated ?? dateCreated,
      comment:
          _commentController.text.isNotEmpty ? _commentController.text : null,
    );

    context.read<CareTaskBloc>().add(SaveCareActionToCache(action: action));
  }

  String _getTaskTitle() {
    final MedicationTask task = widget.task;
    return '${task.title} (${task.dosage} ${task.metric})';
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    final bool isLoading = context.select((CareTaskBloc bloc) => bloc)
        is SaveCareTaskToCacheInProgress;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Medication Task'),
      ),
      body: LoadingOverlay(
        isLoading: isLoading,
        child: BlocListener<CareTaskBloc, CareTaskState>(
          listener: (BuildContext context, CareTaskState state) {
            if (state is SaveCareTaskToCacheError) {
              errorSnackbar(context, state.error);
            } else if (state is SaveCareTaskToCacheSuccess) {
              Navigator.pop(context);
              successSnackbar(context, state.message);
            }
          },
          child: ListView(
            physics: const BouncingScrollPhysics(),
            padding: EdgeInsets.symmetric(horizontal: 3.0.w),
            children: <Widget>[
              SizedBox(height: 3.5.h),
              Text(
                _getTaskTitle(),
                overflow: TextOverflow.ellipsis,
                style: theme.titleLarge,
              ),
              SizedBox(height: 1.5.h),
              Text(
                widget.task.description ?? kNoAdditionalInfo,
                style: theme.bodyLarge,
                overflow: TextOverflow.ellipsis,
              ),
              SizedBox(height: 4.5.h),
              Text(kTaskComplete, style: theme.bodyLarge),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: ElevatedButton(
                      onPressed: () {
                        if (!widget.readOnly) {
                          setState(
                              () => _actionStatus = CareActionStatus.complete);
                        } else {
                          showToast(message: kActionNotAllowed);
                        }
                      },
                      style: ElevatedButton.styleFrom(
                        elevation: 0.2,
                        primary: _actionStatus == CareActionStatus.complete
                            ? Colors.teal
                            : Colors.teal.shade100,
                      ),
                      child: const Icon(Icons.check),
                    ),
                  ),
                  SizedBox(width: 2.0.w),
                  Expanded(
                    child: ElevatedButton(
                      onPressed: () {
                        if (!widget.readOnly) {
                          setState(() =>
                              _actionStatus = CareActionStatus.incomplete);
                        } else {
                          showToast(message: kActionNotAllowed);
                        }
                      },
                      style: ElevatedButton.styleFrom(
                        elevation: 0.2,
                        primary: _actionStatus == CareActionStatus.incomplete
                            ? Colors.red
                            : Colors.red.shade100,
                      ),
                      child: const Icon(Icons.close),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 6.0.h),
              OutlineTextField(
                maxLines: null,
                labelText: _actionStatus == CareActionStatus.incomplete
                    ? 'Comment (Required)'
                    : 'Comment (Optional)',
                readOnly: widget.readOnly,
                controller: _commentController,
                hintText: _actionStatus == CareActionStatus.incomplete
                    ? 'Why was this task not completed?'
                    : 'Type comment',
              ),
              SizedBox(height: 30.0.h),
              ActionButton(
                title: 'Submit',
                enabled: _getButtonEnabled(),
                onPressed: () => _onSubmitButtonTap(user.uid),
              ),
            ],
          ),
        ),
      ),
    );
  }

  bool _getButtonEnabled() {
    if (_actionStatus == CareActionStatus.complete && !widget.readOnly) {
      return true;
    } else if (_actionStatus == CareActionStatus.incomplete &&
        _commentController.text.isNotEmpty &&
        !widget.readOnly) {
      return true;
    } else {
      return false;
    }
  }
}
