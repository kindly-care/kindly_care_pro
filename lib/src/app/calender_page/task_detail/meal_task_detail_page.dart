import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../blocs/care_task/care_task_bloc.dart';
import '../../../common/constants/constants.dart';
import '../../widgets/textfields/textfields.dart';

class MealTaskDetailPage extends StatefulWidget {
  final MealTask task;
  final bool readOnly;
  final MealAction? action;
  const MealTaskDetailPage({
    super.key,
    this.action,
    required this.task,
    required this.readOnly,
  });

  @override
  State<MealTaskDetailPage> createState() => _MealTaskDetailPageState();
}

class _MealTaskDetailPageState extends State<MealTaskDetailPage> {
  late CareActionStatus _actionStatus;
  late TextEditingController _mealController;
  late TextEditingController _commentController;

  @override
  void initState() {
    super.initState();

    _actionStatus = widget.action?.status ?? CareActionStatus.none;
    _mealController = TextEditingController(text: widget.action?.meal);
    _commentController = TextEditingController(text: widget.action?.comment);
  }

  @override
  void dispose() {
    _mealController.dispose();
    _commentController.dispose();
    super.dispose();
  }

  void _onSubmitButtonTap(String careProviderId) {
    final String dateCreated = DateFormat(kYMD).format(DateTime.now());
    final MealAction action = MealAction(
      status: _actionStatus,
      title: widget.task.title,
      taskType: widget.task.type,
      meal: _mealController.text,
      taskId: widget.task.taskId,
      actionId: widget.task.title,
      mealType: widget.task.mealType,
      careProviderId: careProviderId,
      patientId: widget.task.patientId,
      images: widget.action?.images ?? const <String>[],
      createdAt: widget.action?.createdAt ?? DateTime.now(),
      dateCreated: widget.action?.dateCreated ?? dateCreated,
      comment:
          _commentController.text.isNotEmpty ? _commentController.text : null,
    );

    context.read<CareTaskBloc>().add(SaveCareActionToCache(action: action));
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final AppUser user = context.select((AuthBloc bloc) => bloc.user);
    final bool isLoading = context.select((CareTaskBloc bloc) => bloc.state)
        is SaveCareTaskToCacheInProgress;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Meal Task'),
      ),
      body: LoadingOverlay(
        isLoading: isLoading,
        child: BlocListener<CareTaskBloc, CareTaskState>(
          listener: (BuildContext context, CareTaskState state) {
            if (state is SaveCareTaskToCacheError) {
              errorSnackbar(context, state.error);
            } else if (state is SaveCareTaskToCacheSuccess) {
              Navigator.pop(context);
              successSnackbar(context, state.message);
            }
          },
          child: ListView(
            physics: const BouncingScrollPhysics(),
            padding: EdgeInsets.symmetric(horizontal: 3.0.w),
            children: <Widget>[
              SizedBox(height: 3.5.h),
              Text(
                widget.task.title,
                style: theme.titleLarge,
                overflow: TextOverflow.ellipsis,
              ),
              SizedBox(height: 1.5.h),
              Text(
                widget.task.description ?? kNoAdditionalInfo,
                style: theme.bodyLarge,
                overflow: TextOverflow.ellipsis,
              ),
              SizedBox(height: 4.5.h),
              Text(kTaskComplete, style: theme.bodyLarge),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: ElevatedButton(
                      onPressed: () {
                        if (!widget.readOnly) {
                          setState(
                              () => _actionStatus = CareActionStatus.complete);
                        } else {
                          showToast(message: kActionNotAllowed);
                        }
                      },
                      style: ElevatedButton.styleFrom(
                        elevation: 0.2,
                        primary: _actionStatus == CareActionStatus.complete
                            ? Colors.teal
                            : Colors.teal.shade100,
                      ),
                      child: const Icon(Icons.check),
                    ),
                  ),
                  SizedBox(width: 2.0.w),
                  Expanded(
                    child: ElevatedButton(
                      onPressed: () {
                        if (!widget.readOnly) {
                          setState(() =>
                              _actionStatus = CareActionStatus.incomplete);
                        } else {
                          showToast(message: kActionNotAllowed);
                        }
                      },
                      style: ElevatedButton.styleFrom(
                        elevation: 0.2,
                        primary: _actionStatus == CareActionStatus.incomplete
                            ? Colors.red
                            : Colors.red.shade100,
                      ),
                      child: const Icon(Icons.close),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 6.0.h),
              Visibility(
                visible: _actionStatus == CareActionStatus.complete,
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 1.0.h),
                    OutlineTextField(
                      maxLines: null,
                      labelText: 'Meal',
                      readOnly: widget.readOnly,
                      controller: _mealController,
                      hintText: 'What did the patient ate?',
                    ),
                    SizedBox(height: 5.0.h),
                  ],
                ),
              ),
              SizedBox(height: 1.0.h),
              OutlineTextField(
                maxLines: null,
                labelText: _actionStatus == CareActionStatus.incomplete
                    ? 'Comment (Required)'
                    : 'Comment (Optional)',
                readOnly: widget.readOnly,
                controller: _commentController,
                hintText: _actionStatus == CareActionStatus.incomplete
                    ? 'Why was this task not completed?'
                    : 'Type comment',
              ),
              SizedBox(
                height: _actionStatus == CareActionStatus.complete
                    ? 15.0.h
                    : 30.0.h,
              ),
              ActionButton(
                title: 'Submit',
                enabled: _getButtonEnabled(),
                onPressed: () => _onSubmitButtonTap(user.uid),
              ),
            ],
          ),
        ),
      ),
    );
  }

  bool _getButtonEnabled() {
    if (_actionStatus == CareActionStatus.complete &&
        _mealController.text.isNotEmpty &&
        !widget.readOnly) {
      return true;
    } else if (_actionStatus == CareActionStatus.incomplete &&
        _commentController.text.isNotEmpty &&
        !widget.readOnly) {
      return true;
    } else {
      return false;
    }
  }
}
