import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:uuid/uuid.dart';

import '../../blocs/notifications/notification_bloc.dart';
import '../../common/common.dart';

enum _NotificationsPopupAction { delete }

class NotificationsPage extends StatefulWidget {
  final CareProvider careProvider;

  const NotificationsPage({super.key, required this.careProvider});

  @override
  State<NotificationsPage> createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  @override
  void initState() {
    super.initState();
    _fetchNotifications();
  }

  void _fetchNotifications() {
    context
        .read<NotificationBloc>()
        .add(FetchNotifications(uid: widget.careProvider.uid));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Notifications'),
        actions: <Widget>[
          _NotificationsPopupButton(careProvider: widget.careProvider),
        ],
      ),
      body: BlocConsumer<NotificationBloc, NotificationState>(
        listener: (BuildContext context, NotificationState state) {
          if (state is SendNotificationSuccess) {
            successSnackbar(context, 'Operation success.');
          } else if (state is SendNotificationError) {
            errorSnackbar(context, state.error);
          } else if (state is DeleteNotificationSuccess) {
            successSnackbar(context, state.message);
          } else if (state is DeleteNotificationError) {
            errorSnackbar(context, state.error);
          }
        },
        buildWhen: (_, NotificationState current) {
          return current is NotificationsLoading ||
              current is NotificationsLoadSuccess ||
              current is NotificationsLoadError;
        },
        builder: (BuildContext context, NotificationState state) {
          if (state is NotificationsLoadError) {
            return LoadErrorWidget(
                message: state.error, onRetry: _fetchNotifications);
          } else if (state is NotificationsLoadSuccess) {
            return _NotificationsPageView(widget.careProvider);
          } else {
            return const LoadingIndicator();
          }
        },
      ),
    );
  }
}

class _NotificationsPageView extends StatefulWidget {
  final CareProvider careProvider;
  const _NotificationsPageView(this.careProvider, {Key? key}) : super(key: key);

  @override
  State<_NotificationsPageView> createState() => _NotificationsPageViewState();
}

class _NotificationsPageViewState extends State<_NotificationsPageView> {
  void _onNotificationTap(PushNotification notification) {
    if (!notification.isSeen) {
      context.read<NotificationBloc>().add(MarkNotificationAsSeen(
          uid: widget.careProvider.uid, notificationId: notification.id));
    }

    if (notification is InterviewNotification) {
      _showInterviewNotificationDialog(notification);
    } else {
      _showGenericNotificationDialog(notification);
    }
  }

  void _onCareInterviewNotificationTap(
      InterviewNotification notification, bool accepts) {
    final String notificationId = const Uuid().v4();
    final CareProvider careProvider = widget.careProvider;

    final PushNotification pushNotification = PushNotification(
      authorId: careProvider.uid,
      authorName: careProvider.name,
      authorPhone: widget.careProvider.phone,
      title: kCareInterviewRequest,
      id: notificationId,
      text:
          'Care Interview Request for ${careProvider.name} has been ${accepts ? 'accepted.' : 'rejected.'}',
      createdAt: DateTime.now(),
      recipients: <String>[notification.authorId],
      type: NotificationType.info,
    );
    context
        .read<NotificationBloc>()
        .add(SendPushNotification(notification: pushNotification));
    context.read<NotificationBloc>().add(ChangeNotificationStatus(
        notificationId: notification.id, uid: widget.careProvider.uid));
  }

  void _onNotificationLongPress(PushNotification notification) {
    HapticFeedback.lightImpact();
    if (!notification.isSeen) {
      context.read<NotificationBloc>().add(MarkNotificationAsSeen(
          notificationId: notification.id, uid: widget.careProvider.uid));
    }
    if (notification is InterviewNotification) {
      _onInterviewNotificationLongPress(notification);
    } else {
      _onPushNotificationLongPress(notification);
    }
  }

  @override
  Widget build(BuildContext context) {
    final List<PushNotification> notifications =
        context.select((NotificationBloc bloc) => bloc.notifications);
    final bool isLoading = context.select((NotificationBloc bloc) {
      return bloc is DeleteNotificationInProgress ||
          bloc is SendNotificationInProgress;
    });

    if (notifications.isEmpty) {
      return const NoDataWidget(kNoNotifications);
    } else {
      return LoadingOverlay(
        isLoading: isLoading,
        child: ListView.builder(
          padding: EdgeInsets.all(2.0.w),
          physics: const BouncingScrollPhysics(),
          itemCount: notifications.length,
          itemBuilder: (BuildContext context, int index) {
            final PushNotification notification = notifications[index];
            return NotificationCard(
              notification: notification,
              onTap: () => _onNotificationTap(notification),
              onLongPress: () => _onNotificationLongPress(notification),
            );
          },
        ),
      );
    }
  }

  void _showInterviewNotificationDialog(InterviewNotification notification) {
    showThreeButtonDialog(
      context,
      title: notification.title,
      content: notification.text,
      buttonText1: 'Reject',
      buttonText2: 'Accept',
      buttonText3: 'View Patient Profile',
      isButtonText1Disabled: notification.isActedOn,
      isButtonText2Disabled: notification.isActedOn,
      onPressed1: () {
        Navigator.pop(context);
        if (!notification.isActedOn) {
          _onCareInterviewNotificationTap(notification, false);
        }
      },
      onPressed2: () {
        Navigator.pop(context);
        if (!notification.isActedOn) {
          _onCareInterviewNotificationTap(notification, true);
        }
      },
      onPressed3: () {
        Navigator.popAndPushNamed(context, kPatientProfilePageRoute,
            arguments: notification.patientId);
      },
    );
  }

  void _showGenericNotificationDialog(PushNotification notification) {
    showOneButtonDialog(
      context,
      title: notification.title,
      content: notification.text,
      buttonText: 'Ok',
      onPressed: () => Navigator.pop(context),
    );
  }

  void _onInterviewNotificationLongPress(InterviewNotification notification) {
    final String name = notification.authorName.firstName();
    optionsBottomSheet(
      height: 32.0.h,
      context: context,
      title: notification.title,
      options: <Widget>[
        IconListTile(
          title: 'Call $name',
          showTrailing: false,
          icon: FeatherIcons.phone,
          onTap: () {
            Navigator.pop(context);
            onMakeCall(notification.authorPhone);
          },
        ),
        Divider(indent: 18.w),
        IconListTile(
          title: 'Delete Notification',
          showTrailing: false,
          icon: FeatherIcons.trash2,
          onTap: () {
            Navigator.pop(context);
            context.read<NotificationBloc>().add(DeleteNotification(
                  uid: widget.careProvider.uid,
                  notificationId: notification.id,
                ));
          },
        ),
      ],
    );
  }

  void _onPushNotificationLongPress(PushNotification notification) {
    final String name = notification.authorName.firstName();
    optionsBottomSheet(
      height: 32.0.h,
      context: context,
      title: notification.title,
      options: <Widget>[
        IconListTile(
          title: 'Call $name',
          showTrailing: false,
          icon: FeatherIcons.phone,
          onTap: () {
            Navigator.pop(context);
            onMakeCall(notification.authorPhone);
          },
        ),
        Divider(indent: 18.w),
        IconListTile(
          title: 'Delete Notification',
          showTrailing: false,
          icon: FeatherIcons.trash2,
          onTap: () {
            Navigator.pop(context);
            context.read<NotificationBloc>().add(DeleteNotification(
                  uid: widget.careProvider.uid,
                  notificationId: notification.id,
                ));
          },
        ),
      ],
    );
  }
}

class _NotificationsPopupButton extends StatelessWidget {
  final CareProvider careProvider;
  const _NotificationsPopupButton({Key? key, required this.careProvider})
      : super(key: key);

  void _showConfirmDeleteDialog(BuildContext context) {
    showTwoButtonDialog(
      context,
      isDestructiveAction: true,
      title: 'Delete Notifications',
      content: 'Are you sure you want to delete all your notifications?',
      buttonText1: 'Exit',
      buttonText2: 'Delete',
      onPressed1: () => Navigator.pop(context),
      onPressed2: () {
        Navigator.pop(context);
        context
            .read<NotificationBloc>()
            .add(DeleteAllNotifications(uid: careProvider.uid));
      },
    );
  }

  void _onPopupAction(BuildContext context, _NotificationsPopupAction action) {
    switch (action) {
      case _NotificationsPopupAction.delete:
        _showConfirmDeleteDialog(context);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final bool notificationsIsNotEmpty = context
        .select((NotificationBloc bloc) => bloc.notifications)
        .isNotEmpty;
    return PopupMenuButton<_NotificationsPopupAction>(
      icon: Icon(Icons.more_vert_outlined, size: 3.8.h),
      onSelected: (_NotificationsPopupAction value) {
        _onPopupAction(context, value);
      },
      itemBuilder: (BuildContext context) {
        return <PopupMenuEntry<_NotificationsPopupAction>>[
          PopupMenuItem<_NotificationsPopupAction>(
            enabled: notificationsIsNotEmpty,
            padding: EdgeInsets.only(left: 1.0.w),
            value: _NotificationsPopupAction.delete,
            child: MenuElement(
              title: 'Delete All Notifications',
              enabled: notificationsIsNotEmpty,
            ),
          ),
        ];
      },
    );
  }
}
