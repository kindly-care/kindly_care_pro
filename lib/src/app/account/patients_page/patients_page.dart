import 'package:cached_network_image/cached_network_image.dart';
import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../blocs/care_provider/care_provider_bloc.dart';
import '../../../common/common.dart';
import '../../../cubits/account_cubit/account_cubit.dart';

class PatientsPage extends StatefulWidget {
  final CareProvider careProvider;

  const PatientsPage({super.key, required this.careProvider});

  @override
  State<PatientsPage> createState() => _PatientsPageState();
}

class _PatientsPageState extends State<PatientsPage> {
  void _onPatientTap(Patient patient) {
    Navigator.pushNamed(context, kPatientProfilePageRoute,
        arguments: patient.uid);
  }

  void _onRemovePatient(Patient patient) {
    context.read<CareProviderBloc>().add(RemovePatient(
        careProviderId: widget.careProvider.uid, patientId: patient.uid));
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final List<Patient> patients =
        context.select((AccountCubit cubit) => cubit.state.patients);
    final bool isLoading = context.select((CareProviderBloc bloc) => bloc.state)
        is RemovePatientInProgress;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Patients'),
      ),
      body: LoadingOverlay(
        isLoading: isLoading,
        child: patients.isEmpty
            ? const NoDataWidget(
                'You currently do not have any Patients. Your Patients will be displayed here.')
            : ListView.separated(
                shrinkWrap: true,
                itemCount: patients.length,
                padding: EdgeInsets.only(top: 0.15.h),
                physics: const BouncingScrollPhysics(),
                separatorBuilder: (BuildContext context, int index) =>
                    Divider(indent: 22.0.w, endIndent: 8.0.w),
                itemBuilder: (BuildContext context, int index) {
                  final Patient patient = patients[index];
                  return Padding(
                    padding: EdgeInsets.all(1.2.h),
                    child: ListTile(
                      onTap: () => _onPatientTap(patient),
                      onLongPress: () => _onPatientLongPress(patient),
                      leading: CircleAvatar(
                        radius: 3.6.h,
                        backgroundImage:
                            CachedNetworkImageProvider(patient.avatar),
                      ),
                      title: Text(
                        patient.name,
                        overflow: TextOverflow.ellipsis,
                        style: theme.titleMedium,
                      ),
                      subtitle: Text(
                        patient.address?.suburb ?? kUnknownAddress,
                        style: theme.titleSmall,
                      ),
                      trailing: Icon(Icons.chevron_right, size: 3.8.h),
                    ),
                  );
                },
              ),
      ),
    );
  }

  void _onPatientLongPress(Patient patient) {
    HapticFeedback.lightImpact();
    optionsBottomSheet(
      context: context,
      title: patient.name,
      height: 45.0.h,
      options: <Widget>[
        IconListTile(
          title: 'Call',
          showTrailing: false,
          icon: FeatherIcons.phone,
          onTap: () {
            Navigator.pop(context);
            onMakeCall(patient.phone);
          },
        ),
        Divider(indent: 18.w),
        IconListTile(
          title: 'View Profile',
          showTrailing: false,
          icon: FeatherIcons.user,
          onTap: () {
            Navigator.pop(context);
            _onPatientTap(patient);
          },
        ),
        Divider(indent: 18.w),
        IconListTile(
          title: 'Remove Patient',
          showTrailing: false,
          icon: FeatherIcons.userMinus,
          onTap: () {
            Navigator.pop(context);
            _showRemovePatientDialog(patient);
          },
        ),
      ],
    );
  }

  void _showRemovePatientDialog(Patient patient) {
    showTwoButtonDialog(
      context,
      isDestructiveAction: true,
      title: 'Confirm Action',
      content:
          'Are you sure you want to remove this patient? Please ensure that a replacement Care Agent has been found before you continue.',
      buttonText1: 'Cancel',
      buttonText2: 'Remove',
      onPressed1: () => Navigator.pop(context),
      onPressed2: () {
        Navigator.pop(context);
        _onRemovePatient(patient);
      },
    );
  }
}
