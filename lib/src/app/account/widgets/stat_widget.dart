import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import 'counter.dart';

class StatWidget extends StatelessWidget {
  final String stat;
  final String title;
  const StatWidget({super.key, required this.stat, required this.title});

  @override
  Widget build(BuildContext context) {
    final double statDouble = double.tryParse(stat) ?? 0.0;
    final TextTheme theme = Theme.of(context).textTheme;
    return Column(
      children: <Widget>[
        Text(
          title,
          style:
              theme.bodyLarge?.copyWith(color: Colors.black.withOpacity(0.5)),
        ),
        SizedBox(height: 0.8.h),
        Counter(
          begin: 0,
          end: statDouble,
          duration: const Duration(seconds: 2),
          style: theme.bodyLarge?.copyWith(fontWeight: FontWeight.w600),
        ),
      ],
    );
  }
}
