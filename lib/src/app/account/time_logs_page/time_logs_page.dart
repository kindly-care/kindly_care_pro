import 'package:dart_date/dart_date.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../blocs/time_logs/time_logs_bloc.dart';
import '../../../common/common.dart';
import '../../../data/services/time_log/time_log_service.dart';

class TimeLogsPage extends StatelessWidget {
  final String patientId;
  final CareProvider careProvider;

  const TimeLogsPage(
      {super.key, required this.careProvider, required this.patientId});

  @override
  Widget build(BuildContext context) {
    final int month = DateTime.now().month;
    return BlocProvider<TimeLogsBloc>(
      create: (BuildContext context) => TimeLogsBloc(
        timeLogRepository: context.read<TimeLogService>(),
      )..add(FetchTimeLogs(
          month: month,
          patientId: patientId,
          careProviderId: careProvider.uid,
        )),
      child:
          _TimeLogsPageView(careProvider: careProvider, patientId: patientId),
    );
  }
}

class _TimeLogsPageView extends StatefulWidget {
  final String patientId;
  final CareProvider careProvider;

  const _TimeLogsPageView(
      {Key? key, required this.careProvider, required this.patientId})
      : super(key: key);

  @override
  State<_TimeLogsPageView> createState() => _TimeLogsPageViewState();
}

class _TimeLogsPageViewState extends State<_TimeLogsPageView> {
  int _selectedMonth = DateTime.now().month;

  void _onLoadTimeLogs() {
    context.read<TimeLogsBloc>().add(FetchTimeLogs(
          month: _selectedMonth,
          patientId: widget.patientId,
          careProviderId: widget.careProvider.uid,
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Time Logs'),
        actions: <Widget>[
          MonthPopupButton(
            icon: Icons.calendar_today_outlined,
            onChanged: (int month) {
              setState(() => _selectedMonth = month);
              _onLoadTimeLogs();
            },
          ),
        ],
      ),
      body: BlocBuilder<TimeLogsBloc, TimeLogsState>(
        builder: (BuildContext context, TimeLogsState state) {
          if (state is TimeLogsLoadError) {
            return LoadErrorWidget(
                message: state.error, onRetry: _onLoadTimeLogs);
          } else if (state is TimeLogsLoadSuccess) {
            return _PageContent(
                month: _selectedMonth, timeLogs: state.timelogs);
          } else {
            return const LoadingIndicator();
          }
        },
      ),
    );
  }
}

class _PageContent extends StatelessWidget {
  final int month;
  final List<TimeLog> timeLogs;
  const _PageContent({Key? key, required this.month, required this.timeLogs})
      : super(key: key);

  String _getHoursFromMinutes(int totalMinutes) {
    final int hour = totalMinutes ~/ 60;
    final int minutes = totalMinutes % 60;

    if (hour < 10 && minutes < 10) {
      return '0$hour:0${minutes.toString()}';
    } else if (hour < 10 && minutes > 10) {
      return '0$hour:${minutes.toString()}';
    } else if (hour > 10 && minutes < 10) {
      return '$hour:0${minutes.toString()}';
    } else {
      return '${hour.toString()}:${minutes.toString()}';
    }
  }

  static List<String> columns = <String>[
    'Patient',
    'Start',
    'Finish',
    'Hours',
  ];

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final DateTime date = DateTime.now().setMonth(month);
    final String monthYear = DateFormat.yMMMM('en_GB').format(date);
    if (timeLogs.isEmpty) {
      return NoDataWidget('No Time Logs found for $monthYear.');
    } else {
      return InteractiveViewer(
        alignPanAxis: true,
        constrained: false,
        scaleEnabled: false,
        child: DataTable(
          columnSpacing: 6.5.w,
          horizontalMargin: 2.5.w,
          columns: columns
              .map((String item) => DataColumn(
                      label: Text(
                    item,
                    style: theme.bodyMedium?.copyWith(
                      fontWeight: FontWeight.w600,
                      color: Colors.black.withOpacity(0.7),
                    ),
                  )))
              .toList(),
          rows: timeLogs.map((TimeLog timeLog) {
            final String start = DateFormat('dd/MM', 'en_GB')
                .add_jm()
                .format(timeLog.startTime!);
            final String finish = DateFormat('dd/MM', 'en_GB')
                .add_jm()
                .format(timeLog.finishTime!);

            final int diffInMinutes =
                timeLog.finishTime!.difference(timeLog.startTime!).inMinutes;

            final String totalHours = _getHoursFromMinutes(diffInMinutes);

            final String name = timeLog.patientName.lastNameFirstInitial();

            final List<String> cells = <String>[
              name,
              start,
              finish,
              totalHours,
            ];
            return DataRow(
              cells: cells
                  .map((String item) =>
                      DataCell(Text(item, style: theme.bodyMedium)))
                  .toList(),
            );
          }).toList()
            ..add(_totalRow(context)),
        ),
      );
    }
  }

  DataRow _totalRow(BuildContext context) {
    final TextStyle? style = Theme.of(context).textTheme.bodyMedium?.copyWith(
          fontWeight: FontWeight.w600,
          color: Colors.black.withOpacity(0.7),
        );
    return DataRow(cells: <DataCell>[
      DataCell(Text('Total Hours', style: style)),
      const DataCell(Text('')),
      const DataCell(Text('')),
      DataCell(Text(hoursFromTimeLogsLong(timeLogs), style: style)),
    ]);
  }
}
