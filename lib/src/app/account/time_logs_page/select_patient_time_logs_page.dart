import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../cubits/account_cubit/account_cubit.dart';
import 'time_logs_page.dart';

class SelectPatientTimelogsPage extends StatelessWidget {
  final CareProvider careProvider;

  const SelectPatientTimelogsPage({super.key, required this.careProvider});

  void _onPatientTap(BuildContext context, String patientId) {
    Navigator.push(
      context,
      CupertinoPageRoute<void>(
        builder: (_) =>
            TimeLogsPage(careProvider: careProvider, patientId: patientId),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    final List<Patient> patients =
        context.select((AccountCubit cubit) => cubit.state.patients);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Select Patient'),
      ),
      body: patients.isEmpty
          ? const NoDataWidget(
              'You currently do not have any Patients. Your Patients will be displayed here.')
          : ListView.separated(
              shrinkWrap: true,
              itemCount: patients.length,
              padding: EdgeInsets.only(top: 0.15.h),
              physics: const BouncingScrollPhysics(),
              separatorBuilder: (BuildContext context, int index) =>
                  Divider(indent: 22.0.w, endIndent: 8.0.w),
              itemBuilder: (BuildContext context, int index) {
                final Patient patient = patients[index];
                return Padding(
                  padding: EdgeInsets.all(1.2.h),
                  child: ListTile(
                    onTap: () => _onPatientTap(context, patient.uid),
                    leading: CircleAvatar(
                      radius: 3.6.h,
                      backgroundImage:
                          CachedNetworkImageProvider(patient.avatar),
                    ),
                    title: Text(
                      patient.name,
                      style: theme.titleMedium,
                      overflow: TextOverflow.ellipsis,
                    ),
                    subtitle: Text(
                      patient.address?.suburb ?? kUnknownAddress,
                      style: theme.titleSmall,
                    ),
                    trailing: Icon(Icons.chevron_right, size: 3.8.h),
                  ),
                );
              },
            ),
    );
  }
}
