
import 'package:collection/collection.dart';
import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../common/common.dart';
import '../../cubits/account_cubit/account_cubit.dart';
import '../misc/patient_profile/patient_records_page/patient_records_page.dart';
import '../shift_report_page/shift_report_page.dart';
import 'time_logs_page/time_logs_page.dart';
import 'widgets/widgets.dart';

class AccountPage extends StatelessWidget {
  final CareProvider careProvider;

  const AccountPage({super.key, required this.careProvider});

  void _onPatientsTap(BuildContext context) {
    Navigator.pushNamed(context, kPatientsPageRoute, arguments: careProvider);
  }

  void _onAvatarTap(BuildContext context) {
    Navigator.pushNamed(context, kCareProviderProfilePageRoute,
        arguments: careProvider.uid);
  }

  void _onRecordVitalsTap(BuildContext context, {TimeLog? timelog}) {
    if (timelog == null) {
      _showClockedOutDialog(context, 'record Patient Vitals');
    } else {
      Navigator.push(
        context,
        CupertinoPageRoute<void>(
          builder: (_) => PatientRecordsPage(
            careProvider: careProvider,
            patientId: timelog.patientId,
            patientName: timelog.patientName,
          ),
        ),
      );
    }
  }

  void _onLogReportTap(BuildContext context, {TimeLog? timelog}) {
    if (timelog == null) {
      _showClockedOutDialog(context, 'log Care Reports');
    } else {
      Navigator.push(
        context,
        CupertinoPageRoute<String>(
          builder: (_) =>
              ShiftReportPage(careProvider: careProvider, timelog: timelog),
        ),
      );
    }
  }

  void _onTimeLogsTap(BuildContext context, List<Patient> patients) {
    if (patients.length == 1) {
      Navigator.push(
        context,
        CupertinoPageRoute<void>(
          builder: (_) => TimeLogsPage(
              careProvider: careProvider, patientId: patients.first.uid),
        ),
      );
    } else {
      Navigator.pushNamed(context, kPatientSelectPageRoute,
          arguments: careProvider);
    }
  }

  @override
  Widget build(BuildContext context) {
    final List<Patient> patients = context.watch<AccountCubit>().state.patients;
    final List<TimeLog> timeLogs = context.watch<AccountCubit>().state.timelogs;
    return Scaffold(
      body: ValueListenableBuilder<Box<TimeLog>>(
        valueListenable: TimeLogBox.box.listenable(),
        builder: (BuildContext context, Box<TimeLog> box, _) {
          final TimeLog? timelog = box.values.firstWhereOrNull(
              (TimeLog timelog) => timelog.finishTime == null);
          return CustomScrollView(
            physics: const BouncingScrollPhysics(),
            slivers: <Widget>[
              SliverToBoxAdapter(child: SizedBox(height: 8.0.h)),
              SliverList(
                delegate: SliverChildListDelegate(
                  <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        GestureDetector(
                          child: BorderAvatar(
                            radius: 62.5,
                            borderWidth: 3.0,
                            imageUrl: careProvider.avatar,
                            borderColor: Colors.blueGrey,
                          ),
                          onTap: () => _onAvatarTap(context),
                        ),
                        SizedBox(height: 1.5.h),
                        Text(
                          careProvider.name,
                          style: Theme.of(context).textTheme.titleLarge,
                        ),
                      ],
                    ),
                    SizedBox(height: 1.8.h),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: () => _onPatientsTap(context),
                            child: StatWidget(
                              title: 'Patients',
                              stat: patients.length.toString(),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 5.0.h,
                          child: const VerticalDivider(thickness: 2),
                        ),
                        Expanded(
                          child: GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: () => _onTimeLogsTap(context, patients),
                            child: StatWidget(
                              title: 'Hours',
                              stat: hoursFromTimeLogsShort(timeLogs),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 1.0.h),
                    Divider(height: 1.h),
                    IconListTile(
                      title: 'Patients',
                      subtitle: 'View Patients',
                      icon: Icons.elderly_outlined,
                      onTap: () => _onPatientsTap(context),
                    ),
                    Divider(indent: 18.w),
                    IconListTile(
                      title: 'Time Logs',
                      icon: Icons.pending_actions_outlined,
                      subtitle: 'View Time Logs',
                      onTap: () => _onTimeLogsTap(context, patients),
                    ),
                    Divider(indent: 18.w),
                    IconListTile(
                      title: 'Care Report',
                      subtitle: 'Log Care Report',
                      icon: Boxicons.bx_clipboard,
                      onTap: () => _onLogReportTap(context, timelog: timelog),
                    ),
                    Divider(indent: 18.w),
                    IconListTile(
                      title: 'Record Vitals',
                      icon: FeatherIcons.activity,
                      subtitle: 'Record Patient Vitals',
                      onTap: () =>
                          _onRecordVitalsTap(context, timelog: timelog),
                    ),
                    Divider(indent: 18.w),
                    IconListTile(
                      title: 'Log Out',
                      icon: Boxicons.bx_log_out,
                      subtitle: 'Log Out Of App',
                      onTap: () => _showConfirmLogOutDialog(context),
                    ),
                    SizedBox(height: 1.5.h),
                  ],
                ),
              ),
            ],
          );
        },
      ),
    );
  }

  void _showConfirmLogOutDialog(BuildContext context) {
    showTwoButtonDialog(
      context,
      title: 'Confirm Action',
      content: 'Are you sure you want to log out of the app?',
      buttonText1: 'No',
      buttonText2: 'Yes',
      onPressed1: () => Navigator.pop(context),
      onPressed2: () async {
        Navigator.pop(context);
        context.read<AuthBloc>().add(Logout());
        SystemNavigator.pop();
      },
    );
  }

  void _showClockedOutDialog(BuildContext context, String message) {
    showOneButtonDialog(
      context,
      title: 'Not Allowed',
      content:
          'You are currently not Clocked-In. You need to be Clocked-In to $message.',
      buttonText: 'Ok',
      onPressed: () => Navigator.pop(context),
    );
  }
}
