import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';

part 'settings_event.dart';
part 'settings_state.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  final AuthRepository _authRepository;

  SettingsBloc({
    required AuthRepository authRepository,
  })  : _authRepository = authRepository,
        super(SettingsInitial()) {
    on<DeleteAccountData>(_onDeleteAccountData);
  }

  Future<void> _onDeleteAccountData(
      DeleteAccountData event, Emitter<SettingsState> emit) async {
    emit(DeleteAccountInProgress());

    try {
      await _authRepository.deleteAccount();
      emit(const DeleteAccountSuccess(message: 'Account deleted.'));
    } on UpdateDataException catch (e) {
      emit(DeleteAccountError(error: e.toString()));
    }
  }
}
