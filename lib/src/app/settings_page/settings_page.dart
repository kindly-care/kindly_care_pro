import 'package:feather_icons/feather_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:in_app_review/in_app_review.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:share_plus/share_plus.dart';
import 'package:wiredash/wiredash.dart';

import '../../common/common.dart';
import '../../data/services/authentication/auth_service.dart';
import 'bloc/settings_bloc.dart';

class SettingsPage extends StatelessWidget {
  final CareProvider careProvider;

  const SettingsPage({super.key, required this.careProvider});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<SettingsBloc>(
      create: (BuildContext context) => SettingsBloc(
        authRepository: context.read<AuthService>(),
      ),
      child: _SettingsPageView(careProvider),
    );
  }
}

class _SettingsPageView extends StatefulWidget {
  final CareProvider careProvider;

  const _SettingsPageView(this.careProvider, {Key? key}) : super(key: key);

  @override
  State<_SettingsPageView> createState() => _SettingsPageViewState();
}

class _SettingsPageViewState extends State<_SettingsPageView> {
  final InAppReview _appReview = InAppReview.instance;

  Future<void> _onAboutTap() async {
    final PackageInfo info = await PackageInfo.fromPlatform();

    showAboutDialog(
      context: context,
      applicationName: 'Kindly Care',
      applicationVersion: info.version,
      applicationLegalese: kAbout,
    );
  }

  void _onDeleteAccount() {
    context.read<SettingsBloc>().add(const DeleteAccountData());
  }

  void _onRecommendAppTap() => Share.share(kAppURL);

  Future<void> _onFeedbackTap() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    if (mounted) {
      final WiredashController wiredashController = Wiredash.of(context);

      wiredashController.setBuildProperties(
        buildVersion: info.version,
        buildNumber: info.buildNumber,
      );
      wiredashController.setUserProperties(
        userId: widget.careProvider.uid,
        userEmail: widget.careProvider.email,
      );

      wiredashController.show();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings'),
      ),
      body: BlocListener<SettingsBloc, SettingsState>(
        listener: (BuildContext context, SettingsState state) {
          if (state is DeleteAccountError) {
            _showDeleteAccountErrorDialog();
          }
        },
        child: ListView(
          padding: EdgeInsets.all(3.0.w),
          physics: const BouncingScrollPhysics(),
          children: <Widget>[
            TextListTile(
              title: 'Feedback',
              subtitle: 'Give Feedback',
              onTap: _onFeedbackTap,
            ),
            const Divider(),
            TextListTile(
              title: 'Recommend App',
              subtitle: 'Recommend App to Others',
              onTap: _onRecommendAppTap,
            ),
            const Divider(),
            TextListTile(
              title: 'About',
              subtitle: 'About Kindly Care',
              onTap: _onAboutTap,
            ),
            const Divider(),
            TextListTile(
              title: 'Rate App',
              subtitle: 'Rate This App',
              onTap: _appReview.openStoreListing,
            ),
            const Divider(),
            TextListTile(
              title: 'Contact Us',
              subtitle: 'Get In Touch With Us',
              onTap: _showGetInTouchBottomSheet,
            ),
            const Divider(),
            TextListTile(
              title: 'Delete Account',
              subtitle: 'Delete Account & All User Data',
              onTap: _showDeleteAccountConfirmDialog,
            ),
          ],
        ),
      ),
    );
  }

  void _showDeleteAccountConfirmDialog() {
    showTwoButtonDialog(
      context,
      isDestructiveAction: true,
      title: 'Delete Account',
      content:
          'Are you sure you want to delete your account together with all your data?',
      buttonText1: 'Exit',
      buttonText2: 'Delete',
      onPressed1: () => Navigator.pop(context),
      onPressed2: () {
        Navigator.of(context).pop();
        _onDeleteAccount();
      },
    );
  }

  void _showDeleteAccountErrorDialog() {
    showTwoButtonDialog(
      context,
      title: 'Error',
      content:
          'Failed to delete account. Please check your network and try again.',
      buttonText1: 'Cancel',
      buttonText2: 'Retry',
      onPressed1: () => Navigator.pop(context),
      onPressed2: () {
        Navigator.of(context).pop();
        _onDeleteAccount();
      },
    );
  }

  void _showGetInTouchBottomSheet() {
    optionsBottomSheet(
      title: 'Get In Touch With Us',
      height: 40.0.h,
      context: context,
      options: <Widget>[
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            onMakeCall(kContactPhone);
          },
          title: 'Phone',
          showTrailing: false,
          subtitle: kContactPhone,
          icon: FeatherIcons.phone,
        ),
        Divider(indent: 18.w),
        IconListTile(
          onTap: () {
            Navigator.pop(context);
            onEmail(kContactEmail);
          },
          title: 'Email',
          showTrailing: false,
          subtitle: kContactEmail,
          icon: FeatherIcons.mail,
        ),
      ],
    );
  }
}
