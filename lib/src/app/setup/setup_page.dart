import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../blocs/care_provider/care_provider_bloc.dart';
import '../../common/common.dart';

class SetUpPage extends StatefulWidget {
  final AppUser user;

  const SetUpPage({super.key, required this.user});

  @override
  State<SetUpPage> createState() => _SetUpPageState();
}

class _SetUpPageState extends State<SetUpPage> {
  File? _avatar;
  DateTime? _dob;
  late GlobalKey<FormState> _formKey;
  late TextEditingController _bioController;
  late TextEditingController _dobController;
  late TextEditingController _nameController;
  late TextEditingController _phoneController;
  late TextEditingController _emailController;
  late TextEditingController _covidController;
  late TextEditingController _genderController;
  late TextEditingController _licenseController;

  late TextEditingController _dutiesController;
  late TextEditingController _servicesController;
  late TextEditingController _occupationController;
  late TextEditingController _yearStartedController;
  late TextEditingController _certificateNameController;

  late TextEditingController _cityController;
  late TextEditingController _suburbController;
  late TextEditingController _streetNameController;
  late TextEditingController _addressNumberController;

  late TextEditingController _sundayController;
  late TextEditingController _mondayController;
  late TextEditingController _tuesdayController;
  late TextEditingController _wednesdayController;
  late TextEditingController _thursdayController;
  late TextEditingController _fridayController;
  late TextEditingController _saturdayController;

  @override
  void initState() {
    super.initState();

    _formKey = GlobalKey<FormState>();
    final AppUser user = widget.user;

    _nameController = TextEditingController(text: user.name);
    _cityController = TextEditingController();
    _suburbController = TextEditingController();
    _streetNameController = TextEditingController();
    _addressNumberController = TextEditingController();
    _phoneController = TextEditingController(text: user.phone);
    _emailController = TextEditingController(text: user.email);
    _genderController = TextEditingController();
    _dutiesController = TextEditingController();
    _servicesController = TextEditingController();
    _occupationController = TextEditingController();

    _certificateNameController = TextEditingController();

    _covidController = TextEditingController();
    _dobController = TextEditingController();
    _yearStartedController = TextEditingController();
    _bioController = TextEditingController();
    _licenseController = TextEditingController();

    _sundayController = TextEditingController();
    _mondayController = TextEditingController();
    _tuesdayController = TextEditingController();
    _wednesdayController = TextEditingController();
    _thursdayController = TextEditingController();
    _fridayController = TextEditingController();
    _saturdayController = TextEditingController();
  }

  @override
  void dispose() {
    _bioController.dispose();
    _dobController.dispose();
    _nameController.dispose();
    _cityController.dispose();
    _suburbController.dispose();
    _streetNameController.dispose();
    _addressNumberController.dispose();
    _phoneController.dispose();
    _emailController.dispose();
    _covidController.dispose();
    _genderController.dispose();
    _servicesController.dispose();
    _licenseController.dispose();
    _yearStartedController.dispose();
    _occupationController.dispose();
    _certificateNameController.dispose();
    _sundayController.dispose();
    _mondayController.dispose();
    _tuesdayController.dispose();
    _dutiesController.dispose();
    _wednesdayController.dispose();
    _thursdayController.dispose();
    _fridayController.dispose();
    _saturdayController.dispose();

    super.dispose();
  }

  final List<String> _availabilityItems = const <String>[
    'Available',
    'Unavailable',
    'Available Day',
    'Available Night',
  ];

  Future<void> _onSaveButtonTap() async {
    final FormState form = _formKey.currentState!;
    final bool isValid = form.validate();

    if (isValid) {
      final CareProvider careProvider = CareProvider(
        dob: _dob,
        uid: widget.user.uid,
        localAvatar: _avatar,
        address: _buildAddress(),
        bio: _bioController.text,
        images: const <String>[],
        contacts: const <String>[],
        avatar: widget.user.avatar,
        name: _nameController.text,
        phone: _phoneController.text,
        email: _emailController.text,
        gender: _genderController.text,
        availability: _getAvailability(),
        joinedDate: widget.user.joinedDate,
        streamToken: widget.user.streamToken,
        messageToken: widget.user.messageToken,
        duties: _dutiesController.text.split(', '),
        services: _servicesController.text.split(', '),
        isCovidVaccinated: _covidController.text == 'Yes',
        occupation: _occupationController.text.split(', '),
        hasDriverLicense: _licenseController.text == 'Yes',
        assignedPatients: const <String, AssignedPatient>{},
        yearStarted: int.tryParse(_yearStartedController.text),
        certificates: _certificateNameController.text.split(', '),
      );

      context
          .read<CareProviderBloc>()
          .add(UpdateCareProvider(careProvider: careProvider));
    }
  }

  Map<String, String> _getAvailability() {
    return <String, String>{
      'Sunday': _sundayController.text,
      'Monday': _mondayController.text,
      'Tuesday': _tuesdayController.text,
      'Wednesday': _wednesdayController.text,
      'Thursday': _thursdayController.text,
      'Friday': _fridayController.text,
      'Saturday': _saturdayController.text,
    };
  }

  Address? _buildAddress() {
    if (_cityController.text.isEmpty &&
        _suburbController.text.isEmpty &&
        _streetNameController.text.isEmpty &&
        _addressNumberController.text.isEmpty) {
      return null;
    } else {
      return Address(
        city: _cityController.text,
        suburb: _suburbController.text,
        streetName: _streetNameController.text,
        number: int.tryParse(_addressNumberController.text) ?? 0,
      );
    }
  }

  void _onSaveError(String message) {
    showTwoButtonDialog(
      context,
      title: 'Error',
      content: message,
      buttonText1: 'Exit',
      buttonText2: 'Retry',
      onPressed1: () => Navigator.pop(context),
      onPressed2: () {
        Navigator.pop(context);
        _onSaveButtonTap();
      },
    );
  }

  void _onAccountCreateSuccess(CareProvider careProvider) {
    Navigator.popAndPushNamed(context, kHomePageRoute, arguments: careProvider);
  }

  @override
  Widget build(BuildContext context) {
    final bool isLoading = context.select((CareProviderBloc bloc) => bloc.state)
        is UpdateCareProviderInProgress;
    return Scaffold(
      extendBodyBehindAppBar: true,
      body: BlocListener<CareProviderBloc, CareProviderState>(
        listener: (BuildContext context, CareProviderState state) {
          if (state is UpdateCareProviderError) {
            _onSaveError(state.error);
          } else if (state is UpdateCareProviderSuccess) {
            if (state.careProvider != null) {
              _onAccountCreateSuccess(state.careProvider!);
            } else {
              errorSnackbar(context, 'Something went wrong. Please try again.');
            }
          }
        },
        child: LoadingOverlay(
          isLoading: isLoading,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 4.0.w),
            child: CustomScrollView(
              physics: const BouncingScrollPhysics(),
              slivers: <Widget>[
                const SliverAppBar(
                  pinned: false,
                  floating: true,
                  elevation: 0.0,
                  systemOverlayStyle: SystemUiOverlayStyle(
                    statusBarIconBrightness: Brightness.dark,
                  ),
                ),
                Form(
                  key: _formKey,
                  child: SliverList(
                    delegate: SliverChildListDelegate(
                      <Widget>[
                        Center(
                          child: SizedBox(
                            height: 16.5.h,
                            width: 27.5.w,
                            child: Stack(
                              children: <Widget>[
                                _avatar == null
                                    ? CircleAvatar(
                                        radius: 12.5.w,
                                        backgroundImage:
                                            CachedNetworkImageProvider(
                                          widget.user.avatar,
                                        ),
                                      )
                                    : LocalBorderAvatar(
                                        imagePath: _avatar!,
                                        radius: 12.5.w,
                                        borderWidth: 0.0,
                                        borderColor: Colors.white,
                                      ),
                                Positioned(
                                  top: 9.1.h,
                                  left: 12.0.w,
                                  child: ElevatedButton.icon(
                                    style: ElevatedButton.styleFrom(
                                      elevation: 0.0,
                                      padding: EdgeInsets.only(left: 2.1.w),
                                      shape: const CircleBorder(),
                                    ),
                                    onPressed: () async {
                                      _avatar = await ImageService.getImage();
                                      setState(() {});
                                    },
                                    label: const Text(''),
                                    icon: Icon(
                                      Icons.photo_camera_outlined,
                                      size: 3.8.h,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 2.0.h),
                        MaterialTextField(
                          labelText: 'Name',
                          hintText: 'Enter name',
                          controller: _nameController,
                          onChanged: (_) => setState(() {}),
                          validator: (String? val) => val.simpleValidate,
                        ),
                        SizedBox(height: 2.5.h),
                        DropdownTextField(
                          readOnly: true,
                          labelText: 'Gender',
                          hintText: 'Select gender',
                          controller: _genderController,
                          onChanged: (_) => setState(() {}),
                          validator: (String? val) => val.simpleValidate,
                          items: const <String>['Female', 'Male'],
                        ),
                        SizedBox(height: 2.5.h),
                        MaterialTextField(
                          readOnly: true,
                          labelText: 'Occupation',
                          hintText: 'Select occupation',
                          controller: _occupationController,
                          focusNode: AlwaysDisabledFocusNode(),
                          onTap: () {
                            showMultiSelectPicker(
                              context,
                              title: 'Occupation',
                              controller: _occupationController,
                              items: const <String>['Carer', 'Nurse Aide'],
                            );
                          },
                          validator: (String? val) => val.simpleValidate,
                        ),
                        SizedBox(height: 2.5.h),
                        MaterialTextField(
                          readOnly: true,
                          focusNode: AlwaysDisabledFocusNode(),
                          onTap: () {
                            showDateTimeSelector(
                              context,
                              initialDate: _dob,
                              title: 'Date Of Birth',
                              controller: _dobController,
                              mode: CupertinoDatePickerMode.date,
                              format: DateFormat.yMMMMd('en_GB'),
                              onDateChanged: (DateTime date) {
                                _dob = date;
                              },
                            );
                          },
                          labelText: 'Date Of Birth',
                          hintText: 'Select date',
                          controller: _dobController,
                          onChanged: (_) => setState(() {}),
                          prefixIcon: const Icon(Icons.event_outlined),
                          validator: (String? val) => val.simpleValidate,
                        ),
                        SizedBox(height: 4.5.h),
                        const TextHeader('Contact Details'),
                        SizedBox(height: 2.5.h),
                        MaterialTextField(
                          labelText: 'Phone',
                          controller: _phoneController,
                          hintText: 'Enter phone number',
                          onChanged: (_) => setState(() {}),
                          textInputType: TextInputType.phone,
                          validator: (String? val) => val.simpleValidateNumber,
                        ),
                        SizedBox(height: 2.5.h),
                        MaterialTextField(
                          labelText: 'Email',
                          controller: _emailController,
                          hintText: 'Enter email address',
                          onChanged: (_) => setState(() {}),
                          textInputType: TextInputType.emailAddress,
                        ),
                        SizedBox(height: 4.5.h),
                        const TextHeader('Physical Address'),
                        SizedBox(height: 2.5.h),
                        MaterialTextField(
                          isOptional: true,
                          labelText: 'Address number',
                          hintText: 'Enter address number',
                          controller: _addressNumberController,
                          onChanged: (_) => setState(() {}),
                          textInputType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                        ),
                        SizedBox(height: 2.5.h),
                        MaterialTextField(
                          isOptional: true,
                          labelText: 'Street Name',
                          hintText: 'Enter street name',
                          controller: _streetNameController,
                          onChanged: (_) => setState(() {}),
                          textInputType: TextInputType.text,
                        ),
                        SizedBox(height: 2.5.h),
                        MaterialTextField(
                          labelText: 'Suburb',
                          hintText: 'Enter suburb',
                          controller: _suburbController,
                          onChanged: (_) => setState(() {}),
                          textInputType: TextInputType.streetAddress,
                          validator: (String? val) => val.simpleValidate,
                        ),
                        SizedBox(height: 2.5.h),
                        MaterialTextField(
                          labelText: 'City',
                          hintText: 'Enter city',
                          controller: _cityController,
                          onChanged: (_) => setState(() {}),
                          textInputType: TextInputType.streetAddress,
                          validator: (String? val) => val.simpleValidate,
                        ),
                        SizedBox(height: 4.5.h),
                        const TextHeader('Availability'),
                        SizedBox(height: 2.5.h),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: DropdownTextField(
                                readOnly: true,
                                labelText: 'Sunday',
                                items: _availabilityItems,
                                controller: _sundayController,
                                onChanged: (_) => setState(() {}),
                                validator: (String? val) => val.simpleValidate,
                              ),
                            ),
                            SizedBox(width: 2.0.w),
                            Expanded(
                              child: DropdownTextField(
                                readOnly: true,
                                labelText: 'Monday',
                                items: _availabilityItems,
                                controller: _mondayController,
                                onChanged: (_) => setState(() {}),
                                validator: (String? val) => val.simpleValidate,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 2.5.h),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: DropdownTextField(
                                readOnly: true,
                                labelText: 'Tuesday',
                                items: _availabilityItems,
                                controller: _tuesdayController,
                                onChanged: (_) => setState(() {}),
                                validator: (String? val) => val.simpleValidate,
                              ),
                            ),
                            SizedBox(width: 2.0.w),
                            Expanded(
                              child: DropdownTextField(
                                readOnly: true,
                                labelText: 'Wednesday',
                                items: _availabilityItems,
                                controller: _wednesdayController,
                                onChanged: (_) => setState(() {}),
                                validator: (String? val) => val.simpleValidate,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 2.5.h),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: DropdownTextField(
                                readOnly: true,
                                labelText: 'Thursday',
                                items: _availabilityItems,
                                controller: _thursdayController,
                                onChanged: (_) => setState(() {}),
                                validator: (String? val) => val.simpleValidate,
                              ),
                            ),
                            SizedBox(width: 2.0.w),
                            Expanded(
                              child: DropdownTextField(
                                readOnly: true,
                                labelText: 'Friday',
                                items: _availabilityItems,
                                controller: _fridayController,
                                onChanged: (_) => setState(() {}),
                                validator: (String? val) => val.simpleValidate,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 2.5.h),
                        DropdownTextField(
                          readOnly: true,
                          labelText: 'Saturday',
                          items: _availabilityItems,
                          controller: _saturdayController,
                          onChanged: (_) => setState(() {}),
                          validator: (String? val) => val.simpleValidate,
                        ),
                        SizedBox(height: 4.5.h),
                        const TextHeader('Other'),
                        SizedBox(height: 2.5.h),
                        MaterialTextField(
                          hintText: 'eg. 2012',
                          labelText: 'Year Started',
                          controller: _yearStartedController,
                          onChanged: (_) => setState(() {}),
                          textInputType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          validator: (String? val) => val.simpleValidateYear,
                        ),
                        SizedBox(height: 2.5.h),
                        DropdownTextField(
                          readOnly: true,
                          labelText: 'Driver License',
                          controller: _licenseController,
                          onChanged: (_) => setState(() {}),
                          validator: (String? val) => val.simpleValidate,
                          items: const <String>['Yes', 'No'],
                        ),
                        SizedBox(height: 2.5.h),
                        DropdownTextField(
                          readOnly: true,
                          controller: _covidController,
                          labelText: 'Covid Vaccinated',
                          onChanged: (_) => setState(() {}),
                          items: const <String>['Yes', 'No'],
                          validator: (String? val) => val.simpleValidate,
                        ),
                        SizedBox(height: 2.5.h),
                        MaterialTextField(
                          readOnly: true,
                          labelText: 'Services',
                          controller: _servicesController,
                          focusNode: AlwaysDisabledFocusNode(),
                          onTap: () {
                            showMultiSelectPicker(
                              context,
                              items: kServices,
                              title: 'Services',
                              controller: _servicesController,
                            );
                          },
                          validator: (String? val) => val.simpleValidate,
                        ),
                        SizedBox(height: 2.5.h),
                        MaterialTextField(
                          readOnly: true,
                          labelText: 'Duties',
                          controller: _dutiesController,
                          focusNode: AlwaysDisabledFocusNode(),
                          onTap: () {
                            showMultiSelectPicker(
                              context,
                              title: 'Duties',
                              controller: _dutiesController,
                              items: kDuties,
                            );
                          },
                          validator: (String? val) => val.simpleValidate,
                        ),
                        SizedBox(height: 2.5.h),
                        MaterialTextField(
                          readOnly: true,
                          labelText: 'Qualifications',
                          focusNode: AlwaysDisabledFocusNode(),
                          controller: _certificateNameController,
                          onTap: () {
                            showMultiSelectPicker(
                              context,
                              title: 'Qualifications',
                              items: kQualifications,
                              controller: _certificateNameController,
                            );
                          },
                          validator: (String? val) => val.simpleValidate,
                        ),
                        SizedBox(height: 2.5.h),
                        MaterialTextField(
                          minLines: 3,
                          maxLines: null,
                          isOptional: true,
                          labelText: 'Bio',
                          controller: _bioController,
                          onChanged: (_) => setState(() {}),
                          textInputAction: TextInputAction.newline,
                          hintText: 'Enter any additional info about yourself.',
                        ),
                        SizedBox(height: 6.5.h),
                        ActionButton(
                          title: 'Save',
                          onPressed: _onSaveButtonTap,
                        ),
                        SizedBox(height: 2.0.h),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
