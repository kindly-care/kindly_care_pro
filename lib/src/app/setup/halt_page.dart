import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../common/constants/constants.dart';

class HaltPage extends StatelessWidget {
  final AppUser user;

  const HaltPage({super.key, required this.user});

  void _onCreateProAccount(BuildContext context) {
    Navigator.popAndPushNamed(context, kSetUpPagePageRoute, arguments: user);
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Scaffold(
      appBar: AppBar(elevation: 0.0),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(3.0.w),
        physics: const BouncingScrollPhysics(),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(height: 12.0.h),
            Flexible(
              child: SvgPicture.asset(
                'assets/images/nurse.svg',
                height: 18.0.h,
                alignment: Alignment.center,
              ),
            ),
            SizedBox(height: 2.5.h),
            Flexible(
              child: Text(
                'This application is meant for professionals only. Please consider creating a Pro Account or use the Family app instead.',
                style: theme.bodyLarge,
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 28.0.h),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: OutlinedButton(
                    onPressed: () => SystemNavigator.pop(),
                    child: Text(
                      'Exit App',
                      style: theme.bodyLarge
                          ?.copyWith(color: Theme.of(context).primaryColor),
                    ),
                  ),
                ),
                SizedBox(width: 2.0.w),
                Expanded(
                  child: OutlinedButton(
                    onPressed: () => _onCreateProAccount(context),
                    child: Text(
                      'Create Pro Account',
                      style: theme.bodyLarge
                          ?.copyWith(color: Theme.of(context).primaryColor),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
