import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';

import '../blocs/care_provider/care_provider_bloc.dart';
import '../blocs/care_task/care_task_bloc.dart';
import '../blocs/clock_in/clock_in_bloc.dart';
import '../blocs/notifications/notification_bloc.dart';
import '../blocs/patient/patient_bloc.dart';
import '../blocs/report/report_bloc.dart';
import '../cubits/account_cubit/account_cubit.dart';
import '../cubits/chat_cubit/chat_cubit.dart';
import '../data/local/local.dart';
import '../data/services/services.dart';

class Providers extends StatelessWidget {
  final Widget child;

  const Providers({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: <RepositoryProvider<dynamic>>[
        RepositoryProvider<AuthService>(
          create: (BuildContext context) => AuthService(),
        ),
        RepositoryProvider<CareProviderService>(
          create: (BuildContext context) => CareProviderService(),
        ),
        RepositoryProvider<PatientService>(
          create: (BuildContext context) => PatientService(),
        ),
        RepositoryProvider<LocationService>(
          create: (BuildContext context) => LocationService(),
        ),
        RepositoryProvider<ReportService>(
          create: (BuildContext context) => ReportService(),
        ),
        RepositoryProvider<NotificationService>(
          create: (BuildContext context) => NotificationService(),
        ),
        RepositoryProvider<ContactsService>(
          create: (BuildContext context) => ContactsService(),
        ),
        RepositoryProvider<TimeLogService>(
          create: (BuildContext context) => TimeLogService(),
        ),
        RepositoryProvider<CareTaskService>(
          create: (BuildContext context) => CareTaskService(),
        ),
        RepositoryProvider<VitalsService>(
          create: (BuildContext context) => VitalsService(),
        ),
      ],
      child: MultiBlocProvider(
        providers: <BlocProvider<dynamic>>[
          BlocProvider<AuthBloc>(
            create: (BuildContext context) =>
                AuthBloc(authRepository: context.read<AuthService>())
                  ..add(StartApp()),
          ),
          BlocProvider<NotificationBloc>(
            create: (BuildContext context) => NotificationBloc(
              notificationRepository: context.read<NotificationService>(),
            ),
          ),
          BlocProvider<ChatCubit>(
            create: (BuildContext context) => ChatCubit(
              notificationRepository: context.read<NotificationService>(),
            ),
          ),
          BlocProvider<CareTaskBloc>(
            create: (BuildContext context) => CareTaskBloc(
              careTaskRepository: context.read<CareTaskService>(),
            ),
          ),
          BlocProvider<CareProviderBloc>(
            create: (BuildContext context) => CareProviderBloc(
              careProviderRepository: context.read<CareProviderService>(),
            ),
          ),
          BlocProvider<PatientBloc>(
            create: (BuildContext context) => PatientBloc(
              patientRepository: context.read<PatientService>(),
            ),
          ),
          BlocProvider<AccountCubit>(
            create: (BuildContext context) => AccountCubit(
              authRepository: context.read<AuthService>(),
              careProviderRepository: context.read<CareProviderService>(),
              timeLogRepository: context.read<TimeLogService>(),
            )..onFetchAccountData(),
          ),
          BlocProvider<ClockInBloc>(
            create: (BuildContext context) => ClockInBloc(
              vitalsRepository: context.read<VitalsService>(),
              reportRepository: context.read<ReportService>(),
              patientRepository: context.read<PatientService>(),
              timeLogRepository: context.read<TimeLogService>(),
              locationRepository: context.read<LocationService>(),
              careTaskRepository: context.read<CareTaskService>(),
              notificationRepository: context.read<NotificationService>(),
            ),
          ),
          BlocProvider<ReportBloc>(
            create: (BuildContext context) => ReportBloc(
              reportRepository: context.read<ReportService>(),
            ),
          ),
        ],
        child: child,
      ),
    );
  }
}
