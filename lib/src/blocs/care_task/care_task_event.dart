part of 'care_task_bloc.dart';

abstract class CareTaskEvent extends Equatable {
  const CareTaskEvent();

  @override
  List<Object> get props => <Object>[];
}

class FetchCareTasks extends CareTaskEvent {
  final String patientId;
  const FetchCareTasks({required this.patientId});

  @override
  List<Object> get props => <Object>[patientId];
}

class FetchCareTasksByDate extends CareTaskEvent {
  final DateTime date;
  final String patientId;
  const FetchCareTasksByDate({required this.date, required this.patientId});

  @override
  List<Object> get props => <Object>[date, patientId];
}

class SaveCareActionToCache extends CareTaskEvent {
  final CareAction action;
  const SaveCareActionToCache({required this.action});

  @override
  List<Object> get props => <Object>[action];
}
