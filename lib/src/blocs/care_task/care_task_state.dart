part of 'care_task_bloc.dart';

abstract class CareTaskState extends Equatable {
  const CareTaskState();

  @override
  List<Object> get props => <Object>[];
}

class CareTaskInitial extends CareTaskState {}

class CareTasksLoading extends CareTaskState {}

class CareTasksLoadSuccess extends CareTaskState {
  final List<CareTask> tasks;
  final List<CareAction> actions;
  const CareTasksLoadSuccess({required this.tasks, required this.actions});

  @override
  List<Object> get props => <Object>[tasks, actions];
}

class CareTasksLoadError extends CareTaskState {
  final String error;
  const CareTasksLoadError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

class SaveCareTaskToCacheInProgress extends CareTaskState {}

class SaveCareTaskToCacheSuccess extends CareTaskState {
  final String message;
  const SaveCareTaskToCacheSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class SaveCareTaskToCacheError extends CareTaskState {
  final String error;
  const SaveCareTaskToCacheError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}
