// ignore_for_file: depend_on_referenced_packages

import 'package:bloc/bloc.dart';
import 'package:collection/collection.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:rxdart/rxdart.dart';

import '../../data/services/care_task/care_task_repository.dart';

part 'care_task_event.dart';
part 'care_task_state.dart';

class CareTaskBloc extends Bloc<CareTaskEvent, CareTaskState> {
  final CareTaskRepository _careTaskRepository;
  CareTaskBloc({required CareTaskRepository careTaskRepository})
      : _careTaskRepository = careTaskRepository,
        super(CareTaskInitial()) {
    on<FetchCareTasks>(_onFetchCareTasks);
    on<FetchCareTasksByDate>(_onFetchCareTasksByDate);
    on<SaveCareActionToCache>(_onSaveCareActionToCache);
  }

  Future<void> _onFetchCareTasks(
      FetchCareTasks event, Emitter<CareTaskState> emit) async {
    emit(CareTasksLoading());

    await emit.forEach<List<CareTask>>(
        _careTaskRepository.fetchCareTasks(event.patientId),
        onData: (List<CareTask> tasks) =>
            CareTasksLoadSuccess(tasks: tasks, actions: const <CareAction>[]),
        onError: (_, __) =>
            const CareTasksLoadError(error: 'Failed to load data'));
  }

  Future<void> _onFetchCareTasksByDate(
      FetchCareTasksByDate event, Emitter<CareTaskState> emit) async {
    emit(CareTasksLoading());
    final DateTime date = event.date;
    final String patientId = event.patientId;

    final Stream<List<CareTask>> taskStream = _careTaskRepository
        .fetchCareTasksByDate(patientId: patientId, date: date);
    final Stream<List<CareAction>> actionStream = _careTaskRepository
        .fetchCareActionsByDate(patientId: patientId, date: date);

    final CombineLatestStream<dynamic, List<dynamic>> dataStream =
        CombineLatestStream.list<dynamic>(
            <Stream<List<dynamic>>>[taskStream, actionStream]);

    await emit.forEach<List<dynamic>>(dataStream,
        onData: (List<dynamic> data) {
          final List<CareTask> tasks = data[0] as List<CareTask>;
          final List<CareAction> actions = data[1] as List<CareAction>;
          tasks.sortBy((CareTask task) => task.time);
          tasks.sort(
              (CareTask a, CareTask b) => a.time.hour.compareTo(b.time.hour));

          return CareTasksLoadSuccess(tasks: tasks, actions: actions);
        },
        onError: (_, __) =>
            const CareTasksLoadError(error: 'Failed to load data'));
  }

  Future<void> _onSaveCareActionToCache(
      SaveCareActionToCache event, Emitter<CareTaskState> emit) async {
    emit(SaveCareTaskToCacheInProgress());
    final CareAction action = event.action;

    try {
      await _careTaskRepository.saveCareActionToCache(action);
      if (action is MealAction && action.meal.isNotEmpty) {
        _updatePatientMeal(action);
      }
      emit(const SaveCareTaskToCacheSuccess(message: 'Operation success'));
    } on UpdateDataException catch (e) {
      emit(SaveCareTaskToCacheError(error: e.toString()));
    }
  }

  Future<void> _updatePatientMeal(MealAction action) async {
    final Meal meal = Meal(
      meal: action.meal,
      time: DateTime.now(),
      type: action.mealType,
      patientId: action.patientId,
      careProviderId: action.careProviderId,
    );

    await _careTaskRepository.updatePatientMeal(meal);
  }
}
