part of 'time_logs_bloc.dart';

abstract class TimeLogsState extends Equatable {
  const TimeLogsState();

  @override
  List<Object> get props => <Object>[];
}

class TimeLogsInitial extends TimeLogsState {}

class TimeLogsLoading extends TimeLogsState {}

class TimeLogsLoadSuccess extends TimeLogsState {
  final List<TimeLog> timelogs;
  const TimeLogsLoadSuccess({required this.timelogs});

  @override
  List<Object> get props => <Object>[timelogs];
}

class TimeLogsLoadError extends TimeLogsState {
  final String error;
  const TimeLogsLoadError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}
