import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../data/services/time_log/time_log_repository.dart';

part 'time_logs_event.dart';
part 'time_logs_state.dart';

class TimeLogsBloc extends Bloc<TimeLogsEvent, TimeLogsState> {
  final TimeLogRepository _timeLogRepository;
  TimeLogsBloc({required TimeLogRepository timeLogRepository})
      : _timeLogRepository = timeLogRepository,
        super(TimeLogsInitial()) {
    on<FetchTimeLogs>(_onFetchTimeLogs);
  }

  Future<void> _onFetchTimeLogs(
      FetchTimeLogs event, Emitter<TimeLogsState> emit) async {
    emit(TimeLogsLoading());
    try {
      final List<TimeLog> timelogs =
          await _timeLogRepository.fetchCareProviderTimeLogsByMonth(
        month: event.month,
        patientId: event.patientId,
        careProviderId: event.careProviderId,
      );

      timelogs
          .sort((TimeLog a, TimeLog b) => b.startTime!.compareTo(a.startTime!));

      emit(TimeLogsLoadSuccess(timelogs: timelogs));
    } on FetchDataException catch (e) {
      emit(TimeLogsLoadError(error: e.toString()));
    }
  }
}
