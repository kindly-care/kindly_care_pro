part of 'time_logs_bloc.dart';

abstract class TimeLogsEvent extends Equatable {
  const TimeLogsEvent();

  @override
  List<Object> get props => <Object>[];
}

class FetchTimeLogs extends TimeLogsEvent {
  final int month;
  final String patientId, careProviderId;
  const FetchTimeLogs({
    required this.month,
    required this.patientId,
    required this.careProviderId,
  });

  @override
  List<Object> get props => <Object>[month, careProviderId, patientId];
}
