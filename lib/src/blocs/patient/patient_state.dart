part of 'patient_bloc.dart';

abstract class PatientState extends Equatable {
  const PatientState();

  @override
  List<Object?> get props => <Object?>[];
}

class PatientInitial extends PatientState {}

class PatientLoading extends PatientState {}

class PatientLoadSuccess extends PatientState {
  final Patient? patient;
  const PatientLoadSuccess({required this.patient});

  @override
  List<Object?> get props => <Object?>[patient];
}

class PatientLoadError extends PatientState {
  final String error;
  const PatientLoadError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

class CircleMembersLoading extends PatientState {}

class CircleMembersLoadSuccess extends PatientState {
  final List<AppUser> members;
  const CircleMembersLoadSuccess({required this.members});

  @override
  List<Object> get props => <Object>[members];
}

class CircleMembersLoadError extends PatientState {
  final String error;
  const CircleMembersLoadError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

class AssignedCareProvidersLoading extends PatientState {}

class AssignedCareProvidersLoadSuccess extends PatientState {
  final List<CareProvider> careProviders;
  const AssignedCareProvidersLoadSuccess({required this.careProviders});

  @override
  List<Object> get props => <Object>[careProviders];
}

class AssignedCareProvidersLoadError extends PatientState {
  final String error;
  const AssignedCareProvidersLoadError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}
