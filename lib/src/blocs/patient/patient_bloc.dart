import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../data/services/services.dart';

part 'patient_event.dart';
part 'patient_state.dart';

class PatientBloc extends Bloc<PatientEvent, PatientState> {
  final PatientRepository _patientRepository;
  PatientBloc({required PatientRepository patientRepository})
      : _patientRepository = patientRepository,
        super(PatientInitial()) {
    on<FetchPatient>(_onFetchPatient);
    on<FetchPatientCircle>(_onFetchPatientCircle);
    on<FetchAssignedCareProviders>(_onFetchAssignedCareProviders);
  }

  Future<void> _onFetchPatient(
      FetchPatient event, Emitter<PatientState> emit) async {
    emit(PatientLoading());

    try {
      final Patient? patient =
          await _patientRepository.fetchPatient(event.patientId);
      emit(PatientLoadSuccess(patient: patient));
    } on FetchDataException catch (e) {
      emit(PatientLoadError(error: e.toString()));
    }
  }

  Future<void> _onFetchPatientCircle(
      FetchPatientCircle event, Emitter<PatientState> emit) async {
    emit(CircleMembersLoading());
    try {
      final List<AppUser> members =
          await _patientRepository.fetchPatientCircle(event.memberIds);
      emit(CircleMembersLoadSuccess(members: members));
    } on FetchDataException catch (e) {
      emit(CircleMembersLoadError(error: e.toString()));
    }
  }

  Future<void> _onFetchAssignedCareProviders(
      FetchAssignedCareProviders event, Emitter<PatientState> emit) async {
    emit(AssignedCareProvidersLoading());

    try {
      final List<CareProvider> careProviders = await _patientRepository
          .fetchAssignedCareProviders(event.careProviderIds);
      emit(AssignedCareProvidersLoadSuccess(careProviders: careProviders));
    } on FetchDataException catch (e) {
      emit(AssignedCareProvidersLoadError(error: e.toString()));
    }
  }
}
