part of 'patient_bloc.dart';

abstract class PatientEvent extends Equatable {
  const PatientEvent();

  @override
  List<Object> get props => <Object>[];
}

class FetchPatient extends PatientEvent {
  final String patientId;
  const FetchPatient({required this.patientId});

  @override
  List<Object> get props => <Object>[patientId];
}

class FetchPatientCircle extends PatientEvent {
  final List<String> memberIds;
  const FetchPatientCircle({required this.memberIds});

  @override
  List<Object> get props => <Object>[memberIds];
}

class FetchAssignedCareProviders extends PatientEvent {
  final List<String> careProviderIds;
  const FetchAssignedCareProviders({required this.careProviderIds});

  @override
  List<Object> get props => <Object>[careProviderIds];
}
