part of 'notification_bloc.dart';

abstract class NotificationState extends Equatable {
  const NotificationState();

  @override
  List<Object> get props => <Object>[];
}

class NotificationsInitial extends NotificationState {}

class NotificationsLoading extends NotificationState {}

class NotificationsLoadSuccess extends NotificationState {
  final List<PushNotification> notifications;
  const NotificationsLoadSuccess({required this.notifications});

  @override
  List<Object> get props => <Object>[notifications];
}

class NotificationsLoadError extends NotificationState {
  final String error;
  const NotificationsLoadError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

class DeleteNotificationInProgress extends NotificationState {}

class DeleteNotificationSuccess extends NotificationState {
  final String message;
  const DeleteNotificationSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class DeleteNotificationError extends NotificationState {
  final String error;
  const DeleteNotificationError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

class SendNotificationInProgress extends NotificationState {}

class SendNotificationSuccess extends NotificationState {}

class SendNotificationError extends NotificationState {
  final String error;
  const SendNotificationError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}
