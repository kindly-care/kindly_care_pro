part of 'notification_bloc.dart';

abstract class NotificationEvent extends Equatable {
  const NotificationEvent();

  @override
  List<Object> get props => <Object>[];
}

class FetchNotifications extends NotificationEvent {
  final String uid;
  const FetchNotifications({required this.uid});

  @override
  List<Object> get props => <Object>[uid];
}

class DeleteNotification extends NotificationEvent {
  final String uid;
  final String notificationId;
  const DeleteNotification({required this.uid, required this.notificationId});

  @override
  List<Object> get props => <Object>[uid, notificationId];
}

class DeleteAllNotifications extends NotificationEvent {
  final String uid;
  const DeleteAllNotifications({required this.uid});

  @override
  List<Object> get props => <Object>[uid];
}

class MarkNotificationAsSeen extends NotificationEvent {
  final String uid;
  final String notificationId;
  const MarkNotificationAsSeen(
      {required this.uid, required this.notificationId});

  @override
  List<Object> get props => <Object>[uid, notificationId];
}

class SendPushNotification extends NotificationEvent {
  final PushNotification notification;
  const SendPushNotification({required this.notification});

  @override
  List<Object> get props => <Object>[notification];
}

class ChangeNotificationStatus extends NotificationEvent {
  final String uid;
  final String notificationId;
  const ChangeNotificationStatus(
      {required this.uid, required this.notificationId});

  @override
  List<Object> get props => <Object>[uid, notificationId];
}
