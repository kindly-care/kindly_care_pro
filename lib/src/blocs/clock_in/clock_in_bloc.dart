import 'package:bloc/bloc.dart';
import 'package:collection/collection.dart';
import 'package:equatable/equatable.dart';
import 'package:fcm_config/fcm_config.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:uuid/uuid.dart';

import '../../data/local/location/location_repository.dart';
import '../../data/services/services.dart';

part 'clock_in_event.dart';
part 'clock_in_state.dart';

class ClockInBloc extends Bloc<ClockInEvent, ClockInState> {
  final ReportRepository _reportRepository;
  final VitalsRepository _vitalsRepository;
  final TimeLogRepository _timeLogRepository;
  final PatientRepository _patientRepository;
  final CareTaskRepository _careTaskRepository;
  final LocationRepository _locationRepository;
  final NotificationRepository _notificationRepository;
  ClockInBloc({
    required ReportRepository reportRepository,
    required TimeLogRepository timeLogRepository,
    required VitalsRepository vitalsRepository,
    required PatientRepository patientRepository,
    required CareTaskRepository careTaskRepository,
    required LocationRepository locationRepository,
    required NotificationRepository notificationRepository,
  })  : _reportRepository = reportRepository,
        _vitalsRepository = vitalsRepository,
        _timeLogRepository = timeLogRepository,
        _patientRepository = patientRepository,
        _careTaskRepository = careTaskRepository,
        _locationRepository = locationRepository,
        _notificationRepository = notificationRepository,
        super(ClockInInitial()) {
    on<ClockIn>(_onClockIn);
    on<ClockOut>(_onClockOut);
    on<SyncData>(_onSyncData);
  }

  Future<void> _onClockIn(ClockIn event, Emitter<ClockInState> emit) async {
    emit(ClockInInProgress());

    final Patient patient = event.patient;
    final CareProvider agent = event.careProvider;
    final String patientName = patient.name.firstName();
    final String today = DateFormat('yyyy-MM-dd').format(DateTime.now());

    try {
      final LatLng location = await _locationRepository.getCurrentPosition();
      final double distance = _getDistanceInMeters(patient, location);
      if (distance > 1000) {
        emit(ClockInError(
          patient: patient,
          error:
              "It seems you are too far from $patientName's home. You need to be at the patient's home to Clock In.",
          errorType: ClockInErrorType.distance,
        ));
      } else {
        final TimeLog timelog = TimeLog(
          dateCreated: today,
          careProviderId: agent.uid,
          careProviderName: agent.name,
          patientId: patient.uid,
          careProviderPhone: agent.phone,
          patientName: patient.name,
          startTime: DateTime.now(),
          timeLogId: const Uuid().v4(),
        );

        await _timeLogRepository.saveTimeLogToCache(timelog);
        _timeLogRepository.updateClockedInStatus(agent);

        emit(ClockInSuccess(
            patientId: patient.uid,
            message: 'You have successfully Clocked In.'));
      }
    } on UpdateDataException {
      emit(ClockInError(
        patient: patient,
        errorType: ClockInErrorType.network,
        error: 'Failed to Clock In. Please check your network connection.',
      ));
    } on LocationException catch (e) {
      emit(ClockInError(
        patient: patient,
        error: e.toString(),
        errorType: ClockInErrorType.location,
      ));
    }
  }

  Future<void> _onClockOut(ClockOut event, Emitter<ClockInState> emit) async {
    emit(ClockOutInProgress());
    final TimeLog timelog = event.timelog.copyWith(finishTime: DateTime.now());
    final Report reportWithVitals = await _addVitalsToReport(event.report);
    final Report reportWithCareActions =
        await _addCareActionsToReport(reportWithVitals);
    final Report finalReport = reportWithCareActions.copyWith(timelog: timelog);

    final bool hasInternet = await InternetConnectionChecker().hasConnection;
    if (!hasInternet) {
      await _timeLogRepository.saveTimeLogToCache(timelog);
      await _reportRepository.saveReportToCache(finalReport);

      emit(
          const ClockOutSuccess(message: 'You have successfully Clocked Out.'));
    } else {
      try {
        await _timeLogRepository.submitTimeLog(timelog);
        await _reportRepository.submitReport(finalReport);
        await _careTaskRepository.submitCareActions(finalReport.careActions);
        await _sendShiftReportNotification(finalReport);
        await _timeLogRepository.updateClockedInStatus(event.careProvider);

        emit(const ClockOutSuccess(
            message: 'You have successfully Clocked Out.'));
      } on Exception {
        emit(const ClockOutError(
          error:
              'Something went wrong. Please check your network connection and try again.',
        ));
      }
    }
  }

  Future<void> _onSyncData(SyncData event, Emitter<ClockInState> emit) async {
    final bool hasInternet = await InternetConnectionChecker().hasConnection;
    final List<TimeLog> cachedTimelogs =
        await _timeLogRepository.fetchTimeLogsFromCache();
    final List<TimeLog> timelogs = cachedTimelogs
        .where((TimeLog timelog) =>
            timelog.startTime != null && timelog.finishTime != null)
        .toList();

    if (hasInternet && timelogs.isNotEmpty) {
      _showProgressIndicator();

      final List<String> keys =
          timelogs.map((TimeLog timelog) => timelog.timeLogId).toList();
      final List<Report> reports =
          await _reportRepository.fetchReportsFromCacheByKeys(keys);
      final List<CareAction> actions =
          reports.map((Report report) => report.careActions).flattened.toList();

      await _reportRepository.submitReports(reports);
      await _careTaskRepository.submitCareActions(actions);
      await _timeLogRepository.submitTimeLogs(timelogs);

      for (final Report report in reports) {
        await _sendShiftReportNotification(report);
      }
    }
  }

  double _getDistanceInMeters(Patient patient, LatLng location) {
    return Geolocator.distanceBetween(
      location.latitude,
      location.longitude,
      patient.address!.latitude!,
      patient.address!.longitude!,
    );
  }

  Future<Report> _addVitalsToReport(Report report) async {
    final WeightRecord? weight = await _vitalsRepository.fetchWeightFromCache();
    final TemperatureRecord? temperature =
        await _vitalsRepository.fetchTemperatureFromCache();
    final BloodGlucoseRecord? bloodGlucose =
        await _vitalsRepository.fetchBloodGlucoseFromCache();
    final BloodPressureRecord? bloodPressure =
        await _vitalsRepository.fetchBloodPressureFromCache();

    await _vitalsRepository.clearVitalsFromCache();

    return report.copyWith(
      weight: weight,
      temperature: temperature,
      bloodGlucose: bloodGlucose,
      bloodPressure: bloodPressure,
    );
  }

  Future<Report> _addCareActionsToReport(Report report) async {
    final List<CareAction> actions =
        await _careTaskRepository.fetchCareActionsFromCache();

    await _careTaskRepository.clearCareActionsFromCache();

    return report.copyWith(careActions: actions);
  }

  Future<void> _sendShiftReportNotification(Report report) async {
    final Patient? patient =
        await _patientRepository.fetchPatient(report.patientId);

    if (patient != null) {
      final ReportNotification notification =
          _buildReportPushNotification(report, patient.circle.keys.toList());

      await _notificationRepository.sendPushNotification(notification);
    }
  }

  ReportNotification _buildReportPushNotification(
      Report report, List<String> circle) {
    final String notificationId = const Uuid().v4();
    return ReportNotification(
      id: notificationId,
      title: 'Care Report',
      text:
          'New Care Report for ${report.patientName} by ${report.careProviderName}.',
      recipients: circle,
      createdAt: DateTime.now(),
      reportId: report.reportId,
      patientId: report.patientId,
      type: NotificationType.report,
      authorId: report.careProviderId,
      authorName: report.careProviderName,
      authorPhone: report.careProviderPhone,
    );
  }

  void _showProgressIndicator() {
    FCMConfig.instance.local.displayNotification(
      id: 5,
      title: 'Syncing Data',
      android: const AndroidNotificationDetails(
        'Progress channel',
        'Progress channel',
        playSound: false,
        autoCancel: false,
        timeoutAfter: 7000,
        showProgress: true,
        onlyAlertOnce: true,
        indeterminate: true,
        enableVibration: false,
        priority: Priority.low,
        channelShowBadge: false,
        importance: Importance.max,
        icon: '@mipmap/ic_launcher_foreground',
        channelDescription: 'progress channel description',
      ),
    );
  }
}
