part of 'clock_in_bloc.dart';

abstract class ClockInEvent extends Equatable {
  const ClockInEvent();

  @override
  List<Object> get props => <Object>[];
}

class ClockIn extends ClockInEvent {
  final Patient patient;
  final CareProvider careProvider;
  const ClockIn({required this.careProvider, required this.patient});

  @override
  List<Object> get props => <Object>[careProvider, patient];
}

class ClockOut extends ClockInEvent {
  final Report report;
  final TimeLog timelog;
  final CareProvider careProvider;
  const ClockOut({
    required this.report,
    required this.timelog,
    required this.careProvider,
  });

  @override
  List<Object> get props => <Object>[careProvider, report, timelog];
}

class SyncData extends ClockInEvent {}
