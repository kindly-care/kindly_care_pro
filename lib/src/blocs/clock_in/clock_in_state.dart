part of 'clock_in_bloc.dart';

enum ClockInErrorType { distance, location, network }

abstract class ClockInState extends Equatable {
  const ClockInState();

  @override
  List<Object> get props => <Object>[];
}

class ClockInInitial extends ClockInState {}

class ClockInInProgress extends ClockInState {}

class ClockInSuccess extends ClockInState {
  final String message;
  final String patientId;
  const ClockInSuccess({required this.message, required this.patientId});

  @override
  List<Object> get props => <Object>[message, patientId];
}

class ClockInError extends ClockInState {
  final String error;
  final Patient patient;
  final ClockInErrorType errorType;
  const ClockInError(
      {required this.error, required this.patient, required this.errorType});

  @override
  List<Object> get props => <Object>[error, errorType, patient];
}

class ClockOutInProgress extends ClockInState {}

class ClockOutSuccess extends ClockInState {
  final String message;
  const ClockOutSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class ClockOutError extends ClockInState {
  final String error;
  const ClockOutError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}
