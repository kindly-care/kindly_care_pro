part of 'report_bloc.dart';

abstract class ReportEvent extends Equatable {
  const ReportEvent();

  @override
  List<Object> get props => <Object>[];
}

class SaveReportToCache extends ReportEvent {
  final bool silent;
  final Report report;
  const SaveReportToCache({required this.report, this.silent = false});

  @override
  List<Object> get props => <Object>[silent, report];
}
