import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../data/services/report/report_repository.dart';

part 'report_event.dart';
part 'report_state.dart';

class ReportBloc extends Bloc<ReportEvent, ReportState> {
  final ReportRepository _reportRepository;
  ReportBloc({required ReportRepository reportRepository})
      : _reportRepository = reportRepository,
        super(ReportInitial()) {
    on<SaveReportToCache>(_onSaveReportToCache);
  }

  Future<void> _onSaveReportToCache(
      SaveReportToCache event, Emitter<ReportState> emit) async {
    if (event.silent == false) {
      emit(SaveReportToCacheInProgress());
    }

    try {
      await _reportRepository.saveReportToCache(event.report);
      if (event.silent == false) {
        emit(const SaveReportToCacheSuccess(message: 'Care Report saved.'));
      }
    } on UpdateDataException catch (e) {
      emit(
        SaveReportToCacheError(error: e.toString()),
      );
    }
  }
}
