part of 'report_bloc.dart';

abstract class ReportState extends Equatable {
  const ReportState();

  @override
  List<Object> get props => <Object>[];
}

class ReportInitial extends ReportState {}

class SaveReportToCacheInProgress extends ReportState {}

class SaveReportToCacheSuccess extends ReportState {
  final String message;
  const SaveReportToCacheSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class SaveReportToCacheError extends ReportState {
  final String error;
  const SaveReportToCacheError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}
