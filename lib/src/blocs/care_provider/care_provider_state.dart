part of 'care_provider_bloc.dart';

abstract class CareProviderState extends Equatable {
  const CareProviderState();

  @override
  List<Object?> get props => <Object?>[];
}

class CareProviderInitial extends CareProviderState {}

class CareProviderLoading extends CareProviderState {}

class CareProviderLoadSuccess extends CareProviderState {
  final CareProvider? careProvider;
  const CareProviderLoadSuccess({required this.careProvider});

  @override
  List<Object?> get props => <Object?>[careProvider];
}

class CareProviderLoadError extends CareProviderState {
  final String error;
  const CareProviderLoadError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

class UpdateCareProviderInProgress extends CareProviderState {}

class UpdateCareProviderSuccess extends CareProviderState {
  final String message;
  final CareProvider? careProvider;
  const UpdateCareProviderSuccess(
      {required this.careProvider, required this.message});

  @override
  List<Object?> get props => <Object?>[careProvider, message];
}

class UpdateCareProviderError extends CareProviderState {
  final String error;
  const UpdateCareProviderError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

class AssignedPatientsLoading extends CareProviderState {}

class AssignedPatientsLoadSuccess extends CareProviderState {
  final List<Patient> patients;
  const AssignedPatientsLoadSuccess({required this.patients});

  @override
  List<Object> get props => <Object>[patients];
}

class AssignedPatientsLoadError extends CareProviderState {
  final String error;
  const AssignedPatientsLoadError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}

class RemovePatientInProgress extends CareProviderState {}

class RemovePatientSuccess extends CareProviderState {
  final String message;
  const RemovePatientSuccess({required this.message});

  @override
  List<Object> get props => <Object>[message];
}

class RemovePatientError extends CareProviderState {
  final String error;
  const RemovePatientError({required this.error});

  @override
  List<Object> get props => <Object>[error];
}
