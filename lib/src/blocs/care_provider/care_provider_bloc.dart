import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../data/services/care_provider/care_provider_repository.dart';

part 'care_provider_event.dart';
part 'care_provider_state.dart';

class CareProviderBloc extends Bloc<CareProviderEvent, CareProviderState> {
  final CareProviderRepository _careProviderRepository;
  CareProviderBloc({required CareProviderRepository careProviderRepository})
      : _careProviderRepository = careProviderRepository,
        super(CareProviderInitial()) {
    on<FetchCareProvider>(_onFetchCareProvider);
    on<UpdateCareProvider>(_onUpdateCareProvider);
    on<FetchAssignedPatients>(_onFetchAssignedPatients);
    on<RemovePatient>(_onRemovePatient);
  }

  Future<void> _onFetchCareProvider(
      FetchCareProvider event, Emitter<CareProviderState> emit) async {
    emit(CareProviderLoading());

    await emit.forEach<CareProvider?>(
      _careProviderRepository.fetchCareProviderById(event.careProviderId),
      onData: (CareProvider? careProvider) =>
          CareProviderLoadSuccess(careProvider: careProvider),
      onError: (_, __) =>
          const CareProviderLoadError(error: 'Failed to load data'),
    );
  }

  Future<void> _onUpdateCareProvider(
      UpdateCareProvider event, Emitter<CareProviderState> emit) async {
    emit(UpdateCareProviderInProgress());

    try {
      final CareProvider? updatedCareProvider =
          await _careProviderRepository.updateCareProvider(event.careProvider);

      emit(UpdateCareProviderSuccess(
        careProvider: updatedCareProvider,
        message: 'Changes successfully saved.',
      ));
    } on UpdateDataException catch (e) {
      emit(UpdateCareProviderError(error: e.toString()));
    }
  }

  Future<void> _onFetchAssignedPatients(
      FetchAssignedPatients event, Emitter<CareProviderState> emit) async {
    emit(AssignedPatientsLoading());

    try {
      final List<Patient> patients =
          await _careProviderRepository.fetchPatients(event.patientIds);
      emit(AssignedPatientsLoadSuccess(patients: patients));
    } on FetchDataException catch (e) {
      emit(AssignedPatientsLoadError(error: e.toString()));
    }
  }

  Future<void> _onRemovePatient(
      RemovePatient event, Emitter<CareProviderState> emit) async {
    emit(RemovePatientInProgress());

    try {
      await _careProviderRepository.removePatient(
          careProviderId: event.careProviderId, patientId: event.patientId);
      emit(const RemovePatientSuccess(message: 'Patient removed.'));
    } on UpdateDataException catch (e) {
      emit(RemovePatientError(error: e.toString()));
    }
  }
}
