part of 'care_provider_bloc.dart';

abstract class CareProviderEvent extends Equatable {
  const CareProviderEvent();

  @override
  List<Object> get props => <Object>[];
}

class FetchCareProvider extends CareProviderEvent {
  final String careProviderId;
  const FetchCareProvider({required this.careProviderId});

  @override
  List<Object> get props => <Object>[careProviderId];
}

class UpdateCareProvider extends CareProviderEvent {
  final CareProvider careProvider;
  const UpdateCareProvider({required this.careProvider});

  @override
  List<Object> get props => <Object>[careProvider];
}

class FetchAssignedPatients extends CareProviderEvent {
  final List<String> patientIds;
  const FetchAssignedPatients({required this.patientIds});

  @override
  List<Object> get props => <Object>[patientIds];
}

class RemovePatient extends CareProviderEvent {
  final String careProviderId, patientId;
  const RemovePatient({required this.careProviderId, required this.patientId});

  @override
  List<Object> get props => <Object>[careProviderId, patientId];
}
