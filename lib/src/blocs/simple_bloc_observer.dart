// ignore_for_file: always_specify_types, always_declare_return_types

import 'dart:developer' as devtools show log;

import 'package:flutter_bloc/flutter_bloc.dart';

class SimpleBlocObserver extends BlocObserver {
  @override
  void onCreate(BlocBase bloc) {
    super.onCreate(bloc);
    devtools.log('onCreate: ${bloc.runtimeType}');
  }

  @override
  void onEvent(Bloc bloc, Object? event) {
    super.onEvent(bloc, event);
    devtools.log('onEvent $event');
  }

  @override
  onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    devtools.log(
        'onTransition: currentState: ${transition.currentState.runtimeType}, nextState: ${transition.nextState.runtimeType}');
  }

  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    super.onError(bloc, error, stackTrace);
    devtools.log('onError: $error');
  }

  @override
  void onChange(BlocBase bloc, Change change) {
    devtools.log(
        'onChange: currentState: ${change.currentState.runtimeType}, nextState: ${change.nextState.runtimeType}');
    super.onChange(bloc, change);
  }

  @override
  void onClose(BlocBase bloc) {
    super.onClose(bloc);
    devtools.log('${bloc.runtimeType} closed');
  }
}
