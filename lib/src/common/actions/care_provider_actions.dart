import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kindly_components/kindly_components.dart';

import 'actions.dart';

void careProviderLongPress(BuildContext context, CareProvider careProvider) {
  HapticFeedback.lightImpact();
  final TextTheme theme = Theme.of(context).textTheme;
  final TextStyle actionStyle =
      theme.bodyText1!.copyWith(color: Theme.of(context).primaryColor);
  showCupertinoModalPopup<void>(
    context: context,
    builder: (BuildContext context) {
      final AppUser user = context.select((AuthBloc bloc) => bloc.user);
      return CupertinoActionSheet(
        title: Text(
          careProvider.name,
          style: theme.bodyText1!.copyWith(
            color: Colors.black.withOpacity(0.6),
          ),
        ),
        actions: <Widget>[
          CupertinoActionSheetAction(
            child: Text('Call', style: actionStyle),
            onPressed: () {
              Navigator.of(context).pop();
              onMakeCall(careProvider.phone);
            },
          ),
          CupertinoActionSheetAction(
            child: Text('Message', style: actionStyle),
            onPressed: () {
              Navigator.of(context).pop();
              createChannel(context, uid: user.uid, otherId: careProvider.uid);
            },
          ),
        ],
        cancelButton: CupertinoActionSheetAction(
          child: Text('Cancel', style: actionStyle),
          onPressed: () => Navigator.pop(context),
        ),
      );
    },
  );
}
