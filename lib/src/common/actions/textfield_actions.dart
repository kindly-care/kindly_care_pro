import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

void showTextfieldSinglePicker(
  BuildContext context, {
  required String title,
  required List<String> items,
  required String labelText,
  required TextEditingController controller,
}) {
  showModalBottomSheet<void>(
    context: context,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(13.0),
        topRight: Radius.circular(13.0),
      ),
    ),
    builder: (BuildContext context) {
      return _BottomSheet(
        title: title,
        items: items,
        labelText: labelText,
        onSelected: (String? val) {
          controller.text = val!;
        },
        controller: controller,
      );
    },
  );
}

void showShiftPicker(
  BuildContext context, {
  required String title,
  required List<String> items,
  required String labelText,
  required String initial,
  required void Function(String)? onSelected,
}) {
  showModalBottomSheet<void>(
    context: context,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(13.0),
        topRight: Radius.circular(13.0),
      ),
    ),
    builder: (BuildContext context) {
      return _ShiftBottomSheet(
        title: title,
        items: items,
        labelText: labelText,
        onSelected: onSelected,
        initial: initial,
      );
    },
  );
}

void showMultiSelectPicker(
  BuildContext context, {
  required String title,
  required List<String> items,
  required TextEditingController controller,
  Function(List<String>)? onSelectedItems,
}) {
  showModalBottomSheet<void>(
    context: context,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(12.0),
        topRight: Radius.circular(12.0),
      ),
    ),
    builder: (BuildContext context) {
      return _SlidingBottomSheet(
        title: title,
        items: items,
        controller: controller,
        onSelectedItems: onSelectedItems,
      );
    },
  );
}

class _BottomSheet extends StatefulWidget {
  final String? title;
  final String labelText;
  final List<String> items;

  final TextEditingController controller;
  final Function(String?)? onSelected;
  const _BottomSheet({
    Key? key,
    this.title,
    required this.items,
    required this.labelText,
    required this.controller,
    required this.onSelected,
  }) : super(key: key);

  @override
  _BottomSheetState createState() => _BottomSheetState();
}

class _BottomSheetState extends State<_BottomSheet> {
  late String? _groupValue;

  @override
  void initState() {
    super.initState();
    _groupValue = widget.controller.text;
  }

  void _onChanged(String? value) {
    setState(() => _groupValue = value);
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12.0),
          topRight: Radius.circular(12.0),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  TextButton(
                    child: Text(
                      'Cancel',
                      style: theme.bodyText1!.copyWith(
                        fontSize: 18.8.sp,
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                    onPressed: () => Navigator.pop(context),
                  ),
                  Text(
                    widget.title ?? widget.labelText,
                    style: theme.bodyText1!.copyWith(
                      fontSize: 18.8.sp,
                    ),
                  ),
                  TextButton(
                    child: Text(
                      'Apply',
                      style: theme.bodyText1!.copyWith(
                        fontSize: 18.8.sp,
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                    onPressed: () {
                      if (_groupValue == null) {
                        Navigator.pop(context);
                      } else {
                        if (widget.onSelected != null) {
                          widget.onSelected!(_groupValue);
                        }
                        Navigator.pop(context);
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
          const Divider(height: 8.0),
          Expanded(
            flex: 6,
            child: ListView.builder(
              physics: const BouncingScrollPhysics(),
              itemCount: widget.items.length,
              itemBuilder: (BuildContext context, int index) {
                final String item = widget.items[index];
                return RadioListTile<String>(
                  title: Text(item),
                  value: item,
                  groupValue: _groupValue,
                  onChanged: _onChanged,
                  activeColor: Theme.of(context).primaryColor,
                  controlAffinity: ListTileControlAffinity.trailing,
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

class _ShiftBottomSheet extends StatefulWidget {
  final String? title;
  final String initial;
  final String labelText;
  final List<String> items;
  final void Function(String)? onSelected;
  const _ShiftBottomSheet({
    Key? key,
    this.title,
    required this.items,
    required this.initial,
    required this.labelText,
    required this.onSelected,
  }) : super(key: key);

  @override
  _ShiftBottomSheetState createState() => _ShiftBottomSheetState();
}

class _ShiftBottomSheetState extends State<_ShiftBottomSheet> {
  late String _groupValue;

  @override
  void initState() {
    super.initState();
    _groupValue = widget.initial;
  }

  void _onChanged(String? value) {
    setState(() => _groupValue = value!);
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme theme = Theme.of(context).textTheme;
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12.0),
          topRight: Radius.circular(12.0),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  TextButton(
                    child: Text(
                      'Cancel',
                      style: theme.bodyText1!.copyWith(
                        fontSize: 18.8.sp,
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                    onPressed: () => Navigator.pop(context),
                  ),
                  Text(
                    widget.title ?? widget.labelText,
                    style: theme.bodyText1!.copyWith(
                      fontSize: 18.8.sp,
                    ),
                  ),
                  TextButton(
                    child: Text(
                      'Apply',
                      style: theme.bodyText1!.copyWith(
                        fontSize: 18.8.sp,
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                    onPressed: () {
                      if (widget.onSelected != null) {
                        widget.onSelected!(_groupValue);
                      }
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
            ),
          ),
          const Divider(height: 8.0),
          Expanded(
            flex: 6,
            child: ListView.builder(
              physics: const BouncingScrollPhysics(),
              itemCount: widget.items.length,
              itemBuilder: (BuildContext context, int index) {
                final String item = widget.items[index];
                return RadioListTile<String>(
                  title: Text(item),
                  value: item,
                  groupValue: _groupValue,
                  onChanged: _onChanged,
                  activeColor: Theme.of(context).primaryColor,
                  controlAffinity: ListTileControlAffinity.trailing,
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

class _SlidingBottomSheet extends StatefulWidget {
  final String title;
  final List<String> items;
  final Function(List<String>)? onSelectedItems;
  final TextEditingController? controller;

  const _SlidingBottomSheet({
    Key? key,
    this.onSelectedItems,
    required this.items,
    required this.title,
    required this.controller,
  }) : super(key: key);
  @override
  _SlidingBottomSheetState createState() => _SlidingBottomSheetState();
}

class _SlidingBottomSheetState extends State<_SlidingBottomSheet> {
  List<String> _selected = <String>[];

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (widget.controller!.text.isNotEmpty) {
      _selected = widget.controller!.text.split(', ');
    }
  }

  void _onChanged(String source) {
    if (_selected.contains(source)) {
      _selected.remove(source);
    } else {
      _selected.add(source);
    }

    if (widget.onSelectedItems != null) {
      widget.onSelectedItems!(_selected);
    }

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12),
          topRight: Radius.circular(12),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  TextButton(
                    child: Text(
                      'Cancel',
                      style: theme.textTheme.bodyText1!.copyWith(
                        fontSize: 18.8.sp,
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                    onPressed: () => Navigator.pop(context),
                  ),
                  Text(
                    widget.title,
                    style: theme.textTheme.bodyText1!.copyWith(
                      fontSize: 18.8.sp,
                    ),
                  ),
                  TextButton(
                    child: Text(
                      'Done',
                      style: theme.textTheme.subtitle1!.copyWith(
                        fontSize: 18.8.sp,
                        color: theme.primaryColor,
                      ),
                    ),
                    onPressed: () {
                      widget.controller!.text = _selected.join(', ');
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
            ),
          ),
          const Divider(height: 8.0),
          Expanded(
            flex: 8,
            child: ListView.builder(
              physics: const BouncingScrollPhysics(),
              itemCount: widget.items.length,
              itemBuilder: (BuildContext context, int index) {
                final String source = widget.items[index];
                return CheckboxListTile(
                  activeColor: Theme.of(context).primaryColor,
                  value: _selected.contains(source),
                  title: Text(source),
                  onChanged: (_) => _onChanged(source),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
