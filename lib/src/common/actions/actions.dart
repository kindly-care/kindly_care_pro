import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stream_chat_flutter_core/stream_chat_flutter_core.dart';

import '../common.dart';

/// Creates a Chat Channel between two users
void createChannel(BuildContext context,
    {required String uid, required String otherId}) {
  final StreamChatClient client = ChatClient.client;

  final Channel channel =
      client.channel('messaging', extraData: <String, dynamic>{
    'members': <String>[uid, otherId]
  });

  channel.watch();

  Navigator.pushNamed(context, kChatPageRoute, arguments: channel);
}
