import 'package:google_maps_flutter/google_maps_flutter.dart';

class Directions {
  final LatLng location;
  final double distance;
  final Set<Polyline> polylines;
  Directions({
    required this.location,
    required this.distance,
    required this.polylines,
  });
}
