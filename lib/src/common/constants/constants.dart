import 'package:flutter/foundation.dart';

const String kMeal = 'meal';
const String kType = 'type';
const String kChat = 'chat';

const String kTaskType = 'taskType';
const String kMedication = 'medication';
const String kCareProvider = 'careProvider';
const String kNotification = 'notification';

// Users
const String kUser = 'user';
const String kPatient = 'patient';

// Routes
const String kHomePageRoute = 'home';
const String kHaltPageRoute = 'halt';
const String kLoginPageRoute = 'login';
const String kChatPageRoute = 'chat-page';
const String kAccountPageRoute = 'account';
const String kClockInPageRoute = 'clock-in';
const String kPatientsPageRoute = 'patients';
const String kCalenderPageRoute = 'calender';
const String kTimeLogsPageRoute = 'timelogs';
const String kKinProfilePageRoute = 'kin-profile';
const String kMemberProfilePageRoute = 'member-profile';
const String kCreateAccountPageRoute = 'create-account';
const String kPatientSelectPageRoute = 'patient-select';
const String kResetPasswordPageRoute = 'reset-password';
const String kPatientProfilePageRoute = 'patient-profile';
const String kEditCareProviderPageRoute = 'edit-care-provider';
const String kCareProviderProfilePageRoute = 'care-provider-profile';
const String kBloodPressurePageRoute = 'blood-pressure';
const String kBloodGlucosePageRoute = 'blood-glucose';
const String kPatientRecordsPageRoute = 'patient-records';
const String kTemperaturePageRoute = 'temperature';
const String kWeightPageRoute = 'weight';
const String kNotificationsPageRoute = 'notifications';
const String kSettingsPageRoute = 'settings';
const String kOnboardingPageRoute = 'onboarding';
const String kSetUpPagePageRoute = 'setup';
const String kShiftReportPageRoute = 'shift-report';
const String kPatientMapPageRoute = 'patient-map';
const String kContactsPageRoute = 'contacts';
const String kMessagesPageRoute = 'messages';
const String kPatientReportsPageRoute = 'patient-reports';

// Strings
const String kTaskComplete = 'Task Complete?';
const String kActionNotAllowed = 'Action Not Allowed';
const String kCareInterviewRequest = 'Care Interview Request';
const String kNoAdditionalInfo = 'No additional information provided.';
const String kNoCurrentPatients = 'You currently do not have any patients.';
const String kNotClockedIn =
    'You are currently not Clocked In. Please Clock In to begin your shift.';
const String kNoNotifications = 'You currently do not have any notifications.';

// Lists
const List<String> kMoodItems = <String>[
  'Happy',
  'Confused',
  'Neutral',
  'Sad',
];

const List<String> availabilityItems = <String>[
  'Available',
  'Unavailable',
  'Available Day',
  'Available Night',
];

final List<String> kShortWeekDays = <String>[
  'Sun',
  'Mon',
  'Tue',
  'Wed',
  'Thu',
  'Fri',
  'Sat'
];

final List<String> kLongWeekDays = <String>[
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday'
];

final List<String> kServices = <String>[
  'Day Care',
  'Night Care',
  'Live-in Care',
];

final List<String> kQualifications = <String>[
  'Nurse Aid (Red Cross)',
  'Nurse Aid (St John)',
  'Dementia Care (ZARDA)',
  'Palliative Care (Island Hospice)',
  'None',
];

final List<String> kDuties = <String>[
  'Locum',
  'Laundry',
  'Massages',
  'Feeding',
  'Grooming',
  'Bowel Care',
  'Bed Making',
  'Wound Care',
  'Dementia Care',
  'Palliative Care',
  'Personal Care',
  'Meal Preparation',
  'Grocery Shopping',
  'Light Housekeeping',
  'Lifting & Transfers',
  'Medication Reminders',
  'Administering Medication',
  'Physical Activities & Exercises',
];

// Hive Keys
const String kAuthKey = 'auth_key';
const String kWeightKey = 'weight_key';
const String kTemperatureKey = 'temperature_key';
const String kBloodGlucoseKey = 'blood_glucose_key';
const String kBloodPressureKey = 'blood_pressure_key';

// Analytics Events
const String kWeightEvt = 'weight_evt';
const String kTemperatureEvt = 'temperature_evt';
const String kBloodGlucoseEvt = 'blood_glucose_evt';
const String kBloodPressureEvt = 'blood_pressure_evt';

// LastRecords
const String kLastWeightRecord = 'lastWeightRecord';
const String kLastTemperatureRecord = 'lastTemperatureRecord';
const String kLastBloodGlucoseRecord = 'lastBloodGlucoseRecord';
const String kLastBloodPressureRecord = 'lastBloodPressureRecord';

// Vitals
const String kWeight = 'weight';
const String kTemperature = 'temperature';
const String kBloodGlucose = 'blood_glucose';
const String kBloodPressure = 'blood_pressure';

// Errors
const String kNetworkError =
    'Something went wrong. Please check your internet connection and try again.';

// Misc
/// date format (year-month-date)
const String kYMD = 'yyyy-MM-dd';
const String kReports = 'reports';
const String kReviews = 'reviews';
const int kAnimationDuration = 400;
const String kUserType = 'userType';
const String kTimeLogs = 'time_logs';
const String kTimeLogKey = 'timelog';
const String kCreatedAt = 'createdAt';
const String kCareTasks = 'care_tasks';
const String kRepeatDays = 'repeatDays';
const String kDateCreated = 'dateCreated';
const String kCareActions = 'care_actions';
const String kAuthDetails = 'auth_details';
const String kContactPhone = '+263773018727';
const String kNotifications = 'notifications';
const String kIsAppFirstRun = 'isAppFirstRun';
const Duration kTimeOut = Duration(seconds: 12);

const String kMapKey = 'google_api_key';
const String kStreamChatKey = 'stream_chat_api';
const String kWiredashSecret = 'wiredash_secret';
const String kWiredashProjectId = 'wiredash_projectId';


const String kInterviewRequest = 'interviewRequest';
const String kContactEmail = 'primelabs@tutamail.com';
const String kAppURL =
    'https://play.google.com/store/apps/details?id=com.primelabs.casa_pro';

const String kDefaultAvatar =
    'https://firebasestorage.googleapis.com/v0/b/casa-333c5.appspot.com/o/app%2Fdefault-avatar.png?alt=media&token=893bccf7-6fe6-430f-b132-e352baae1748';
const String kAbout =
    'Made with extreme love and care by the good people over at PrimeLabs.\n\nAll rights reserved.';
const String kChatNotification = kReleaseMode
    ? 'notifications-sendChatNotification'
    : 'notifications-sendChatNotificationTest';
