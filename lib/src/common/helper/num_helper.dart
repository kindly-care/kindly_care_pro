import 'dart:math';

import 'package:kindly_components/kindly_components.dart';

/// returns both hours and minutes
String hoursFromTimeLogsLong(List<TimeLog> timeLogs) {
  final int totalMinutes = timeLogs.fold<int>(0, (int prev, TimeLog timeLog) {
    final int minutes =
        timeLog.finishTime!.difference(timeLog.startTime!).inMinutes;
    return prev + minutes;
  });

  final int hours = totalMinutes ~/ 60;
  final int minutes = totalMinutes % 60;

  if (hours < 10 && minutes < 10) {
    return '0$hours:0${minutes.toString()}';
  } else if (hours < 10 && minutes > 10) {
    return '0$hours:${minutes.toString()}';
  } else if (hours > 10 && minutes < 10) {
    return '$hours:0${minutes.toString()}';
  } else {
    return '${hours.toString()}:${minutes.toString()}';
  }
}

/// returns hours only
String hoursFromTimeLogsShort(List<TimeLog> timeLogs) {
  final int totalMinutes = timeLogs.fold<int>(0, (int prev, TimeLog timeLog) {
    final int minutes =
        timeLog.finishTime!.difference(timeLog.startTime!).inMinutes;
    return prev + minutes;
  });

  final int hours = totalMinutes ~/ 60;

  return hours.toString();
}

int numberGenerator() {
  const int min = 100000000;
  const int max = 999999999;

  final Random random = Random();

  return random.nextInt(max - min);
}

String getMonth(int month) {
  switch (month) {
    case 1:
      return 'January';
    case 2:
      return 'February';
    case 3:
      return 'March';
    case 4:
      return 'April';
    case 5:
      return 'May';
    case 6:
      return 'June';
    case 7:
      return 'July';
    case 8:
      return 'August';
    case 9:
      return 'September';
    case 10:
      return 'October';
    case 11:
      return 'November';
    case 12:
      return 'December';
    default:
      return 'Unknown';
  }
}
