// ignore: depend_on_referenced_packages
import 'package:permission_handler/permission_handler.dart';

class PermissionHelper {
  Future<void> requestPermission() async {
    await <Permission>[
      Permission.location,
      Permission.storage,
      Permission.camera,
    ].request();
  }
}
