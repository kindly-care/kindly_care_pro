import 'package:hive_flutter/hive_flutter.dart';
import 'package:kindly_components/kindly_components.dart';

import '../constants/constants.dart';

class TimeLogBox {
  static late final Box<TimeLog> box;

  static Future<Box<TimeLog>> init() async =>
      box = await Hive.openBox<TimeLog>(kTimeLogs);
}

class CareActionBox {
  static late final Box<CareAction> box;

  static Future<Box<CareAction>> init() async =>
      box = await Hive.openBox<CareAction>(kCareActions);
}

class ReportBox {
  static late final Box<Report> box;

  static Future<Box<Report>> init() async =>
      box = await Hive.openBox<Report>(kReports);
}
