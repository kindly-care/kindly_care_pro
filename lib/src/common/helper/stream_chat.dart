import 'package:kindly_components/kindly_components.dart';

import 'package:stream_chat_flutter_core/stream_chat_flutter_core.dart';

import '../constants/constants.dart';

abstract class ChatHelpers {
  static String getChannelImage(Channel channel, AppUser user) {
    if (channel.image != null) {
      return channel.image!;
    } else if (channel.state?.members.isNotEmpty ?? false) {
      final List<Member>? others = channel.state?.members
          .where((Member member) => member.userId != user.uid)
          .toList();
      if (others?.length == 1) {
        return others!.first.user?.image ?? kDefaultAvatar;
      } else {
        return kDefaultAvatar;
      }
    } else {
      return kDefaultAvatar;
    }
  }

  static String getChannelName(Channel channel, AppUser user) {
    if (channel.name != null) {
      return channel.name!;
    } else if (channel.state?.members.isNotEmpty ?? false) {
      final List<Member>? others = channel.state?.members
          .where((Member member) => member.userId != user.uid)
          .toList();
      if (others?.length == 1) {
        return others!.first.user?.name ?? 'Channel Name';
      } else {
        return 'Channel Name';
      }
    } else {
      return 'Channel Name';
    }
  }

  static String getOtherRecipientId(Channel channel, AppUser user) {
    if (channel.state?.members.isNotEmpty ?? false) {
      final List<Member>? others = channel.state?.members
          .where((Member member) => member.userId != user.uid)
          .toList();
      if (others?.length == 1) {
        return others!.first.user?.id ?? '';
      } else {
        return '';
      }
    } else {
      return '';
    }
  }
}
