import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:stream_chat_flutter_core/stream_chat_flutter_core.dart';
import 'package:stream_chat_persistence/stream_chat_persistence.dart';

import '../constants/constants.dart';

class ChatClient {
  static late final StreamChatClient client;

  static void init({required FirebaseRemoteConfig config}) {
    final String key = config.getString(kStreamChatKey);
    client = StreamChatClient(key);
    client.chatPersistenceClient = StreamChatPersistenceClient(
      logLevel: Level.SEVERE,
      connectionMode: ConnectionMode.background,
    );
  }
}
