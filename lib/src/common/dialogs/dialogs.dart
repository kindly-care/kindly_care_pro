import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../app/widgets/textfields/textfields.dart';

void oneButtonDialog(
  BuildContext context, {
  required String title,
  required String content,
  required String buttonText,
  required VoidCallback onPressed,
  bool barrierDismissible = true,
}) {
  final TextTheme theme = Theme.of(context).textTheme;
  showDialog<dynamic>(
    context: context,
    barrierDismissible: barrierDismissible,
    builder: (_) {
      return AlertDialog(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0.px)),
        title: Text(title, style: theme.titleLarge),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(content, style: theme.bodyLarge),
            SizedBox(height: 1.8.h),
            SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  elevation: 0.0,
                  primary: Colors.grey.shade200,
                  splashFactory: NoSplash.splashFactory,
                  shadowColor: const Color.fromRGBO(0, 0, 0, 0.16),
                ),
                onPressed: onPressed,
                child: Text(buttonText, style: theme.bodyLarge),
              ),
            ),
          ],
        ),
      );
    },
  );
}

dynamic twoButtonDialog(
  BuildContext context, {
  required String title,
  required String content,
  required String buttonText1,
  required String buttonText2,
  required VoidCallback onPressed1,
  required VoidCallback onPressed2,
  bool barrierDismissible = true,
  bool isDestructiveAction = false,
  bool backButtonClosesDialog = true,
}) {
  final TextTheme theme = Theme.of(context).textTheme;
  return showDialog<dynamic>(
    context: context,
    barrierDismissible: barrierDismissible,
    builder: (_) {
      return WillPopScope(
        onWillPop: () async => backButtonClosesDialog,
        child: AlertDialog(
          actionsAlignment: MainAxisAlignment.center,
          actionsPadding: EdgeInsets.symmetric(horizontal: 4.0.w),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0.px)),
          title: Text(title, style: theme.titleLarge),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(content, style: theme.bodyLarge),
              SizedBox(height: 1.8.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        elevation: 0.0,
                        primary: Colors.grey.shade200,
                        splashFactory: NoSplash.splashFactory,
                        shadowColor: const Color.fromRGBO(0, 0, 0, 0.16),
                      ),
                      onPressed: onPressed1,
                      child: Text(buttonText1, style: theme.bodyLarge),
                    ),
                  ),
                  SizedBox(width: 1.8.w),
                  Expanded(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        elevation: 0.0,
                        splashFactory: NoSplash.splashFactory,
                        primary: isDestructiveAction
                            ? Colors.red.shade600
                            : Theme.of(context).primaryColor,
                        shadowColor: const Color.fromRGBO(0, 0, 0, 0.16),
                      ),
                      onPressed: onPressed2,
                      child: Text(
                        buttonText2,
                        style: theme.bodyLarge?.copyWith(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    },
  );
}

dynamic threeButtonDialog(
  BuildContext context, {
  required String title,
  required String content,
  required String buttonText1,
  required String buttonText2,
  required String buttonText3,
  required VoidCallback onPressed1,
  required VoidCallback onPressed2,
  required VoidCallback onPressed3,
  bool barrierDismissible = true,
  bool isDestructiveAction = false,
  bool isButtonText1Disabled = false,
  bool isButtonText2Disabled = false,
  bool isButtonText3Disabled = false,
  bool backButtonClosesDialog = true,
}) {
  final TextTheme theme = Theme.of(context).textTheme;

  return showDialog<dynamic>(
    context: context,
    barrierDismissible: barrierDismissible,
    builder: (_) {
      return WillPopScope(
        onWillPop: () async => backButtonClosesDialog,
        child: AlertDialog(
          title: Text(title, style: theme.titleLarge),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(content, style: theme.bodyLarge),
              SizedBox(height: 1.8.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        elevation: 0.0,
                        primary: Colors.grey.shade200,
                        splashFactory: NoSplash.splashFactory,
                        shadowColor: const Color.fromRGBO(0, 0, 0, 0.16),
                      ),
                      onPressed: isButtonText1Disabled ? null : onPressed1,
                      child: Text(buttonText1,
                          style: theme.bodyLarge?.copyWith(
                            color: isButtonText1Disabled ? Colors.white : null,
                          )),
                    ),
                  ),
                  SizedBox(width: 1.8.w),
                  Expanded(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        elevation: 0.0,
                        primary: Colors.grey.shade200,
                        splashFactory: NoSplash.splashFactory,
                        shadowColor: const Color.fromRGBO(0, 0, 0, 0.16),
                      ),
                      onPressed: isButtonText2Disabled ? null : onPressed2,
                      child: Text(buttonText2,
                          style: theme.bodyLarge?.copyWith(
                            color: isButtonText2Disabled ? Colors.white : null,
                          )),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 0.8.h),
              SizedBox(
                width: double.infinity,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    elevation: 0.0,
                    splashFactory: NoSplash.splashFactory,
                    primary: Theme.of(context).primaryColor,
                    shadowColor: const Color.fromRGBO(0, 0, 0, 0.16),
                  ),
                  onPressed: onPressed3,
                  child: Text(buttonText3,
                      style: theme.bodyLarge?.copyWith(color: Colors.white)),
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
}

void showInputDialog(
  BuildContext context, {
  required String title,
  String labelText = '',
  String suffixText = '',
  String hintText = '',
  required VoidCallback onSubmit,
  required TextEditingController controller,
}) {
  final ThemeData theme = Theme.of(context);
  showCupertinoDialog<void>(
    context: context,
    builder: (BuildContext context) {
      return CupertinoAlertDialog(
        title: Text(
          title,
          style:
              theme.textTheme.headline6!.copyWith(fontWeight: FontWeight.w700),
        ),
        content: TextFieldComponent(
          hasDecimal: true,
          hintText: hintText,
          labelText: labelText,
          suffixText: suffixText,
          controller: controller,
        ),
        actions: <Widget>[
          TextButton(
            child: Text(
              'Cancel',
              style: theme.textTheme.bodyText1!
                  .copyWith(color: theme.primaryColor),
            ),
            onPressed: () => Navigator.pop(context),
          ),
          TextButton(
            onPressed: onSubmit,
            child: Text(
              'Submit',
              style: theme.textTheme.bodyText1!
                  .copyWith(color: theme.primaryColor),
            ),
          ),
        ],
      );
    },
  );
}

void showBloodPressureInputDialog(
  BuildContext context, {
  required VoidCallback onSubmit,
  required TextEditingController systolicController,
  required TextEditingController diastolicController,
}) {
  final TextTheme theme = Theme.of(context).textTheme;
  showCupertinoDialog<void>(
    context: context,
    builder: (BuildContext context) {
      return CupertinoAlertDialog(
        title: Text('Add Blood Pressure', style: theme.titleLarge),
        content: Column(
          children: <Widget>[
            TextFieldComponent(
              labelText: '',
              hasDecimal: true,
              hintText: 'Systolic',
              suffixText: 'mmHg',
              controller: systolicController,
            ),
            SizedBox(height: 1.0.h),
            TextFieldComponent(
              labelText: '',
              hasDecimal: true,
              hintText: 'Diastolic',
              suffixText: 'mmHg',
              controller: diastolicController,
            ),
          ],
        ),
        actions: <Widget>[
          TextButton(
            child: Text(
              'Cancel',
              style: theme.bodyLarge
                  ?.copyWith(color: Theme.of(context).primaryColor),
            ),
            onPressed: () => Navigator.pop(context),
          ),
          TextButton(
            onPressed: onSubmit,
            child: Text(
              'Submit',
              style: theme.bodyLarge
                  ?.copyWith(color: Theme.of(context).primaryColor),
            ),
          ),
        ],
      );
    },
  );
}

void showEmailInputDialog(
  BuildContext context, {
  required String title,
  String hintText = '',
  required VoidCallback onSubmit,
  required TextEditingController controller,
}) {
  final TextTheme theme = Theme.of(context).textTheme;
  showDialog<void>(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(
          title,
          style: theme.titleLarge?.copyWith(fontWeight: FontWeight.w700),
        ),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            MaterialTextField(
              hintText: hintText,
              controller: controller,
              textInputType: TextInputType.emailAddress,
            ),
            SizedBox(height: 1.8.h),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      elevation: 0.0,
                      primary: Colors.grey.shade200,
                      splashFactory: NoSplash.splashFactory,
                      shadowColor: const Color.fromRGBO(0, 0, 0, 0.16),
                    ),
                    onPressed: () => Navigator.pop(context),
                    child: Text('Cancel', style: theme.bodyLarge),
                  ),
                ),
                SizedBox(width: 1.8.w),
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      elevation: 0.0,
                      splashFactory: NoSplash.splashFactory,
                      shadowColor: const Color.fromRGBO(0, 0, 0, 0.16),
                    ),
                    onPressed: onSubmit,
                    child: Text(
                      'Ok',
                      style: theme.bodyLarge?.copyWith(color: Colors.white),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    },
  );
}
