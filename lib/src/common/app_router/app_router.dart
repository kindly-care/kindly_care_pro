import 'package:flutter/cupertino.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:stream_chat_flutter_core/stream_chat_flutter_core.dart';

import '../../app/account/patients_page/patients_page.dart';
import '../../app/account/time_logs_page/select_patient_time_logs_page.dart';
import '../../app/home/home_page.dart';
import '../../app/messages/messages.dart';
import '../../app/misc/care_provider/care_provider_profile/care_provider_profile_page.dart';
import '../../app/misc/care_provider/care_provider_profile/edit_care_provider/edit_care_provider_page.dart';
import '../../app/misc/clock_in_page/clock_in_page.dart';
import '../../app/misc/kin_profile/kin_profile_page.dart';
import '../../app/misc/member_profile/member_profile_page.dart';
import '../../app/misc/patient_profile/patient_profile_page.dart';
import '../../app/misc/patient_profile/patient_reports_page/patient_reports_page.dart';
import '../../app/notifications/notifications_page.dart';
import '../../app/onboarding/onboarding_page.dart';
import '../../app/settings_page/settings_page.dart';
import '../../app/setup/halt_page.dart';
import '../../app/setup/setup_page.dart';
import '../../app/views/patient_map/patient_map_page.dart';
import '../constants/constants.dart';
import 'error_route_page.dart';

class AppRoute {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final Object? args = settings.arguments;
    switch (settings.name) {
      case kCreateAccountPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => const CreateAccountPage(),
        );
      case kPatientProfilePageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => PatientProfilePage(patientId: args as String),
        );
      case kResetPasswordPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => const ResetPasswordPage(),
        );
      case kOnboardingPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => const OnboardingPage(),
        );
      case kHaltPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => HaltPage(user: args as AppUser),
        );
      case kContactsPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => const ContactsPage(),
        );
      case kSetUpPagePageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => SetUpPage(user: args as AppUser),
        );
      case kMemberProfilePageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => MemberProfilePage(user: args as AppUser),
        );
      case kMessagesPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => const MessagesPage(),
        );
      case kPatientReportsPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => PatientReportsPage(patient: args as Patient),
        );
      case kLoginPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (BuildContext context) => LoginPage(
            onAuthSuccess: (AppUser user) {
              if (user is CareProvider) {
                Navigator.popAndPushNamed(context, kHomePageRoute,
                    arguments: user);
              } else {
                Navigator.popAndPushNamed(context, kHaltPageRoute,
                    arguments: user);
              }
            },
          ),
        );
      case kClockInPageRoute:
        return CupertinoPageRoute<String>(
          settings: settings,
          fullscreenDialog: true,
          builder: (_) => ClockInPage(careProvider: args as CareProvider),
        );
      case kHomePageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => HomePage(careProvider: args as CareProvider),
        );
      case kSettingsPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => SettingsPage(careProvider: args as CareProvider),
        );
      case kChatPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => ChatPage(channel: args as Channel),
        );
      case kPatientsPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => PatientsPage(careProvider: args as CareProvider),
        );
      case kPatientSelectPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) =>
              SelectPatientTimelogsPage(careProvider: args as CareProvider),
        );
      case kEditCareProviderPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) =>
              EditCareProviderPage(careProvider: args as CareProvider),
        );
      case kNotificationsPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => NotificationsPage(careProvider: args as CareProvider),
        );
      case kCareProviderProfilePageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) =>
              CareProviderProfilePage(careProviderId: args as String),
        );
      case kPatientMapPageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => PatientMapPage(patient: args as Patient),
        );
      case kKinProfilePageRoute:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => KinProfilePage(kin: args as AppUser),
        );
      default:
        return CupertinoPageRoute<void>(
          settings: settings,
          builder: (_) => const ErrorRoutePage(),
        );
    }
  }
}
