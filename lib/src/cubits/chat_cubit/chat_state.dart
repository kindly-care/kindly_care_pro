part of 'chat_cubit.dart';

abstract class ChatState extends Equatable {
  const ChatState();

  @override
  List<Object> get props => <Object>[];
}

class ChatInitial extends ChatState {
  const ChatInitial();
}

class ChatUserConnecting extends ChatState {
  const ChatUserConnecting();
}

class ChatUserConnected extends ChatState {
  const ChatUserConnected();
}

class ChatUserConnectError extends ChatState {
  final String message;
  const ChatUserConnectError(this.message);

  @override
  List<Object> get props => <Object>[message];
}
