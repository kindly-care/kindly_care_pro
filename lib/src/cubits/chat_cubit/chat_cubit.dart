import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:stream_chat_flutter_core/stream_chat_flutter_core.dart';

import '../../common/common.dart';
import '../../data/services/notification/notification_repository.dart';

part 'chat_state.dart';

class ChatCubit extends Cubit<ChatState> {
  final NotificationRepository notificationRepository;
  ChatCubit({required this.notificationRepository})
      : super(const ChatInitial());

  Future<void> connectStreamUser(AppUser user) async {
    emit(const ChatUserConnecting());
    final StreamChatClient client = ChatClient.client;

    try {
      await client.connectUser(
        User(
          id: user.uid,
          name: user.name,
          image: user.avatar,
          extraData: <String, dynamic>{
            'phone': user.phone,
          },
        ),
        user.streamToken,
      );

      emit(const ChatUserConnected());
    } on FetchDataException catch (e) {
      emit(ChatUserConnectError(e.toString()));
    }
  }

  void sendChatNotification({
    required String sender,
    required String message,
    required String recipientId,
  }) {
    notificationRepository.sendChatNotification(
        message: message, sender: sender, recipientId: recipientId);
  }

  void disconnectChat() {
    final StreamChatClient client = ChatClient.client;
    client.chatPersistenceClient?.disconnect(flush: true);
    client.closeConnection();
  }

  @override
  Future<void> close() {
    final StreamChatClient client = ChatClient.client;
    client.chatPersistenceClient?.disconnect();
    client.closeConnection();
    return super.close();
  }
}
