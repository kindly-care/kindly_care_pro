// ignore_for_file: depend_on_referenced_packages

import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:rxdart/rxdart.dart';

import '../../data/services/services.dart';

part 'account_state.dart';

class AccountCubit extends Cubit<AccountState> {
  final AuthRepository _authRepository;
  final TimeLogRepository _timeLogRepository;
  final CareProviderRepository _careProviderRepository;
  AccountCubit({
    required AuthRepository authRepository,
    required TimeLogRepository timeLogRepository,
    required CareProviderRepository careProviderRepository,
  })  : _authRepository = authRepository,
        _timeLogRepository = timeLogRepository,
        _careProviderRepository = careProviderRepository,
        super(const AccountState());

  StreamSubscription<AppUser?>? _userSubscription;
  StreamSubscription<Object>? _multiStreamSubscription;

  void onFetchAccountData() {
    emit(const AccountState());
    try {
      _userSubscription?.cancel();
      _userSubscription =
          _authRepository.fetchCurrentUser().listen((AppUser? user) {
        if (user != null && user is CareProvider) {
          _fetchCareProviderData(user);
        }
      });
    } on FetchDataException catch (e) {
      emit(AccountState(status: AccountStatus.error, message: e.toString()));
    }
  }

  void _fetchCareProviderData(CareProvider careProvider) {
    try {
      final Stream<List<Patient>> patientsStream = _careProviderRepository
          .fetchPatientsStream(careProvider.assignedPatients.keys.toList());
      final Stream<List<TimeLog>> timeLogsStream = _timeLogRepository
          .fetchCareProviderCurrentMonthTimeLogs(careProvider.uid);

      _multiStreamSubscription?.cancel();
      _multiStreamSubscription = CombineLatestStream.list<dynamic>(
              <Stream<Object?>>[patientsStream, timeLogsStream])
          .listen((List<dynamic> value) {
        final List<Patient> patients = value[0] as List<Patient>;
        final List<TimeLog> timelogs = value[1] as List<TimeLog>;

        emit(AccountState(
          patients: patients,
          timelogs: timelogs,
          status: AccountStatus.success,
        ));
      });
    } on Exception {
      rethrow;
    }
  }

  @override
  Future<void> close() {
    _userSubscription?.cancel();
    _multiStreamSubscription?.cancel();
    return super.close();
  }
}
