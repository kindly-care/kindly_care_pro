part of 'account_cubit.dart';

enum AccountStatus { loading, success, error }

class AccountState {
  final String message;
  final AccountStatus status;
  final List<Patient> patients;
  final List<TimeLog> timelogs;
  const AccountState({
    this.message = '',
    this.patients = const <Patient>[],
    this.timelogs = const <TimeLog>[],
    this.status = AccountStatus.loading,
  });
}
