import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../common/directions/directions.dart';

abstract class LocationRepository {
  /// get user's current position [LatLng].
  Future<LatLng> getCurrentPosition();

  /// get user's position [LatLng] from area [String].
  Future<LatLng> getPositionFromArea(String area);

  /// fetches [Directions] to [destination]. Throws [LocationException].
  Future<Directions> fetchDirections(LatLng destination);

  /// get distant in km from current position.
  Future<double> getDistanceFromCurrentPosition(LatLng destination);
}
