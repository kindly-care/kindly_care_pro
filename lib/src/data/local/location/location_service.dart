import 'dart:ui';

import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../common/constants/constants.dart';
import '../../../common/directions/directions.dart';
import 'location_repository.dart';

class LocationService implements LocationRepository {
  final PolylinePoints _polylinePoints = PolylinePoints();
  final FirebaseRemoteConfig _config = FirebaseRemoteConfig.instance;

  @override
  Future<LatLng> getCurrentPosition() async {
    bool serviceEnabled;
    LocationPermission permission;
    try {
      serviceEnabled = await Geolocator.isLocationServiceEnabled();
      if (!serviceEnabled) {
        return const LatLng(-17.7885837, 31.063817);
        // throw LocationException(
        // 'Location services are disabled. Please enable location services and try again.');
      }

      permission = await Geolocator.checkPermission();
      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.deniedForever) {
          throw LocationException(
              'Denied location permission. Please grant permission and try again.');
        }
      }

      final Position position = await Geolocator.getCurrentPosition();
      return LatLng(position.latitude, position.longitude);
    } on Exception catch (e) {
      throw LocationException(e.toString());
    }
  }

  @override
  Future<double> getDistanceFromCurrentPosition(LatLng destination) async {
    try {
      final LatLng pos = await getCurrentPosition();
      final double distMeters = Geolocator.distanceBetween(
        pos.latitude,
        pos.longitude,
        destination.latitude,
        destination.longitude,
      );

      return distMeters / 1000;
    } on Exception {
      throw LocationException('');
    }
  }

  @override
  Future<LatLng> getPositionFromArea(String area) async {
    try {
      final List<Location> locations =
          await locationFromAddress(area, localeIdentifier: 'en_ZW');
      if (locations.isEmpty) {
        throw LocationException('Failed to get location');
      } else {
        return LatLng(locations.first.latitude, locations.first.longitude);
      }
    } on Exception catch (e) {
      throw LocationException(e.toString());
    }
  }

  @override
  Future<Directions> fetchDirections(LatLng destination) async {
    final List<LatLng> polylineCoords = <LatLng>[];
    final Set<Polyline> polylines = <Polyline>{};

    try {
      final LatLng location = await getCurrentPosition();
      final String mapKey = _config.getString(kMapKey);

      final PolylineResult result =
          await _polylinePoints.getRouteBetweenCoordinates(
        mapKey,
        PointLatLng(location.latitude, location.longitude),
        PointLatLng(destination.latitude, destination.longitude),
      );

      if (result.status == 'OK') {
        for (final PointLatLng point in result.points) {
          polylineCoords.add(LatLng(point.latitude, point.longitude));
        }

        polylines.add(
          Polyline(
            width: 4,
            points: polylineCoords,
            color: const Color(0xFF54D3C2),
            polylineId: const PolylineId('poly'),
          ),
        );

        final double distMeters = Geolocator.distanceBetween(
          location.latitude,
          location.longitude,
          destination.latitude,
          destination.latitude,
        );

        final double distKM = distMeters / 10000;

        final Directions directions = Directions(
            location: location, distance: distKM, polylines: polylines);

        return directions;
      } else {
        throw LocationException(
            "Failed to get directions, please make sure that you have an active internet connection and that your Location Services are enabled in your device's Settings.");
      }
    } catch (_) {
      throw LocationException(
          "Failed to get directions, please make sure that you have an active internet connection and that your Location Services are enabled in your device's Settings.");
    }
  }
}
