import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';

final CollectionReference<Map<String, dynamic>> userCollection =
    FirebaseFirestore.instance
        .collection(kReleaseMode ? 'users' : 'care_users');
