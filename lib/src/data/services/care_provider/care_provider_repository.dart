import 'package:kindly_components/kindly_components.dart';


abstract class CareProviderRepository {
  /// updates and fetches [CareProvider]. Throws [UpdateDataException].
  Future<CareProvider?> updateCareProvider(CareProvider careProvider);

  /// fetches [Patient]s. Throws [FetchDataException].
  Future<List<Patient>> fetchPatients(List<String> patientIds);

  /// fetches a stream of [Patient]s. Throws [FetchDataException].
  Stream<List<Patient>> fetchPatientsStream(List<String> patientIds);

  /// fetches [CareProvider]s [Review]s. Throws [FetchDataException].
  Future<List<Review>> fetchCareProviderReviews(String careProviderId);

  /// removes a [Patient] from being assigned to an [CareProvider]. Throws [UpdateDataException].
  Future<void> removePatient(
      {required String careProviderId, required String patientId});

  /// fetches [CareProvider] by id. Throws [FetchDataException].
  Stream<CareProvider?> fetchCareProviderById(String uid);

  /// deletes an [CareProvider]. Throws [UpdateDataException].
  Future<void> deleteCareProvider(String agentId);
}
