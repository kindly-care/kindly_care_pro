import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../common/common.dart';
import '../../references.dart';
import '../firebase_helper.dart';
import 'care_provider_repository.dart';

class CareProviderService implements CareProviderRepository {
  final FirebaseFirestore _db = FirebaseFirestore.instance;

  @override
  Future<CareProvider?> updateCareProvider(CareProvider careProvider) async {
    String? avatarURL;
    final List<String> imageURLs = careProvider.images;

    try {
      if (careProvider.localAvatar != null) {
        avatarURL =
            await getDownloadURL(careProvider.localAvatar!, careProvider.uid);
      }

      if (careProvider.localImages != null &&
          careProvider.localImages!.isNotEmpty) {
        final List<String> images = await getDownloadURLs(
            files: careProvider.localImages!, pathId: careProvider.uid);
        imageURLs.addAll(images);
      }

      await userCollection
          .doc(careProvider.uid)
          .update(careProvider
              .copyWith(avatar: avatarURL, images: imageURLs)
              .toJson())
          .timeout(kTimeOut);

      final DocumentSnapshot<Map<String, dynamic>> doc =
          await userCollection.doc(careProvider.uid).get();

      if (doc.exists) {
        return CareProvider.fromJson(doc.data()!);
      } else {
        return null;
      }
    } on Exception {
      throw UpdateDataException('Failed to save changes');
    }
  }

  @override
  Future<List<Review>> fetchCareProviderReviews(String careProviderId) async {
    try {
      final QuerySnapshot<Map<String, dynamic>> query = await userCollection
          .doc(careProviderId)
          .collection(kReviews)
          .orderBy('createdAt', descending: true)
          .get()
          .timeout(kTimeOut);

      return query.docs
          .map((DocumentSnapshot<Map<String, dynamic>> doc) =>
              Review.fromJson(doc.data()!))
          .toList();
    } on Exception {
      throw FetchDataException('Failed to load data');
    }
  }

  @override
  Future<List<Patient>> fetchPatients(List<String> patientIds) async {
    try {
      if (patientIds.isEmpty) {
        return <Patient>[];
      }

      final QuerySnapshot<Map<String, dynamic>> query = await userCollection
          .where(kUserType, isEqualTo: kPatient)
          .where('uid', whereIn: patientIds)
          .get()
          .timeout(kTimeOut);

      return query.docs
          .map((DocumentSnapshot<Map<String, dynamic>> doc) =>
              Patient.fromJson(doc.data()!))
          .toList();
    } on Exception {
      throw FetchDataException('Failed to load data');
    }
  }

  @override
  Stream<List<Patient>> fetchPatientsStream(List<String> patientIds) {
    try {
      if (patientIds.isEmpty) {
        patientIds.add('placeholder');
      }

      final Stream<QuerySnapshot<Map<String, dynamic>>> query = userCollection
          .where(kUserType, isEqualTo: kPatient)
          .where('uid', whereIn: patientIds)
          .snapshots();

      return query.map(
        (QuerySnapshot<Map<String, dynamic>> snapshot) => snapshot.docs
            .map((QueryDocumentSnapshot<Map<String, dynamic>> doc) =>
                Patient.fromJson(doc.data()))
            .toList(),
      );
    } on Exception {
      throw FetchDataException('Failed to load data');
    }
  }

  @override
  Future<void> removePatient(
      {required String careProviderId, required String patientId}) async {
    final WriteBatch batch = _db.batch();

    try {
      final DocumentReference<Map<String, dynamic>> agentDoc =
          userCollection.doc(careProviderId);

      final DocumentReference<Map<String, dynamic>> patientDoc =
          userCollection.doc(patientId);

      batch.update(patientDoc, <String, dynamic>{
        'assignedCareProviders.$careProviderId': FieldValue.delete(),
      });
      batch.update(agentDoc, <String, dynamic>{
        'assignedPatients.$patientId': FieldValue.delete(),
        'patients': FieldValue.arrayRemove(<String>[patientId]),
      });

      await batch.commit().timeout(kTimeOut);
    } on Exception {
      throw UpdateDataException('Operation failed');
    }
  }

  @override
  Stream<CareProvider?> fetchCareProviderById(String uid) {
    try {
      final Stream<DocumentSnapshot<Map<String, dynamic>>> docStream =
          userCollection.doc(uid).snapshots();

      return docStream.map((DocumentSnapshot<Map<String, dynamic>> doc) =>
          doc.exists ? CareProvider.fromJson(doc.data()!) : null);
    } on Exception {
      throw FetchDataException('Failed to load data');
    }
  }

// TODO(me): remove all assigned agents as well
  @override
  Future<void> deleteCareProvider(String careProviderId) async {
    try {
      await userCollection.doc(careProviderId).delete();
      analytics.logEvent(name: 'remove_care_provider');
    } on Exception {
      throw UpdateDataException('Operation failed');
    }
  }
}
