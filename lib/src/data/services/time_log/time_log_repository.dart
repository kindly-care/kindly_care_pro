import 'package:kindly_components/kindly_components.dart';

abstract class TimeLogRepository {
  /// saves [TimeLog] to cache. Throws [UpdateDataException].
  Future<void> saveTimeLogToCache(TimeLog timeLog);

  /// fetches cached [TimeLog]s. Throws [FetchDataException].
  Future<List<TimeLog>> fetchTimeLogsFromCache();

  /// submits [TimeLog] to database. Throws [UpdateDataException].
  Future<void> submitTimeLog(TimeLog timelog);

  /// submits multiple [TimeLog]s to database. Throws [UpdateDataException].
  Future<void> submitTimeLogs(List<TimeLog> timelogs);

  /// fetches [CareProvider]'s [TimeLog]s. Throws [FetchDataException].
  Future<List<TimeLog>> fetchCareProviderTimeLogsByMonth(
      {required String careProviderId,
      required String patientId,
      required int month});

  /// fetches a stream of [TimeLog]s. Throws [FetchDataException].
  Stream<List<TimeLog>> fetchCareProviderCurrentMonthTimeLogs(
      String careProviderId);

  /// updates [CareProvider]'s clocked-in status
  Future<void> updateClockedInStatus(CareProvider careProvider);
}
