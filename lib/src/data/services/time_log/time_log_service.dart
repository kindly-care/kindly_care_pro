import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dart_date/dart_date.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../common/analytics/analytics.dart';
import '../../../common/constants/constants.dart';
import '../../references.dart';
import 'time_log_repository.dart';

class TimeLogService implements TimeLogRepository {
  final FirebaseFirestore _db = FirebaseFirestore.instance;

  @override
  Future<void> saveTimeLogToCache(TimeLog timelog) async {
    try {
      final Box<TimeLog> box = await Hive.openBox<TimeLog>(kTimeLogs);
      await box.put(timelog.timeLogId, timelog);
    } on Exception {
      throw UpdateDataException('Operation failed');
    }
  }

  @override
  Future<List<TimeLog>> fetchTimeLogsFromCache() async {
    try {
      final Box<TimeLog> box = await Hive.openBox<TimeLog>(kTimeLogs);
      return box.values.toList();
    } on Exception {
      throw FetchDataException('Something went wrong');
    }
  }

  @override
  Future<void> submitTimeLog(TimeLog timelog) async {
    final WriteBatch batch = _db.batch();

    final DocumentReference<Map<String, dynamic>> careProviderDoc =
        userCollection
            .doc(timelog.careProviderId)
            .collection(kTimeLogs)
            .doc(timelog.timeLogId);

    try {
      batch.set(careProviderDoc, timelog.toJson());

      await batch.commit().timeout(kTimeOut);
      _clearCachedTimeLogs(<String>[timelog.timeLogId]);

      analytics.logEvent(name: 'submit_timelog');
    } on Exception {
      throw UpdateDataException('Operation failed');
    }
  }

  @override
  Future<void> submitTimeLogs(List<TimeLog> timelogs) async {
    final WriteBatch batch = _db.batch();

    for (final TimeLog timelog in timelogs) {
      final DocumentReference<Map<String, dynamic>> careProviderDoc =
          userCollection
              .doc(timelog.careProviderId)
              .collection(kTimeLogs)
              .doc(timelog.timeLogId);

      batch.set(careProviderDoc, timelog.toJson());
    }

    try {
      await batch.commit().timeout(kTimeOut);
      final List<String> keys =
          timelogs.map((TimeLog timelog) => timelog.timeLogId).toList();
      _clearCachedTimeLogs(keys);
      analytics.logEvent(name: 'submit_timelog');
    } on Exception {
      throw UpdateDataException('Operation failed');
    }
  }

  Future<void> _clearCachedTimeLogs(List<String> keys) async {
    try {
      final Box<TimeLog> box = await Hive.openBox<TimeLog>(kTimeLogs);
      await box.deleteAll(keys);
    } on Exception {
      throw UpdateDataException('Operation failed');
    }
  }

  @override
  Future<List<TimeLog>> fetchCareProviderTimeLogsByMonth({
    required int month,
    required String patientId,
    required String careProviderId,
  }) async {
    final DateTime day = DateTime.now().setMonth(month);

    final String start = DateFormat('yyyy-MM-dd').format(day.startOfMonth);
    final String end = DateFormat('yyyy-MM-dd').format(day.endOfMonth);

    try {
      final QuerySnapshot<Map<String, dynamic>> query = await userCollection
          .doc(careProviderId)
          .collection(kTimeLogs)
          .where('patientId', isEqualTo: patientId)
          .where('dateCreated', isGreaterThanOrEqualTo: start)
          .where('dateCreated', isLessThanOrEqualTo: end)
          .orderBy('dateCreated', descending: true)
          .get()
          .timeout(kTimeOut);

      return query.docs
          .map((DocumentSnapshot<Map<String, dynamic>> doc) =>
              TimeLog.fromJson(doc.data()!))
          .toList();
    } on Exception {
      throw FetchDataException('Failed to load data');
    }
  }

  @override
  Stream<List<TimeLog>> fetchCareProviderCurrentMonthTimeLogs(
      String careProviderId) {
    final DateTime day = DateTime.now();

    final String start = DateFormat('yyyy-MM-dd').format(day.startOfMonth);
    final String end = DateFormat('yyyy-MM-dd').format(day.endOfMonth);

    try {
      final Stream<QuerySnapshot<Map<String, dynamic>>> query = userCollection
          .doc(careProviderId)
          .collection(kTimeLogs)
          .where('dateCreated', isGreaterThanOrEqualTo: start)
          .where('dateCreated', isLessThanOrEqualTo: end)
          .orderBy('dateCreated', descending: true)
          .snapshots();

      return query.map((QuerySnapshot<Map<String, dynamic>> snapshot) =>
          snapshot.docs
              .map((QueryDocumentSnapshot<Map<String, dynamic>> doc) =>
                  TimeLog.fromJson(doc.data()))
              .toList());
    } on Exception {
      throw FetchDataException('Failed to load data');
    }
  }

  @override
  Future<void> updateClockedInStatus(CareProvider careProvider) async {
    final bool status = careProvider.isClockedIn;
    userCollection.doc(careProvider.uid).update(<String, dynamic>{
      'isClockedIn': !status,
    });
  }
}
