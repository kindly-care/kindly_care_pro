import 'package:kindly_components/kindly_components.dart';

abstract class ContactsRepository {
  /// adds contact. Throws [UpdateDataException].
  Future<void> addContact({required String uid, required String contactId});

  /// remove contact. Throws [UpdateDataException].
  Future<void> removeContact({required String uid, required String contactId});

  /// fetches contacts. Throws [FetchDataException].
  Future<List<AppUser>> fetchContacts(List<String> contactIds);
}
