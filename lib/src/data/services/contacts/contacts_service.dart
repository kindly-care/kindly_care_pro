import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:collection/collection.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../common/common.dart';
import '../../references.dart';
import 'contacts_repository.dart';

class ContactsService implements ContactsRepository {
  @override
  Future<void> addContact(
      {required String uid, required String contactId}) async {
    try {
      await userCollection.doc(uid).update(<String, dynamic>{
        'contacts': FieldValue.arrayUnion(<String>[contactId]),
      }).timeout(kTimeOut);

      analytics.logEvent(name: 'contact_added');
    } on Exception {
      throw UpdateDataException('Operation failed');
    }
  }

  @override
  Future<void> removeContact(
      {required String uid, required String contactId}) async {
    try {
      await userCollection.doc(uid).update(<String, dynamic>{
        'contacts': FieldValue.arrayRemove(<String>[contactId]),
      }).timeout(kTimeOut);
      analytics.logEvent(name: 'contact_removed');
    } on Exception {
      throw UpdateDataException('Operation failed');
    }
  }

  @override
  Future<List<AppUser>> fetchContacts(List<String> contactIds) async {
    try {
      if (contactIds.isEmpty) {
        return <AppUser>[];
      } else {
        final List<AppUser> contacts = <AppUser>[];
        final List<List<String>> batches = contactIds
            .splitBeforeIndexed(
              (int index, String id) =>
                  index == 9 ||
                  index == 18 ||
                  index == 27 ||
                  index == 36 ||
                  index == 45 ||
                  index == 54 ||
                  index == 63 ||
                  index == 72 ||
                  index == 81 ||
                  index == 90 ||
                  index == 99,
            )
            .toList();

        for (final List<String> batch in batches) {
          final QuerySnapshot<Map<String, dynamic>> query = await userCollection
              .where('uid', whereIn: batch)
              .get()
              .timeout(kTimeOut);

          final List<AppUser> cons = query.docs
              .map((DocumentSnapshot<Map<String, dynamic>> doc) =>
                  AppUser.fromJson(doc.data()!))
              .toList();

          contacts.addAll(cons);
        }

        return contacts;
      }
    } on Exception {
      throw FetchDataException('Failed to load data');
    }
  }
}
