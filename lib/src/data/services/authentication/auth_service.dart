// ignore_for_file: depend_on_referenced_packages

import 'dart:developer' as devtools show log;

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:kindly_components/kindly_components.dart';
import 'package:stream_chat_flutter_core/stream_chat_flutter_core.dart'
    show StreamChatClient;

import '../../../common/common.dart';
import '../../references.dart';

class AuthService implements AuthRepository {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final FirebaseMessaging _fcm = FirebaseMessaging.instance;
  // final FirebaseFunctions _functions = FirebaseFunctions.instance;

  @override
  Future<void> registerWithEmail(AuthDetails details) async {
    _firebaseAuth.authStateChanges();

    try {
      final UserCredential credential =
          await _firebaseAuth.createUserWithEmailAndPassword(
        email: details.email,
        password: details.password,
      );

      await credential.user!.sendEmailVerification();
      await saveAuthDetails(details);

      analytics.logSignUp(signUpMethod: 'email');
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        throw AuthException('Provided password is too weak.');
      } else if (e.code == 'email-already-in-use') {
        throw AuthException(
            'An account already exists for that email address.');
      } else if (e.code == 'invalid-email') {
        throw AuthException('The email address you provided is invalid.');
      } else if (e.code == 'operation-not-allowed') {
        throw AuthException('Operation not allowed. Please contact support.');
      }
    } on Exception {
      throw AuthException('Operation failed. Please try again.');
    }
  }

  @override
  Future<void> signInWithEmail(AuthDetails details) async {
    devtools.log('signing in with email..');
    try {
      final UserCredential credential =
          await _firebaseAuth.signInWithEmailAndPassword(
              email: details.email, password: details.password);

      final User user = credential.user!;

      assert(!user.isAnonymous);

      final User currentUser = _firebaseAuth.currentUser!;

      assert(user.uid == currentUser.uid);

      await saveAuthDetails(details);
      await _initUser(currentUser);
      devtools.log('done..');
      analytics.logLogin(loginMethod: 'email');
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        throw AuthException('No user was found with the given email address.');
      } else if (e.code == 'wrong-password') {
        throw AuthException('Wrong password provided.');
      } else if (e.code == 'invalid-email') {
        throw AuthException('The email address you provided is invalid.');
      } else if (e.code == 'user-disabled') {
        throw AuthException('Your account has been disabled. Contact support.');
      }
    } on Exception catch (e) {
      devtools.log('error: $e');
      throw AuthException('Sign in failed. Please try again.');
    }
  }

  @override
  Stream<AppUser?> fetchCurrentUser() {
    try {
      final String? uid = _firebaseAuth.currentUser?.uid;
      if (uid == null) {
        return Stream<AppUser?>.value(null);
      } else {
        final Stream<DocumentSnapshot<Map<String, dynamic>>> snapshot =
            userCollection.doc(uid).snapshots();

        return snapshot.map((DocumentSnapshot<Map<String, dynamic>> doc) {
          if (doc.exists) {
            if (doc.data()!.containsValue(kCareProvider)) {
              return CareProvider.fromJson(doc.data()!);
            } else if (doc.data()!.containsValue(kUser)) {
              return AppUser.fromJson(doc.data()!);
            } else {
              return null;
            }
          } else {
            return null;
          }
        });
      }
    } on Exception {
      throw FetchDataException('Something went wrong');
    }
  }

  @override
  Future<AppUser?> fetchUserById(String uid) async {
    try {
      final DocumentSnapshot<Map<String, dynamic>> doc =
          await userCollection.doc(uid).get().timeout(kTimeOut);

      if (doc.exists) {
        if (doc.data()!.containsValue(kCareProvider)) {
          return CareProvider.fromJson(doc.data()!);
        } else if (doc.data()!.containsValue(kUser)) {
          return AppUser.fromJson(doc.data()!);
        } else {
          return null;
        }
      } else {
        return null;
      }
    } on Exception {
      throw FetchDataException('Operation failed');
    }
  }

  @override
  Future<AppUser?> fetchUserByEmail(String email) async {
    try {
      final QuerySnapshot<Map<String, dynamic>> query = await userCollection
          .where('email', isEqualTo: email)
          .get()
          .timeout(kTimeOut);

      if (query.docs.isNotEmpty) {
        final QueryDocumentSnapshot<Map<String, dynamic>> doc =
            query.docs.first;
        return AppUser.fromJson(doc.data());
      } else {
        return null;
      }
    } on Exception {
      throw FetchDataException('Operation failed');
    }
  }

  @override
  Future<void> saveAuthDetails(AuthDetails auth) async {
    try {
      final Box<AuthDetails> box =
          await Hive.openBox<AuthDetails>(kAuthDetails);
      await box.put(kAuthKey, auth);
    } on Exception {
      throw UpdateDataException('Something went wrong');
    }
  }

  @override
  Future<AuthDetails?> fetchAuthDetails() async {
    try {
      final Box<AuthDetails> box =
          await Hive.openBox<AuthDetails>(kAuthDetails);

      return box.get(kAuthKey);
    } on Exception {
      throw FetchDataException('Something went wrong');
    }
  }

  @override
  Future<void> resetPassword(String email) async {
    try {
      await _firebaseAuth.sendPasswordResetEmail(email: email);
      analytics.logEvent(name: 'reset_password');
    } on FirebaseAuthException catch (e) {
      if (e.code == 'auth/invalid-email') {
        throw AuthException('The email address you provided is invalid.');
      } else if (e.code == 'auth/user-not-found') {
        throw AuthException(
            'No user was found with the provided email address.');
      }
    } on Exception {
      throw AuthException('Something went wrong. Please try again.');
    }
  }

  @override
  Future<void> signOut() async {
    await _firebaseAuth.signOut();
  }

  @override
  Future<void> deleteAccount() async {
    try {
      final String uid = _firebaseAuth.currentUser!.uid;
      await userCollection.doc(uid).delete();
      analytics.logEvent(name: 'delete_account');
    } on Exception {
      throw UpdateDataException('Failed to perform operation.');
    }
  }

  Future<void> _initUser(User user) async {
    devtools.log('initializing current user..');
    final AuthDetails? details = await fetchAuthDetails();

    try {
      final String? messageToken = await _fcm.getToken();
      final DocumentSnapshot<Map<String, dynamic>> doc =
          await userCollection.doc(user.uid).get().timeout(kTimeOut);

      devtools.log('initializing..');

      if (!doc.exists) {
        // final String streamToken = await _fetchStreamUserToken();
        final StreamChatClient client = ChatClient.client;

        final AppUser newUser = AppUser(
          uid: user.uid,
          email: user.email,
          userType: UserType.user,
          joinedDate: DateTime.now(),
          contacts: const <String>[],
          messageToken: messageToken ?? '',
          avatar: user.photoURL ?? kDefaultAvatar,
          streamToken: client.devToken(user.uid).rawValue,
          phone: user.phoneNumber ?? details?.phone ?? '07',
          name: user.displayName ?? details?.name ?? 'Name',
        );

        await userCollection.doc(user.uid).set(newUser.toJson());
      } else {
        await userCollection.doc(user.uid).update(<String, dynamic>{
          'messageToken': messageToken,
          'lastSeen': DateTime.now().toIso8601String(),
        });
      }
    } on Exception catch (e) {
      devtools.log('_initUser error: $e');
      throw AuthException('Something went wrong. Please try again');
    }
  }

  // Future<String> _fetchStreamUserToken() async {
  //   try {
  //     final HttpsCallable callable =
  //         _functions.httpsCallable('getStreamUserToken');
  //     final HttpsCallableResult<String> result = await callable.call<String>();
  //     return result.data;
  //   } on Exception catch (e) {
  //     devtools.log('stream token error: $e');
  //     throw FetchDataException('Failed to load data');
  //   }
  // }
}
