import 'package:kindly_components/kindly_components.dart';


abstract class VitalsRepository {
  /// fetches [BloodGlucoseRecord]s. Throws [FetchDataException].
  Stream<List<BloodGlucoseRecord>> fetchBloodGlucoseByMonth(
      {required String patientId, required int month});

  /// fetches [BloodPressureRecord]s. Throws [FetchDataException].
  Stream<List<BloodPressureRecord>> fetchBloodPressureByMonth(
      {required String patientId, required int month});

  /// fetches [WeightRecord]s. Throws [FetchDataException].
  Stream<List<WeightRecord>> fetchWeightByMonth(
      {required String patientId, required int month});

  /// fetches [TemperatureRecord]s. Throws [FetchDataException].
  Stream<List<TemperatureRecord>> fetchTemperatureByMonth(
      {required String patientId, required int month});

  /// records temperature. Throws [UpdateDataException].
  Future<void> recordTemperature(TemperatureRecord record);

  /// records weight. Throws [UpdateDataException].
  Future<void> recordWeight(WeightRecord record);

  /// records blood sugar. Throws [UpdateDataException].
  Future<void> recordBloodGlucose(BloodGlucoseRecord record);

  /// records blood pressure. Throws [UpdateDataException].
  Future<void> recordBloodPressure(BloodPressureRecord record);

  // locals
  Future<WeightRecord?> fetchWeightFromCache();
  Future<TemperatureRecord?> fetchTemperatureFromCache();
  Future<BloodGlucoseRecord?> fetchBloodGlucoseFromCache();
  Future<BloodPressureRecord?> fetchBloodPressureFromCache();
  Future<void> clearVitalsFromCache();
}
