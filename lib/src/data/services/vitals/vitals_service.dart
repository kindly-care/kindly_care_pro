import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dart_date/dart_date.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../common/common.dart';
import '../../references.dart';
import 'vitals_repository.dart';

class VitalsService implements VitalsRepository {
  @override
  Stream<List<BloodPressureRecord>> fetchBloodPressureByMonth(
      {required int month, required String patientId}) {
    final DateTime date = DateTime.now().setMonth(month);

    final String start = DateFormat(kYMD).format(date.startOfMonth);
    final String end = DateFormat(kYMD).format(date.endOfMonth);

    try {
      final Stream<QuerySnapshot<Map<String, dynamic>>> query = userCollection
          .doc(patientId)
          .collection(kBloodPressure)
          .where(kDateCreated, isGreaterThanOrEqualTo: start)
          .where(kDateCreated, isLessThanOrEqualTo: end)
          .orderBy(kDateCreated, descending: true)
          .snapshots();

      return query.map((QuerySnapshot<Map<String, dynamic>> snapshot) =>
          snapshot.docs
              .map((QueryDocumentSnapshot<Map<String, dynamic>> doc) =>
                  BloodPressureRecord.fromJson(doc.data()))
              .toList());
    } on Exception {
      throw FetchDataException('Failed to load data');
    }
  }

  @override
  Stream<List<BloodGlucoseRecord>> fetchBloodGlucoseByMonth(
      {required int month, required String patientId}) {
    final DateTime date = DateTime.now().setMonth(month);

    final String start = DateFormat(kYMD).format(date.startOfMonth);
    final String end = DateFormat(kYMD).format(date.endOfMonth);

    try {
      final Stream<QuerySnapshot<Map<String, dynamic>>> query = userCollection
          .doc(patientId)
          .collection(kBloodGlucose)
          .where(kDateCreated, isGreaterThanOrEqualTo: start)
          .where(kDateCreated, isLessThanOrEqualTo: end)
          .orderBy(kDateCreated, descending: true)
          .snapshots();

      return query.map((QuerySnapshot<Map<String, dynamic>> snapshot) =>
          snapshot.docs
              .map((QueryDocumentSnapshot<Map<String, dynamic>> doc) =>
                  BloodGlucoseRecord.fromJson(doc.data()))
              .toList());
    } on Exception {
      throw FetchDataException('Failed to load data');
    }
  }

  @override
  Stream<List<TemperatureRecord>> fetchTemperatureByMonth(
      {required int month, required String patientId}) {
    final DateTime date = DateTime.now().setMonth(month);

    final String start = DateFormat(kYMD).format(date.startOfMonth);
    final String end = DateFormat(kYMD).format(date.endOfMonth);

    try {
      final Stream<QuerySnapshot<Map<String, dynamic>>> query = userCollection
          .doc(patientId)
          .collection(kTemperature)
          .where(kDateCreated, isGreaterThanOrEqualTo: start)
          .where(kDateCreated, isLessThanOrEqualTo: end)
          .orderBy(kDateCreated, descending: true)
          .snapshots();

      return query.map((QuerySnapshot<Map<String, dynamic>> snapshot) =>
          snapshot.docs
              .map((QueryDocumentSnapshot<Map<String, dynamic>> doc) =>
                  TemperatureRecord.fromJson(doc.data()))
              .toList());
    } on Exception {
      throw FetchDataException('Failed to load data');
    }
  }

  @override
  Stream<List<WeightRecord>> fetchWeightByMonth(
      {required int month, required String patientId}) {
    final DateTime date = DateTime.now().setMonth(month);

    final String start = DateFormat(kYMD).format(date.startOfMonth);
    final String end = DateFormat(kYMD).format(date.endOfMonth);

    try {
      final Stream<QuerySnapshot<Map<String, dynamic>>> query = userCollection
          .doc(patientId)
          .collection(kWeight)
          .where(kDateCreated, isGreaterThanOrEqualTo: start)
          .where(kDateCreated, isLessThanOrEqualTo: end)
          .orderBy(kDateCreated, descending: true)
          .snapshots();

      return query.map((QuerySnapshot<Map<String, dynamic>> snapshot) =>
          snapshot.docs
              .map((QueryDocumentSnapshot<Map<String, dynamic>> doc) =>
                  WeightRecord.fromJson(doc.data()))
              .toList());
    } on Exception {
      throw FetchDataException('Failed to load data');
    }
  }

  @override
  Future<void> recordBloodPressure(BloodPressureRecord record) async {
    try {
      await userCollection
          .doc(record.patientId)
          .collection(kBloodPressure)
          .add(record.toJson())
          .timeout(kTimeOut);

      await userCollection.doc(record.patientId).update(<String, dynamic>{
        kLastBloodPressureRecord: record.toJson(),
      }).timeout(kTimeOut);

      await saveBloodPressureToCache(record);
      analytics.logEvent(name: kBloodPressureEvt);
    } on Exception {
      throw UpdateDataException('Failed to submit blood pressure');
    }
  }

  @override
  Future<void> recordBloodGlucose(BloodGlucoseRecord record) async {
    try {
      await userCollection
          .doc(record.patientId)
          .collection(kBloodGlucose)
          .add(record.toJson())
          .timeout(kTimeOut);

      await userCollection.doc(record.patientId).update(<String, dynamic>{
        kLastBloodGlucoseRecord: record.toJson(),
      }).timeout(kTimeOut);

      await saveBloodGlucoseToCache(record);
      analytics.logEvent(name: kBloodGlucoseEvt);
    } on Exception {
      throw UpdateDataException('Failed to submit blood glucose');
    }
  }

  @override
  Future<void> recordTemperature(TemperatureRecord record) async {
    try {
      await userCollection
          .doc(record.patientId)
          .collection(kTemperature)
          .add(record.toJson())
          .timeout(kTimeOut);

      await userCollection.doc(record.patientId).update(<String, dynamic>{
        kLastTemperatureRecord: record.toJson(),
      }).timeout(kTimeOut);

      await saveTemperatureToCache(record);
      analytics.logEvent(name: kTemperatureEvt);
    } on Exception {
      throw UpdateDataException('Failed to submit temperature');
    }
  }

  @override
  Future<void> recordWeight(WeightRecord record) async {
    try {
      await userCollection
          .doc(record.patientId)
          .collection(kWeight)
          .add(record.toJson())
          .timeout(kTimeOut);

      await userCollection.doc(record.patientId).update(<String, dynamic>{
        kLastWeightRecord: record.toJson(),
      }).timeout(kTimeOut);

      await saveWeightToCache(record);
      analytics.logEvent(name: kWeightEvt);
    } on Exception {
      throw UpdateDataException('Failed to submit weight');
    }
  }

  // local

  Future<void> saveWeightToCache(WeightRecord record) async {
    final Box<WeightRecord> box = await Hive.openBox<WeightRecord>(kWeight);
    await box.put(kWeightKey, record);
  }

  @override
  Future<WeightRecord?> fetchWeightFromCache() async {
    final Box<WeightRecord> box = await Hive.openBox<WeightRecord>(kWeight);
    return box.get(kWeightKey);
  }

  Future<void> saveTemperatureToCache(TemperatureRecord record) async {
    final Box<TemperatureRecord> box =
        await Hive.openBox<TemperatureRecord>(kTemperature);
    await box.put(kTemperatureKey, record);
  }

  @override
  Future<TemperatureRecord?> fetchTemperatureFromCache() async {
    final Box<TemperatureRecord> box =
        await Hive.openBox<TemperatureRecord>(kTemperature);
    return box.get(kTemperatureKey);
  }

  Future<void> saveBloodGlucoseToCache(BloodGlucoseRecord record) async {
    final Box<BloodGlucoseRecord> box =
        await Hive.openBox<BloodGlucoseRecord>(kBloodGlucose);
    await box.put(kBloodGlucoseKey, record);
  }

  @override
  Future<BloodGlucoseRecord?> fetchBloodGlucoseFromCache() async {
    final Box<BloodGlucoseRecord> box =
        await Hive.openBox<BloodGlucoseRecord>(kBloodGlucose);
    return box.get(kBloodGlucoseKey);
  }

  Future<void> saveBloodPressureToCache(BloodPressureRecord record) async {
    final Box<BloodPressureRecord> box =
        await Hive.openBox<BloodPressureRecord>(kBloodPressure);
    await box.put(kBloodPressureKey, record);
  }

  @override
  Future<BloodPressureRecord?> fetchBloodPressureFromCache() async {
    final Box<BloodPressureRecord> box =
        await Hive.openBox<BloodPressureRecord>(kBloodPressure);
    return box.get(kBloodPressureKey);
  }

  @override
  Future<void> clearVitalsFromCache() async {
    final Box<WeightRecord> weightBox =
        await Hive.openBox<WeightRecord>(kWeight);
    final Box<TemperatureRecord> temperatureBox =
        await Hive.openBox<TemperatureRecord>(kTemperature);
    final Box<BloodGlucoseRecord> bloodSugarBox =
        await Hive.openBox<BloodGlucoseRecord>(kBloodGlucose);
    final Box<BloodPressureRecord> bloodPressureBox =
        await Hive.openBox<BloodPressureRecord>(kBloodPressure);

    weightBox.clear();
    bloodSugarBox.clear();
    temperatureBox.clear();
    bloodPressureBox.clear();
  }
}
