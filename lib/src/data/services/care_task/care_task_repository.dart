import 'package:kindly_components/kindly_components.dart';


abstract class CareTaskRepository {
  /// fetches [Patients]'s care tasks. Throws [FetchDataException].
  Stream<List<CareTask>> fetchCareTasks(String patientId);

  /// fetches [Patients]'s care tasks by date. Throws [FetchDataException].
  Stream<List<CareTask>> fetchCareTasksByDate(
      {required String patientId, required DateTime date});

  /// fetches [Patients]'s care actions. Throws [FetchDataException].
  Stream<List<CareAction>> fetchCareActionsByDate(
      {required String patientId, required DateTime date});

  /// removes [CareTask]s from cache. Throws [UpdateDataException].
  Future<void> clearCareActionsFromCache();

  /// saves [CareAction] to cache. Throws [UpdateDataException].
  Future<void> saveCareActionToCache(CareAction action);

  /// fetches locally stored [CareAction]s. Throws [FetchDataException].
  Future<List<CareAction>> fetchCareActionsFromCache();

  /// submits [CareAction]s and clears them from cached. Throws [UpdateDataException].
  Future<void> submitCareActions(List<CareAction> actions);

  /// updates [Patient]'s [Meal]s. Throws [UpdateDataException].
  Future<void> updatePatientMeal(Meal meal);
}
