import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../common/constants/constants.dart';
import '../../references.dart';
import 'care_task_repository.dart';

class CareTaskService implements CareTaskRepository {
  final FirebaseFirestore _db = FirebaseFirestore.instance;

  @override
  Stream<List<CareTask>> fetchCareTasks(String patientId) {
    try {
      final Stream<QuerySnapshot<Map<String, dynamic>>> query = userCollection
          .doc(patientId)
          .collection(kCareTasks)
          .orderBy('lastUpdated', descending: true)
          .snapshots();

      return query.map((QuerySnapshot<Map<String, dynamic>> snapshot) =>
          snapshot.docs.map((QueryDocumentSnapshot<Map<String, dynamic>> doc) {
            if (doc.data()['type'] == 'meal') {
              return MealTask.fromJson(doc.data());
            } else if (doc.data()['type'] == 'medication') {
              return MedicationTask.fromJson(doc.data());
            } else {
              return CareTask.fromJson(doc.data());
            }
          }).toList());
    } on Exception {
      throw FetchDataException('Failed to load data');
    }
  }

  @override
  Stream<List<CareTask>> fetchCareTasksByDate(
      {required String patientId, required DateTime date}) {
    final String day = DateFormat('EEEE').format(date);
    try {
      final Stream<QuerySnapshot<Map<String, dynamic>>> query = userCollection
          .doc(patientId)
          .collection(kCareTasks)
          .where(kRepeatDays, arrayContains: day)
          .snapshots();

      return query.map((QuerySnapshot<Map<String, dynamic>> snapshot) =>
          snapshot.docs.map((QueryDocumentSnapshot<Map<String, dynamic>> doc) {
            if (doc.data()['type'] == 'meal') {
              return MealTask.fromJson(doc.data());
            } else if (doc.data()['type'] == 'medication') {
              return MedicationTask.fromJson(doc.data());
            } else {
              return CareTask.fromJson(doc.data());
            }
          }).toList());
    } on Exception {
      throw FetchDataException('Failed to load data');
    }
  }

  @override
  Stream<List<CareAction>> fetchCareActionsByDate(
      {required String patientId, required DateTime date}) {
    final String formattedDate = DateFormat(kYMD).format(date);

    try {
      final Stream<QuerySnapshot<Map<String, dynamic>>> query = userCollection
          .doc(patientId)
          .collection(kCareActions)
          .where(kDateCreated, isEqualTo: formattedDate)
          .snapshots();

      return query.map(
        (QuerySnapshot<Map<String, dynamic>> snapshot) => snapshot.docs
            .map((QueryDocumentSnapshot<Map<String, dynamic>> doc) {
          if (doc.data()['taskType'] == 'meal') {
            return MealAction.fromJson(doc.data());
          } else if (doc.data()['taskType'] == 'medication') {
            return MedicationAction.fromJson(doc.data());
          } else {
            return CareAction.fromJson(doc.data());
          }
        }).toList(),
      );
    } on Exception {
      throw FetchDataException('Failed to load data');
    }
  }

  @override
  Future<void> saveCareActionToCache(CareAction action) async {
    try {
      final Box<CareAction> box = await Hive.openBox<CareAction>(kCareActions);
      await box.put(action.actionId, action);
    } on Exception {
      throw UpdateDataException('Operation failed');
    }
  }

  @override
  Future<void> submitCareActions(List<CareAction> actions) async {
    final WriteBatch batch = _db.batch();

    try {
      for (final CareAction action in actions) {
        final DocumentReference<Map<String, dynamic>> actionDoc = userCollection
            .doc(action.patientId)
            .collection(kCareActions)
            .doc(action.actionId);

        batch.set(actionDoc, action.toJson());
      }

      await batch.commit().timeout(kTimeOut);
    } catch (e) {
      throw UpdateDataException('Operation failed');
    }
  }

  @override
  Future<void> updatePatientMeal(Meal meal) async {
    final String type = meal.type.toLowerCase();
    try {
      await userCollection.doc(meal.patientId).update(<String, dynamic>{
        'meals.$type': meal.toJson(),
      }).timeout(kTimeOut);
    } on Exception {
      throw UpdateDataException('Operation failed');
    }
  }

  @override
  Future<void> clearCareActionsFromCache() async {
    try {
      final Box<CareAction> box = await Hive.openBox<CareAction>(kCareActions);
      await box.clear();
    } on Exception {
      throw UpdateDataException('Operation failed');
    }
  }

  @override
  Future<List<CareAction>> fetchCareActionsFromCache() async {
    try {
      final Box<CareAction> box = await Hive.openBox<CareAction>(kCareActions);
      return box.values.toList();
    } on Exception {
      throw FetchDataException('Operation failed');
    }
  }
}
