import 'dart:developer' as devtools show log;

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../common/common.dart';
import '../../references.dart';
import 'notification_repository.dart';

class NotificationService implements NotificationRepository {
  final FirebaseFirestore _db = FirebaseFirestore.instance;
  final FirebaseFunctions _functions = FirebaseFunctions.instance;

  @override
  Stream<List<PushNotification>> fetchNotifications(String uid) {
    try {
      final Stream<QuerySnapshot<Map<String, dynamic>>> query = userCollection
          .doc(uid)
          .collection(kNotifications)
          .orderBy(kCreatedAt, descending: true)
          .snapshots();

      return query.map((QuerySnapshot<Map<String, dynamic>> snapshot) =>
          snapshot.docs.map((QueryDocumentSnapshot<Map<String, dynamic>> doc) {
            return PushNotification.fromMap(doc.data());
          }).toList());
    } on Exception {
      throw FetchDataException('Failed to load data');
    }
  }

  @override
  Future<void> sendPushNotification(PushNotification notification) async {
    final WriteBatch batch = _db.batch();

    try {
      for (final String recipient in notification.recipients) {
        final DocumentReference<Map<String, dynamic>> doc = userCollection
            .doc(recipient)
            .collection(kNotifications)
            .doc(notification.id);

        batch.set(doc, notification.toMap());
      }

      await batch.commit().timeout(kTimeOut);
      analytics.logEvent(name: 'push_notification_sent');
    } on Exception {
      throw UpdateDataException('Operation failed');
    }
  }

  @override
  Future<void> deleteNotification(
      {required String uid, required String notificationId}) async {
    try {
      await userCollection
          .doc(uid)
          .collection(kNotifications)
          .doc(notificationId)
          .delete()
          .timeout(kTimeOut);
    } on Exception {
      throw UpdateDataException('Failed to delete notification');
    }
  }

  @override
  Future<void> deleteAllNotifications(
      {required String uid, required List<String> notificationIds}) async {
    final WriteBatch batch = FirebaseFirestore.instance.batch();

    try {
      for (final String id in notificationIds) {
        final DocumentReference<Map<String, dynamic>> doc =
            userCollection.doc(uid).collection(kNotifications).doc(id);

        batch.delete(doc);
      }

      await batch.commit().timeout(kTimeOut);
    } on Exception {
      throw UpdateDataException('Failed to perform operation');
    }
  }

  @override
  void markNotificationAsSeen(
      {required String uid, required String notificationId}) {
    userCollection
        .doc(uid)
        .collection(kNotifications)
        .doc(notificationId)
        .update(<String, dynamic>{'isSeen': true});
  }

  @override
  void changeNotificationStatus(
      {required String uid, required String notificationId}) {
    userCollection
        .doc(uid)
        .collection(kNotifications)
        .doc(notificationId)
        .update(<String, dynamic>{
      'isActedOn': true,
    });
  }

  @override
  Future<void> sendChatNotification({
    required String message,
    required String sender,
    required String recipientId,
  }) async {
    final HttpsCallable callable = _functions.httpsCallable(kChatNotification);

    try {
      await callable.call<void>(<String, dynamic>{
        'message': message,
        'sender': sender,
        'recipientId': recipientId,
      });
    } on Exception catch (e) {
      devtools.log('error:', error: e);
    }
  }
}
