import 'package:kindly_components/kindly_components.dart';

abstract class NotificationRepository {
  /// fetches user [PushNotification]s. Throws [FetchDataException].
  Stream<List<PushNotification>> fetchNotifications(String uid);

  /// sends [PushNotification]. Throws [UpdateDataException].
  Future<void> sendPushNotification(PushNotification notification);

  /// deletes [PushNotification]. Throws [UpdateDataException].
  Future<void> deleteNotification(
      {required String uid, required String notificationId});

  /// deletes all [PushNotification]s. Throws [UpdateDataException].
  Future<void> deleteAllNotifications(
      {required String uid, required List<String> notificationIds});

  /// marks [PushNotification] as seen.
  void markNotificationAsSeen(
      {required String uid, required String notificationId});

  /// changes [PushNotification]'s status.
  void changeNotificationStatus(
      {required String uid, required String notificationId});

  /// send chat notification to device.
  Future<void> sendChatNotification({
    required String message,
    required String sender,
    required String recipientId,
  });
}
