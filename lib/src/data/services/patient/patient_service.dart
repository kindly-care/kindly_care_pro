import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../common/common.dart';
import '../../references.dart';
import 'patient_repository.dart';

class PatientService implements PatientRepository {
  @override
  Future<List<AppUser>> fetchPatientCircle(List<String> ids) async {
    try {
      final QuerySnapshot<Map<String, dynamic>> query = await userCollection
          .where('uid', whereIn: ids)
          .get()
          .timeout(kTimeOut);

      return query.docs
          .map((DocumentSnapshot<Map<String, dynamic>> doc) =>
              AppUser.fromJson(doc.data()!))
          .toList();
    } on Exception {
      throw FetchDataException('Failed to load data.');
    }
  }

  @override
  Future<Patient?> fetchPatient(String id) async {
    try {
      final DocumentSnapshot<Map<String, dynamic>> doc =
          await userCollection.doc(id).get().timeout(kTimeOut);

      if (doc.exists && doc.data()![kUserType] == kPatient) {
        return Patient.fromJson(doc.data()!);
      } else {
        return null;
      }
    } on Exception {
      throw FetchDataException('Failed to load data.');
    }
  }

  @override
  Future<List<CareProvider>> fetchAssignedCareProviders(
      List<String> ids) async {
    try {
      if (ids.isEmpty) {
        return const <CareProvider>[];
      }
      final QuerySnapshot<Map<String, dynamic>> query = await userCollection
          .where('uid', whereIn: ids)
          .where(kUserType, isEqualTo: kCareProvider)
          .get()
          .timeout(kTimeOut);

      return query.docs
          .map((DocumentSnapshot<Map<String, dynamic>> doc) =>
              CareProvider.fromJson(doc.data()!))
          .toList();
    } on Exception {
      throw FetchDataException('Failed to load data');
    }
  }
}
