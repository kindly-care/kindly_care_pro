import 'package:kindly_components/kindly_components.dart';

abstract class PatientRepository {
  /// fetches [AppUser]s. Throws [FetchDataException].
  Future<List<AppUser>> fetchPatientCircle(List<String> ids);

  /// fetch [Patient] by id. Throws [FetchDataException].
  Future<Patient?> fetchPatient(String id);

  /// fetches assigned [CareProvider]s. Throws [FetchDataException].
  Future<List<CareProvider>> fetchAssignedCareProviders(List<String> ids);
}
