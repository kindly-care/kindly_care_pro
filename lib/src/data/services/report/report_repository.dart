import 'package:kindly_components/kindly_components.dart';

abstract class ReportRepository {
  /// fetches [Patient]s [Report]. Throws [FetchDataException].
  Future<List<Report>> fetchReportsByMonth(
      {required String patientId, required int month});

  /// fetches [CareProvider] specific [Report]s for a given [Patient]. Throws [FetchDataException].
  Future<List<Report>> fetchCareProviderReportsByMonth({
    required int month,
    required String patientId,
    required String careProviderId,
  });

  /// fetches a single [Patient]'s [Report] by id. Throws [FetchDataException].
  Future<Report?> fetchReport(
      {required String patientId, required String reportId});

  /// attempts to save [Report] to cache. Throws [UpdateDataException].
  Future<void> saveReportToCache(Report report);

  /// attempts to fetch cached [Report]s by keys. Throws [FetchDataException].
  Future<List<Report>> fetchReportsFromCacheByKeys(List<String> keys);

  /// attempts to submit [Report] to database. Throws [UpdateDataException].
  Future<void> submitReport(Report report);

  /// submits multiple [Report]s to database. Throws [UpdateDataException].
  Future<void> submitReports(List<Report> reports);
}
