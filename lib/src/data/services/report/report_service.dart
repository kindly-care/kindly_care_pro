import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dart_date/dart_date.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:intl/intl.dart';
import 'package:kindly_components/kindly_components.dart';

import '../../../common/analytics/analytics.dart';
import '../../../common/constants/constants.dart';
import '../../references.dart';
import '../firebase_helper.dart';
import 'report_repository.dart';

class ReportService implements ReportRepository {
  final FirebaseFirestore _db = FirebaseFirestore.instance;

  @override
  Future<List<Report>> fetchReportsByMonth(
      {required String patientId, required int month}) async {
    final DateTime day = DateTime.now().setMonth(month);

    final String start = DateFormat('yyyy-MM-dd').format(day.startOfMonth);
    final String end = DateFormat('yyyy-MM-dd').format(day.endOfMonth);

    try {
      final QuerySnapshot<Map<String, dynamic>> query = await userCollection
          .doc(patientId)
          .collection(kReports)
          .where('dateCreated', isGreaterThanOrEqualTo: start)
          .where('dateCreated', isLessThanOrEqualTo: end)
          .orderBy('dateCreated', descending: false)
          .get()
          .timeout(kTimeOut);

      return query.docs
          .map((DocumentSnapshot<Map<String, dynamic>> doc) =>
              Report.fromJson(doc.data()!))
          .toList();
    } on Exception {
      throw FetchDataException('Failed to load data.');
    }
  }

  @override
  Future<List<Report>> fetchCareProviderReportsByMonth({
    required int month,
    required String patientId,
    required String careProviderId,
  }) async {
    final DateTime day = DateTime.now().setMonth(month);

    final String start = DateFormat('yyyy-MM-dd').format(day.startOfMonth);
    final String end = DateFormat('yyyy-MM-dd').format(day.endOfMonth);

    try {
      final QuerySnapshot<Map<String, dynamic>> query = await userCollection
          .doc(patientId)
          .collection(kReports)
          .where('dateCreated', isGreaterThanOrEqualTo: start)
          .where('dateCreated', isLessThanOrEqualTo: end)
          .where('careProviderId', isEqualTo: careProviderId)
          .orderBy('dateCreated', descending: true)
          .get()
          .timeout(kTimeOut);

      return query.docs
          .map((DocumentSnapshot<Map<String, dynamic>> doc) =>
              Report.fromJson(doc.data()!))
          .toList();
    } on Exception {
      throw FetchDataException('Failed to load data.');
    }
  }

  @override
  Future<Report?> fetchReport(
      {required String patientId, required String reportId}) async {
    try {
      final DocumentSnapshot<Map<String, dynamic>> queryDoc =
          await userCollection
              .doc(patientId)
              .collection(kReports)
              .doc(reportId)
              .get()
              .timeout(kTimeOut);

      if (queryDoc.exists) {
        return Report.fromJson(queryDoc.data()!);
      } else {
        return null;
      }
    } on Exception {
      throw FetchDataException('Failed to load data.');
    }
  }

  @override
  Future<void> saveReportToCache(Report report) async {
    try {
      final Box<Report> box = await Hive.openBox<Report>(kReports);

      await box.put(report.reportId, report);
    } on Exception {
      throw UpdateDataException('Failed to write report');
    }
  }

  @override
  Future<List<Report>> fetchReportsFromCacheByKeys(List<String> keys) async {
    final List<Report> reports = <Report>[];
    try {
      final Box<Report> box = await Hive.openBox<Report>(kReports);

      for (final String key in keys) {
        final Report? report = box.get(key);
        if (report != null) {
          reports.add(report);
        }
      }

      return reports;
    } on Exception {
      throw FetchDataException('Failed to load data');
    }
  }

  @override
  Future<void> submitReport(Report report) async {
    List<String> imageURLs = <String>[];
    final bool hasPhotos = report.attachedPhotos.isNotEmpty;
    try {
      if (hasPhotos) {
        final List<File> images = <File>[];
        for (final String path in report.attachedPhotos) {
          final Uri uri = Uri(path: path);
          final File file = File.fromUri(uri);
          images.add(file);
        }

        imageURLs =
            await getDownloadURLs(files: images, pathId: report.patientId);
      }

      await userCollection
          .doc(report.patientId)
          .collection(kReports)
          .doc(report.reportId)
          .set(report.copyWith(attachedPhotos: imageURLs).toJson())
          .timeout(kTimeOut);

      _clearReportsFromCache(<String>[report.reportId]);
      analytics.logEvent(name: 'report_submitted');
    } on Exception {
      throw UpdateDataException('Failed to upload report');
    }
  }

  @override
  Future<void> submitReports(List<Report> reports) async {
    final WriteBatch batch = _db.batch();

    try {
      for (final Report report in reports) {
        List<String> imageURLs = <String>[];
        final bool hasPhotos = report.attachedPhotos.isNotEmpty;
        if (hasPhotos) {
          final List<File> images = <File>[];
          for (final String path in report.attachedPhotos) {
            final Uri uri = Uri(path: path);
            final File file = File.fromUri(uri);
            images.add(file);
          }

          imageURLs =
              await getDownloadURLs(files: images, pathId: report.patientId)
                  .timeout(kTimeOut);
        }

        final DocumentReference<Map<String, dynamic>> reportDoc = userCollection
            .doc(report.patientId)
            .collection(kReports)
            .doc(report.reportId);

        batch.set(
            reportDoc, report.copyWith(attachedPhotos: imageURLs).toJson());
      }

      await batch.commit().timeout(kTimeOut);

      final List<String> keys =
          reports.map((Report report) => report.reportId).toList();
      _clearReportsFromCache(keys);
      analytics.logEvent(name: 'report_submitted');
    } on Exception {
      throw UpdateDataException('Failed to upload report');
    }
  }

  Future<void> _clearReportsFromCache(List<String> keys) async {
    try {
      final Box<Report> box = await Hive.openBox<Report>(kReports);
      await box.deleteAll(keys);
    } on Exception {
      throw UpdateDataException('Operation failed');
    }
  }
}
