// ignore_for_file: depend_on_referenced_packages

import 'package:firebase_messaging/firebase_messaging.dart';

class CloudMessaging {
  FirebaseMessaging messaging = FirebaseMessaging.instance;

  void init() {
    onMessage();
    onBackground();
  }

  void onMessage() {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      if (message.notification != null) {
        // print('Message also contained a notification: ${message.notification}');
      }
    });
  }

  void onBackground() {
    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  }

  static Future<void> _firebaseMessagingBackgroundHandler(
      RemoteMessage message) async {
    // print('Handling a background message: ${message.messageId}');
  }
}
