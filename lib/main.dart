import 'package:fcm_config/fcm_config.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:kindly_components/kindly_components.dart';

import 'app.dart';
import 'src/app/providers.dart';
import 'src/blocs/simple_bloc_observer.dart';
import 'src/common/helper/helper.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Hive.initFlutter();
  Hive.registerAdapter(ReportAdapter());
  Hive.registerAdapter(TimeLogAdapter());
  Hive.registerAdapter(CareActionAdapter());
  Hive.registerAdapter(MealActionAdapter());
  Hive.registerAdapter(AuthDetailsAdapter());
  Hive.registerAdapter(WeightRecordAdapter());
  Hive.registerAdapter(CareTaskTypeAdapter());
  Hive.registerAdapter(MedicationActionAdapter());
  Hive.registerAdapter(CareActionStatusAdapter());
  Hive.registerAdapter(TemperatureRecordAdapter());
  Hive.registerAdapter(BloodGlucoseRecordAdapter());
  Hive.registerAdapter(BloodPressureRecordAdapter());

  await SharedPrefs.init();
  await ReportBox.init();
  await TimeLogBox.init();
  await CareActionBox.init();
  await Firebase.initializeApp();

  final FirebaseRemoteConfig remoteConfig = FirebaseRemoteConfig.instance;
  await remoteConfig.setConfigSettings(RemoteConfigSettings(
    fetchTimeout: const Duration(minutes: 1),
    minimumFetchInterval: const Duration(hours: 24),
  ));

  await remoteConfig.fetchAndActivate();
  ChatClient.init(config: remoteConfig);

  await FCMConfig.instance.init(
    defaultAndroidForegroundIcon: '@mipmap/ic_launcher_foreground',
    onBackgroundMessage: _firebaseMessagingBackgroundHandler,
    defaultAndroidChannel: const AndroidNotificationChannel(
      'high_importance_channel',
      'General Notifications',
      importance: Importance.max,
    ),
  );

  await SystemChrome.setPreferredOrientations(<DeviceOrientation>[
    DeviceOrientation.portraitDown,
    DeviceOrientation.portraitUp,
  ]);

  BlocOverrides.runZoned(
    () => runApp(Providers(child: MyApp(config: remoteConfig))),
    blocObserver: SimpleBlocObserver(),
  );
}

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // print('Handling a background message ${message.messageId}');
}
